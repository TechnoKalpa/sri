<!--

@date        4/4/11
@description HRBP User Interface.  Implements the SlickGrid to provide an excel-like interface
             for manipulating data within force.

-->
<apex:page showHeader="false" standardStylesheets="false" sidebar="false" controller="CAT_HrbpController" id="thePage" action="{!createAppData}">
    <head>
        <title>Facebook Compensation</title>

        <c:cat_hrbpuicss />

        <c:CAT_HrbpUIJS currCycle="{!currCycle}"
                        pageLoadTime="{!pageLoadTime}"
                        focalPrefix="{!focalPrefix}"
                        locked="{!locked}"
                        isTierUser="{!isTierUser}"
                        isCompAdmin="{!isCompAdmin}"
                        isHrbp="{!isHrbp}"
                        initialChunkSize="{!initialChunkSize}"
                        chunkSize="{!chunkSize}"
                        primaryFilterParam="{!primaryFilterParam}"
                        primaryFilterParam2="{!primaryFilterParam2}"
                        scrollUpLocked="{!scrollUpLocked}"
                        vestedAsOf="{!currCycle.Vested_As_Of__c}" />

        <c:cat_lazyloader currCycle="{!currCycle}"
                          prevCycle="{!prevCycle}"
                          initialChunkSize="{!initialChunkSize}"
                          chunkSize="{!chunkSize}" />
    </head>
    <body>
        <apex:form styleClass="full-width" id="theForm">

            <apex:actionFunction name="clearSecondaryFilters" action="{!clearFilters}" rerender="mainPane, headerInfo" oncomplete="startChunking();" />

            <!-- Rerenders the page message which shows errors on the filter component -->
            <apex:actionFunction name="forcePageMessageRefresh" rerender="fib" />

            <!-- Rerenders the split list (Tags drawer) -->
            <apex:actionFunction name="rerenderSplits" rerender="splitSelector" />

            <!-- Initiate Apex which will save the current filter -->
            <apex:actionFunction name="saveFilter" action="{!saveFilter}" rerender="filterList, mainPane, headerInfo" oncomplete="startChunking();">
                <apex:param name="firstParam" value="" assignTo="{!filterName}" />
            </apex:actionFunction>

            <!-- HEADER -->
            <div id="header" class="full-width header-decoration">

                <div id="headerLeft">
                    <!--apex:image value="{!URLFOR($Resource.CAT_Assets, '/CAT_Assets/assets/internlogo.gif')}" /--><!-- styleClass="headerLeftImg" -->
                    <span>COMPENSATION</span>
                </div>

                <div id="headerCenter">

                    <div id="headerCenterRadio">
                        <div id="headerCenterRadioInner">
                            <span>Employee Name: <input type="radio" name="psearch" value="name" /></span><br />
                            <span>Manager Rollup: <input type="radio" name="psearch" value="chain" /></span>
                        </div>
                    </div>

                    <div id="primarySearchPanel">
                        <input type="text" id="searchAutoComplete" />
                    </div>

                    <div id="headerCenterButton">
                        <button class="submitBtn" onclick="$('#loadingImg').show();executePrimaryFilter();return false;">
                            <span>Search</span>
                        </button>
                        <button class="submitBtn" onclick="$('#loadingImg').show();clearPrimaryFilter();executePrimaryFilter();return false;">
                            <span>Clear</span>
                        </button>
                    </div>
                </div>

                <div id="headerRight">
                    <apex:outputPanel id="headerInfo" styleClass="headerRightVF">
                        <div class="tooltip1">Applied Filter&#58;&nbsp;{!appliedFilter}
                            <div style="position: relative; left: -200px;top:-5px;white-space:normal;">
                                <span style="width: 100px;">
                                    <apex:outputText value="{!IF(ISBLANK(filterDescription), 'Empty Filter', filterDescription)}" escape="false" />
                                </span>
                            </div>
                        </div>
                        <div id="recordCount">Employee Count: Loading...</div>
                    </apex:outputPanel>
                </div>

            </div>

            <!-- BODY -->
            <div id="parentDiv">

                <img id="loadingImg" class="loadingIcon" src="{!URLFOR($Resource.CAT_Assets, '/CAT_Assets/assets/oldLoading.gif')}"/>

                <!-- Flag/Unflag all DS/DE -->
                <div id="headerSelection">
                    <!-- Export Button -->
                    <apex:commandLink id="generateExcel2" rerender="hdnExportVal" onclick="allExport()" value="Export All" styleClass="export-link" />
                    <apex:commandLink id="generateExcel" rerender="hdnExportVal" value="Export" styleClass="export-link" />                    
                </div>

                <!-- Grey div at top which holds tabs -->
                <div id="tabs">

                    <div id="tabUL" >
                        <ul class="tabs">
                            <!-- Tabs repeated over here -->
                            <apex:repeat value="{!tabs}" var="tab" id="tabRepeater">
                                <li><a href="#{!tab.Name}">{!tab.Name}</a></li>
                            </apex:repeat>
                        </ul>

                    </div>
                </div>

                <!-- Container which holds grid and control panel -->
                <div id="mainContainer" class="shadowDiv">

                    <!-- SlickGrid! -->
                    <div class="tab_container shadowDiv" id="tabGroup">
                        <div id="tab1" class="tab_content full-width full-height">
                            <div id="myGrid"></div>
                            <div id="myReport" class="full-width full-height">
                                <div id="reportingTabBody">
                                    <span id="reportingTimeStamp">Loading...</span><br /><br />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Tab to open control panel -->
                    <div id="filterButtonContainer">
                        <div id="openCloseButton" class="stackedButton">
                            <div class="rotate90">
                                &nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filters
                            </div>
                        </div>

                        <div class="stackedButton stackedButtonDivider" />

                        <div id="clearSecondaryFiltersButton" class="stackedButton" onclick="$('#loadingImg').show();clearSecondaryFilters();">
                            <div class="rotate90">
                                &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clear Filters
                            </div>
                        </div>
                    </div>

                    <!-- Control Panel -->
                    <div id="controlPanel" class="shadowDiv">

                        <div id="cp-header">
                            &nbsp;&nbsp;Control Panel
                            <div id="cp-locker" onclick="lockUnlockSidebar();">
                                <img id="unlockIcon" src="{!URLFOR($Resource.CAT_Assets, '/CAT_Assets/assets/lockiconunlocked.png')}" />
                                <img id="lockIcon" src="{!URLFOR($Resource.CAT_Assets, '/CAT_Assets/assets/lockiconlocked.png')}" />
                            </div>
                        </div>

                        <div id="controlPanelOuter">
                            <div id="controlPanelInner">

                                <!-- Filters -->
                                <div>
                                    <h3 id="filterh3" class="black-text">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filters
                                    </h3>

                                    <div id="filterDiv">
                                        <!--  Filter Code Goes Here -->
                                        <apex:outputpanel id="filterList">
                                            <hr />
                                            <b>Global Filters</b><br /><br />
                                            <apex:repeat value="{!globalFilters}" var="f" >
                                                <div id="{!f.id}">
                                                    <apex:outputPanel >
                                                        <apex:commandLink styleClass="tooltip filter" action="{!applyFilter}" onclick="$('#loadingImg').show();" rerender="mainPane, headerInfo" status="filterStatus" oncomplete="startChunking();" value="{!f.Name}">
                                                            <apex:param name="filterId" value="{!f.id}" assignTo="{!filterId}"/>
                                                            <span>
                                                                <apex:outputText value="{!IF(ISBLANK(f.Description__c), 'Empty Filter', f.Description__c)}" escape="false" />
                                                            </span>
                                                        </apex:commandLink>
                                                    </apex:outputPanel>
                                                </div>
                                                <br />
                                            </apex:repeat>
                                            <br />
                                            <hr />
                                            <div class="full-width">
                                                <div class="left-align bold-text">My Filters</div>
                                                <div class="right-align">
                                                    <a onclick="$('#filterGenerateMessage').empty();$( '#createFilterDialog' ).dialog( 'open' ).parent().appendTo($('#thePage\\:theForm'));" id="newFilter" class="button">
                                                        <span>New Filter</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <br /><br /><br />
                                             <apex:repeat value="{!localFilters}" var="f">
                                                <div id="{!f.id}">
                                                    <apex:outputPanel >
                                                        <apex:commandLink styleClass="tooltip filter" action="{!applyFilter}" onclick="$('#loadingImg').show();" rerender="mainPane, headerInfo" status="filterStatus" oncomplete="startChunking();" value="{!f.Name}"  >
                                                            <apex:param name="filterId" value="{!f.id}" assignTo="{!filterId}"/>
                                                            <span >
                                                                <apex:outputText value="{!IF(ISBLANK(f.Description__c), 'Empty Filter', f.Description__c)}" escape="false" />
                                                            </span>
                                                        </apex:commandLink>
                                                    </apex:outputPanel>
                                                    <apex:commandLink rendered="{!f.Category__c != 'HRBPUI Last Filter'}"  value="[Remove]" styleClass="red-text right-align" action="{!deleteFilter}" onclick="$('#loadingImg').show();"
                                                                      oncomplete="$('#loadingImg').hide();" rerender="filterList" >
                                                        <apex:param name="filterId" value="{!f.id}"  />
                                                    </apex:commandLink>
                                                </div>
                                                <br />
                                            </apex:repeat>
                                            <br /><br /><br />
                                        </apex:outputpanel>
                                    </div>
                                </div>

                                <!-- Settings -->
                                <div>
                                    <h3 id="settingsh3" class="white-text">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Settings
                                    </h3>
                                    <div id="settingsDiv">
                                        <hr />
                                        <apex:outputLabel styleClass="bold-text">Grid Settings</apex:outputLabel><br /><br />
                                        <input type="checkbox" id="readOnlyHBox" checked="yes" />&nbsp;Enable editable highlight
                                        <br />
                                        <input type="checkbox" id="overHBox" checked="yes" />&nbsp;Enable override highlight
                                        <br />
                                        <input type="checkbox" id="discretionaryHBox" checked="yes" />&nbsp;Enable discretionary input highlight
                                        <br /><br />
                                        <hr />
                                        <apex:outputLabel styleClass="bold-text">Record Settings</apex:outputLabel><br /><br />
                                        <apex:outputPanel styleClass="left-align">
                                              <apex:commandLink styleClass="button" onClick="ryppleRecordUpdate();" rerender="ryppleRerender" id="ryppleRerender">
                                                <span> Work.com Record Update</span>
                                            </apex:commandLink>
                                        </apex:outputPanel>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <!-- Save Filter Dialog -->
            <div id="createFilterDialog" title="Create Filter">
                <apex:outputPanel id="mainPane">
                    <div>
                        <c:cat_filterinputbare CAT_filterInputManager="{!fim}" id="fib" />
                        <input type="hidden" id="soql" value="{!lastQuery}" />
                        <input type="hidden" id="filter-name" value="{!appliedFilter}" />
                    </div>

                    <div class="full-width" align="center">
                        <apex:commandLink styleClass="button" onclick="$('#loadingImg').show();" action="{!clearFilters}"
                                          rerender="mainPane, headerInfo" oncomplete="startChunking();">
                            <span>Clear Filters</span>
                        </apex:commandLink>
                        <apex:commandLink styleClass="button small-right-margin" onclick="$('#loadingImg').show();"
                                          action="{!resetAppliedFilter}" rerender="mainPane, headerInfo" oncomplete="startChunking();">
                            <span>Apply Filter</span>
                        </apex:commandLink>
                        <a onclick="$( '#saveFilterDialog' ).dialog( 'open' );" id="saveFilter" class="button small-right-margin">
                            <span>Save Filter</span>
                        </a>
                    </div>
                    <div id="filterGenerateMessage" class="typical-font full-width" align="left"/>
                </apex:outputPanel>
            </div>

            <!-- Hidden input used by export functionality -->
            <apex:inputHidden value="{!exportString}" id="hdnExportVal" />

            <!-- Popups and jQuery Dialogs -->

            <!-- Save Filter Dialog -->
            <div id="saveFilterDialog" title="Save Filter" class="typical-font">
                <label for="name">Name</label>
                <input type="text" name="name" id="saveFilterDialogName" class="text ui-widget-content ui-corner-all 90pct-width" /><br /><br />
            </div>

            <!-- IC / MGR Switch - Success - New Business Title Dialog -->
            <div id="icMgrSwitchTitleDialog" title="Please enter a Title">
                <label for="name">Title: </label>
                <input type="text" name="titleForSwitch" id="titleForSwitch" class="text ui-widget-content ui-corner-all" onkeypress="handleEnter();" />
            </div>

            <!-- IC / MGR Switch - Failure - Error Dialog -->
            <div id="NoJobWarningDialog" title="Data Missing!">
                <label for="name">Warning, could not find IC/MGR data in the system for this employee. </label>
            </div>

            <!--  Override increase too large Dialog  -->
            <div id="AboveCapDialog" title="Increase is above cap!">
                <label for="name">Discretionary salary increases over 15% require compensation approval. Do you wish to submit for approval? </label>
                <br/><br/>
                <label for="commentlabel">Comments: </label>
                <input type="text" name="Comments" id="capComment" class="90pct-width text ui-widget-content ui-corner-all" /><br /><br />
            </div>

            <!-- Mass Action Wait Message -->
            <div id="massActionBack" />
            <div id="performingMassAction">
                Performing action, please wait...
            </div>

            <!-- Employee Popup -->
            <div id="employeePopupBack" />

            <div id="employeePopup">
                <c:cat_employeepopup />
            </div>

            <!-- This just keeps using marginTop as the property to animate off of much simpler -->
            <div id="bottomSlideUpHelper" />
            <apex:outputPanel rendered="{!currCycle.Phase__c != 'Phase 5: Locked and archived'}">
                <!-- This is what actually slides up from the bottom -->
                <div id="bottomSlideUp">
                    <div id="bottomSlideUpGrappler">
                      <span class="tooltip1" style="float: left;margin-left: 10px;font-style: italic;">
                          FB Stock Price: $
                          <apex:outputText value="{0, number, ###.00}" >
                              <apex:param value="{!currCycle.Stock_Value_Lock_Phase__c}"/>
                          </apex:outputText>
                      </span>
                      <a id="bsugLink" class="white-text pointer">Expand</a>
                    </div>
                    <div class="left-align">
                        <table class="slideUpTable">
                            <tr >
                                <th class="slideUpCellLong"></th>
                                <th class="slideUpCellShort">Total</th>
                                <th class="slideUpCellShort">Average</th>
                            </tr>
                            <tr >
                                <td class="slideUpCellLong">Bonus Payouts</td>
                                <td id="fillInBonusPayouts" class="slideUpCellShort slideUpCell">0 USD</td>
                                <td id="fillInBonusPayoutsAvg" class="slideUpCellShort slideUpCell"></td>
                            </tr>
                            <tr id="su-merit-row">
                                <td class="slideUpCellLong">Merit Salary Increases (annualized)</td>
                                <td id="fillInMeritIncreases" class="slideUpCellShort slideUpCell">0 USD</td>
                                <td id="fillInMeritIncreasesAvg" class="slideUpCellShort slideUpCell">0.0% increase</td>
                            </tr>
                            <tr id="su-ds-row">
                                <td class="slideUpCellLong">DS Increases (annualized)</td>
                                <td id="fillInDSIncreases" class="slideUpCellShort slideUpCell">0 USD</td>
                                <td id="fillInDSIncreasesAvg" class="slideUpCellShort slideUpCell">0.0% increase</td>
                            </tr>
                            <tr >
                                <td class="slideUpCellLong">Promotional Salary Increases (annualized)</td>
                                <td id="fillInPromoInc" class="slideUpCellShort slideUpCell">0 USD</td>
                                <td id="fillInPromoIncAvg" class="slideUpCellShort slideUpCell">0.0% increase</td>
                            </tr>
                            <tr id="su-refresh-row">
                                <td class="slideUpCellLong">Refresher Equity Granted</td>
                                <td id="fillInRefresherGranted" class="slideUpCellShort slideUpCell">0 USD<br />(0 RSUs)</td>
                                <td id="fillInRefresherGrantedAvg" class="slideUpCellShort slideUpCell"></td>
                            </tr>
                            <tr id="su-de1-row">
                                <td class="slideUpCellLong">Tier 1 Discretionary Equity Granted</td>
                                <td id="fillInDE1Granted" class="slideUpCellShort slideUpCell">0 USD<br />(0 RSUs)</td>
                                <td id="fillInDEGrantedAvg" class="slideUpCellShort slideUpCell"></td>
                            </tr>
                            <tr id="su-de2-row">
                              <td class="slideUpCellLong">Tier 2 Discretionary Equity Granted</td>
                              <td id="fillInDE2Granted" class="slideUpCellShort slideUpCell">0 USD<br />(0 RSUs)</td>
                              <td id="fillInDEGrantedAvg" class="slideUpCellShort slideUpCell"></td>
                          </tr>
                        </table>
                        <span id="bsu-totals">Totals reflect people shown in grid above</span>
                    </div>
                    <table class="slideUpTable left-align">
                        <tr>
                            <th></th>
                            <th># of people</th>
                            <th>% of eligible</th>
                        </tr>
                        <tr >
                            <td class="slideUpCellLong">Promotions</td>
                            <td id="fillInPromoCount" class="slideUpCellShort slideUpCell">0 people</td>
                            <td id="fillInPromoPct" class="slideUpCellShort slideUpCell">0% of eligible</td>
                        </tr>
                        <tr id="su-de1-count">
                            <td class="slideUpCellLong">Employees Receiving Tier 1 DE</td>
                            <td id="fillInDE1Count" class="slideUpCellShort slideUpCell">0 people</td>
                            <td id="fillInDE1Pct" class="slideUpCellShort slideUpCell">0% of eligible</td>
                        </tr>
                        <tr id="su-de2-count">
                          <td class="slideUpCellLong">Employees Receiving Tier 2 DE</td>
                          <td id="fillInDE2Count" class="slideUpCellShort slideUpCell">0 people</td>
                          <td id="fillInDE2Pct" class="slideUpCellShort slideUpCell">0% of eligible</td>
                      </tr>
                        <tr id="su-ds-count">
                            <td class="slideUpCellLong">Employees Receiving DS</td>
                            <td id="fillInDSCount" class="slideUpCellShort slideUpCell">0 people</td>
                            <td id="fillInDSPct" class="slideUpCellShort slideUpCell">0% of eligible</td>
                        </tr>
                    </table>
                </div>
            </apex:outputPanel>

            <div id="addRowDialog" title="Add Row">
                <a href="" tabindex="1"></a>
                <input type="text" id="analyticsAutoComplete" tabindex="2" />
                <br /><br />
                filter for people under the following manager only:<br />
                <input type="text" id="analyticsLoopTypeahead" />
                <input type="hidden" id="analyticsHiddenInput" />
            </div>
        </apex:form>

        <!-- Export Downloadify Popup -->
        <div id="fbPopup">
            <div id="fbPopupHeader">
                Export
                <div id="fb-popup-close" onclick="closeExportPopup();">close</div>
            </div>
            <div id="fbPopupBody">
                <br /><br /><br />
                Click <i>Download</i> to download the export.
            </div>
            <div id="fbPopupFooter"></div>
        </div>
    </body>

    <!-- And at the very bottom, more javascript
         It's located here because it needs to be called here since
         it needs to be called after buildSlickGrid is called above,
         which is called as the page is loading, before DOM ready -->

    <c:cat_ondomready isTierUser="{!isTierUser}"
                      primaryFilterParam2="{!primaryFilterParam2}"
                      scrollUpLocked="{!scrollUpLocked}"
                      primaryFilterParam="{!primaryFilterParam}"
                      locked="{!locked}"
                      currCycle="{!currCycle}" />
</apex:page>