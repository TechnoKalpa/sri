public abstract class CAT_AbstractSearchStatement {
    
    // The list of CAT_AbstractCriterion which make up the where clause.
    public List<CAT_AbstractCriterion> criteria {get; set;}
    
    /**
     * @description Adds an CAT_AbstractCriterion to the where clause.
     * @param       CAT_AbstractCriterion criterion - the criterion to add to the where clause.
     */
    public void addCriterion(CAT_AbstractCriterion criterion) {
        criteria.add(criterion);
    }
    
    /**
     * @description Turns the statement into a SOQL query.
     * @return String - the soql query in string format.
     */
    public abstract String toSOQL();
    
}