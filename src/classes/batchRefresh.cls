global class batchRefresh implements Database.Batchable<Sobject>, Database.Stateful  {

    global final string query;
    global CommonDMLResults unsuccessfuls;
 

        global batchRefresh(String q){
            Query=q;
        }

        global Database.QueryLocator start(Database.BatchableContext BC){
            unsuccessfuls= new CommonDMLResults();

            return Database.getQueryLocator(query);
        }
        global void execute(Database.BatchableContext BC, LIST<SObject> scope){

            unsuccessfuls.add(Database.update(scope,false),scope);
        }

        global void finish(Database.BatchableContext BC) {

            unsuccessfuls.batchOnFinish(BC.getJobId(),false);

        }
}