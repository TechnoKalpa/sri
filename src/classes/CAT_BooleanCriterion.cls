public class CAT_BooleanCriterion extends CAT_AbstractCriterion {
    
    private Boolean value;
    
    /**
     * @description Constructor for a filter statement on a field with data type boolean.
     * @param       String aName - the name of the filter statement.
     * @param       String op - the operator in the filter statement.
     * @param       Boolean aValue - the value in the filter statement.
     */
    public CAT_BooleanCriterion(String aName, String op, Boolean aValue) {
        name = aName;
        operator = op;
        value = aValue;
    }
    
    /** 
     * @description Mutator method for the value of the filter statement.
     * @param       Object aValue - the new value
     */ 
    public override void setValue(Object aValue) {
        value = Boolean.valueOf(aValue);
    }

    /** 
     * @description Combines the name, operator, and value to create a boolean filter statement within the where clause for a SOQL query.
     * @return      String - the criterion in SOQL.
     */     
    public override String toSOQL() {
        return name + ' ' + operator + ' ' + value;
    }

    /** 
     * @description Accessor method for the value of the filter statement.
     * @return      Object - the value of the criterion.
     */     
    public override Object getValue() {
        return value;
    }
}