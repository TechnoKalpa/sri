public class CAT_DateCriterion extends CAT_AbstractCriterion {

    private Date value;

    /**
     * @description Constructor for a filter statement on a field with data type date.
     * @param       String aName - the name of the filter statement.
     * @param       String op - the operator in the filter statement.
     * @param       Date aValue - the value in the filter statement.
     */
    public CAT_DateCriterion(String aName, String op, Date aValue) {
        name = aName;
        operator = op;
        value = aValue;
    }
    
    /** 
     * @description Mutator method for the value of the filter statement.
     * @param       Object aValue - the new value
     */     
    public override void setValue(Object aValue) {
        value = Date.valueOf(aValue);
    }

    /** 
     * @description Combines the name, operator, and value to create a date filter statement within the where clause for a SOQL query.
     * @return      String - the criterion in SOQL.
     */     
    public override String toSOQL() {
        return name + ' ' + operator + ' ' + String.valueOf(value);
    }

    /** 
     * @description Accessor method for the value of the filter statement.
     * @return      Object - the value of the criterion.
     */ 
    public override Object getValue() {
        return value;
    }

}