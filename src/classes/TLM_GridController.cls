global with sharing class TLM_GridController {
  private static final Integer CHUNK_SIZE    = 100;//Integer.valueOf(System.Label.CAT_Chunk_Size); //Number of results per chunk
  private static final Integer INIT_CHUNK_SIZE = 10;//Integer.valueOf(System.Label.CAT_Initial_Chunk_Size); //Initial chunk size
  private static final String FIRST = 'Ready Now';
  private static final String SECOND = '6-12 months';
  private static final String THIRD = '12-18 months';
  private static final String FOURTH = '18+ months';
  public String TLM_RECORDTYPE='REcordtype';
  public CAT_FilterInputManager fim    {get; set;} //Controls the FilterInput component
  public CAT_Review_Cycle__c currCycle {get; set;} //The current review cycle
  public CAT_Review_Cycle__c prevCycle {get; set;} //The previous review cycle
  public Boolean scrollUpLocked        {get; set;}
  public String filterDescription      {get; set;} //Filter description on mouse over of the filter
  public transient String exportString {get; set;} //Used by export functionality, holds table downloads as .xlsx
  public Boolean isCompAdmin           {get; set;} //True if user is in comp admin group
  public Boolean isTierUser            {get; set;}
  public String filterName             {get; set;} //Used by "Save Filter" functionality.  Holds the users desired name for the filter.
  public String appliedFilter          {get; set;} //Currently applied filter
  public Boolean locked                {get; set;} //True when cycle is in phase 1 or 5
  public String focalPrefix            {get; set;} //First three digits of focal file ids.  Used by typeahead in reporting tab
  public String primaryFilterParam     {get; set;} //Contains data typed into primary filter
  public String primaryFilterParam2    {get; set;} //Contains value of radio button associated with primary filter
  public Date startingEquityDate       {get; set;} //Contains the date shown for starting of the employee popup
  public String pageLoadTime           {get; set;}
  public String  lastQuery             {get; set;}
  public String yearDefaultValue       {get; set;}
  public CAT_Grid_Tab__c[] popUpTabs {get; private set;}
  public TLM_Settings__c settings {
    get {
      return TLM_Settings__c.getInstance();
    }
  }

  public List<SelectOption> strengthType {
    get {
      if (this.strengthType == null) {
        this.strengthType = new List<SelectOption>();
        for (Schema.PicklistEntry p :
          TLM_Strengths__c.Strength_1_Type__c.getDescribe(
          ).getPicklistValues()){

          this.strengthType.add(new SelectOption(p.getLabel(), p.getValue()));
        }
      }
     return this.strengthType;
    }

    set;
  }

  public List<SelectOption> aspirType {
    get {
      if (this.aspirType == null) {
        this.aspirType = new List<SelectOption>();
        for (Schema.PicklistEntry p :
            TLM_Strengths__c.Aspiration_1_Type__c.getDescribe(
            ).getPicklistValues()){

          this.aspirType.add(new SelectOption(p.getLabel(), p.getValue()));
        }
      }
      return this.aspirType;
    }
    set;
  }

  public List<SelectOption> supportType {
    get {
      if (this.supportType == null) {
        this.supportType = new List<SelectOption>();
        for (Schema.PicklistEntry p :
            TLM_Strengths__c.Strength_1_Support_Type__c.getDescribe(
            ).getPicklistValues()){

          this.supportType.add(new SelectOption(p.getLabel(), p.getValue()));
        }
      }
      return this.supportType;
    }
    set;
  }

  public List<SelectOption> courseOptions {
    get {
      if (this.courseOptions == null) {
        this.courseOptions = new List<SelectOption>();
        this.courseOptions.add(new SelectOption('',''));
        for (Schema.PicklistEntry p :
          TLM_Strengths__c.L_D_Course_1__c.getDescribe().getPicklistValues()) {

          this.courseOptions.add(new SelectOption(p.getLabel(), p.getValue()));
        }
      }
      return this.courseOptions;
    }
    set;
  }

  public List<SelectOption> years {
    get {
      if (this.years == null) {
        this.years = new List<SelectOption>();
        /*for (TLM_IDP_Year__c y : [SELECT Year__c, Default__c
          FROM TLM_IDP_Year__c ORDER BY Year__c ASC]) {

          this.years.add(new SelectOption(y.year__c, y.year__c));
          if (y.Default__c) {
            yearDefaultValue = y.Year__c;
          }
        }*/
      }
      return this.years;
    }
    set;
  }

  public List<SelectOption> developmentPlanTypes {
    get {
      if (this.developmentPlanTypes == null) {
        this.developmentPlanTypes = new List<SelectOption>();
        Set<String> developmentTypes;
        /* =
          TLM_DevelopmentPlanUtil.getAvailableDevelopementTypes();*/

        for (String type : developmentTypes) {
          this.developmentPlanTypes.add(new SelectOption(type, type));
        }
      }
      return this.developmentPlanTypes;
    }
    set;
  }

  private Boolean createAppData;           //True when the user has never been in HRBPUI before and they need an app data record created.
  private CAT_Application_Data__c appData; //The app data record for the current user
  private String splitValue;               //Value of tag user has clicked on

  public static final String OVERRIDE_SET = 'CAT_Override_Set__c';
  public static final String FOCAL_HEADER = 'TLM_Focal_Header__c';
  public static final String TLM_EMPLOYEE = 'TLM_Employee__c';

  /**
   * @description Constructor for TLM_GridController.  Instantiates the filter input manager.
   **/
  global TLM_GridController() {
    List<CAT_Review_Cycle__c> reviewCycle;/* =
      CAT_HrbpUIUtil.getCurrentReviewCycle(true);
      */
    if(!reviewCycle.isEmpty()) {
        currCycle = reviewCycle[0];
    }

    loadPrevCycle();

    primaryFilterParam2 = 'name';
    isTierUser      = true;

    focalPrefix  = TLM_Employee__c.SObjectType.getDescribe().getKeyPrefix();
    pageLoadTime = '' + Datetime.now().addMinutes(-2);

    //Load CAT_Application_Data__c record
    loadData();

    //Set the applied filter to none, then check app data record to see if there there is a last applied filter,
    //If there was, apply it.
    appliedFilter = '*None*';
    filterName = '';

    /*fim = new CAT_FilterInputManager(TLM_EMPLOYEE, TLM_GridUtil.getFieldsToSelect(), Integer.valueOf(System.Label.CAT_FIM_Minimum),
                     Integer.valueOf(System.Label.CAT_FIM_Maximum), getDefaultWhere(), true);
  */
    //Load in saved filtering information
    if(appData.Primary_Filter_Option__c != null) {
      primaryFilterParam2 = appData.Primary_Filter_Option__c;
    }

    if(appData.Primary_Filter__c != null && appData.Primary_Filter__c.trim() != '') {
      primaryFilterParam = appData.Primary_Filter__c;
    }

    if(appData.Saved_Filter__c != null) {
      applySpecificFilter(appData.Saved_Filter__c, true);
    }

    //Determine if user is comp admin
    List <GroupMember> compAdminGroup = new List <GroupMember> ([SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.Name = 'Comp Admin' and Group.Type = 'Queue' AND UserOrGroupId =: UserInfo.getUserId()]);
    isCompAdmin = compAdminGroup.size() > 0;

    lastQuery = fim.getSOQLQuery();
    popUpTabs = [Select Id, Name, Tab_Order__c, Active__c, Switch_Order__c, CSS_id__c, CSS_Class__c from CAT_Grid_Tab__c where RecordType.Name = 'TLM Pop up Tabs' order by Tab_Order__c];
  }

  /**
   * @description getter for chunk size
   **/
  public Integer getChunkSize() {
    return CHUNK_SIZE;
  }

  /**
   * @description getter for initial chunk size
   **/
  public Integer getInitialChunkSize() {
    return INIT_CHUNK_SIZE;
  }

  /**
   * @description Loads the CAT_Application_Data__c record for the current user.  If it doesnt exist, createAppData is flagged.
   **/
  private void loadData() {
    transient List<CAT_Application_Data__c> appDataList = [
      SELECT Id, Saved_Filter__c, Primary_Filter__c, Primary_Filter_Option__c, Slide_Up_Locked__c
      FROM CAT_Application_Data__c
      WHERE OwnerId = :UserInfo.getUserId()
      AND RecordType.Name = :TLM_RECORDTYPE];

    if(appDataList.isEmpty()) {
      appData = new CAT_Application_Data__c(OwnerId = UserInfo.getUserId());
      Id rtId ;//= TLM_GridUtil.getRecordType(appData,TLM_RECORDTYPE);
      appData.RecordTypeId = rtId;
      createAppData = true;
    } else {
      createAppData = false;
      appData = appDataList[0];
      scrollUpLocked = appDataList[0].Slide_Up_Locked__c;
    }

    if(appData.Saved_Filter__c == null) {
      transient List<CAT_Filter_Group__c> defaultFilters = [SELECT Id
        FROM CAT_Filter_Group__c
        WHERE Category__c = 'HRBPUI Default Filter'
        AND RecordType.Name = :TLM_RECORDTYPE];

      if(defaultFilters.isEmpty()) {
        appData.Saved_Filter__c = null;
      } else {
        appData.Saved_Filter__c = defaultFilters[0].Id;
      }
    }
  }

  private void loadPrevCycle() {
    if (prevCycle != null) {
      return;
    }
    List<CAT_Review_Cycle__c> reviewCycles = [
      select Id
      from CAT_Review_Cycle__c
      where Phase__c = 'Phase 5: Locked and archived'
      order by End_Date__c desc
      limit 1];

    if (!reviewCycles.isEmpty()) {
      prevCycle = reviewCycles.get(0);
    }
  }

  /**
   * @description When flagged to createAppData, this method will insert a new app data record for the user.
   **/
  public void createAppData() {
    if(createAppData) {
      insert appData;
      createAppData = false;
    }
  }

  /**
   * @description Gets the basic where clause, to be modified by the FIM.
   * @return      The basic where clause.
   **/
  private String getDefaultWhere() {
      return 'WHERE Active__c = true' +
            ' ORDER BY Full_Name__c asc';
  }

  /**
   * @description Gets the users tabs.
   * @return      List<CAT_Grid_Tab__c> - the users tabs.
   **/
  public List<CAT_Grid_Tab__c> getTabs() {
    Id recordTypeId ;/*= TLM_GridUtil.getRecordType(new CAT_Grid_Tab__c(),
    TLM_RECORDTYPE);*/
    return [SELECT Name FROM CAT_Grid_Tab__c
    WHERE Active__c = true AND
    Default__c = false AND
    OwnerId = :UserInfo.getUserId() AND
    RecordTypeId = :recordTypeId
    Order by Tab_Order__c asc];
  }

  /**
   * @description Gets global filters defined for the org.
   * @return      List<CAT_Filter_Group__c> - all defined global filters.
   **/
  global List<CAT_Filter_Group__c> getGlobalFilters(){
       return [SELECT CAT_Filter_Group__c.Name, CAT_Filter_Group__c.Category__c,CAT_Filter_Group__c.Description__c
              FROM CAT_Filter_Group__c
              WHERE (Category__c = 'HRBPUI Global Filter'
                 OR Category__c = 'HRBPUI Default Filter')
                AND RecordType.Name = :TLM_RECORDTYPE
              order by Name asc];
  }

  /**
   * @description Gets local filters defined for the user.
   * @return      List<CAT_Filter_Group__c> - local filters defined for the user.
   **/
  global List<CAT_Filter_Group__c> getLocalFilters(){
    return [SELECT Name, Description__c, Category__c FROM CAT_Filter_Group__c
            WHERE (Category__c = 'HRBPUI Local Filter'
               OR Category__c = 'HRBPUI Last Filter')
               AND OwnerId = :UserInfo.getUserId()
               AND RecordType.Name = :TLM_RECORDTYPE
            order by Category__c asc, Name asc];
  }

  /**
   * @description Builds out a tooltip for the current filter.
   **/
  private void determineFilterTooltip() {
    filterDescription = '';

    transient Map<String, String> filterNameToLabel = new Map<String, String>();
    for(SelectOption so : fim.filterFields) {
      filterNameToLabel.put(so.getValue(), so.getLabel());
    }

    //Load the values back in
    CAT_Filter__c currentFilter;
    for(CAT_FilterInputManager.FilterOption fo : fim.foList) {
      if (fo.optionValues.Name != null && fo.optionValues.Name.length() > 0 && fo.optionValues.Name != '--None--'
                       && fo.optionValues.getValue() != null
                       && fo.optionValues.operator   != null
                       && fo.optionValues.operator   != '--None--') {

        filterDescription += filterNameToLabel.get(fo.optionValues.Name) + ' ' + fo.optionValues.operator + ' ' +
                   String.valueOf(fo.optionValues.getValue()) + '\n';
      }
    }

    if(filterDescription != '') {
      filterDescription = filterDescription.substring(0, filterDescription.length() - 1);
    }
  }

  /**
   * @description Applies a specific filter.  Loads data into FIM.  FIM filter is then applied by a rerender of dataTableBuilder.
   * @param     Id specificId  - id of filter to apply
   * @param     Boolean onLoad - when the filter is applied at a time other than onLoad, update the app data record to save the last used filter.
   **/
  private void applySpecificFilter(Id specificId, Boolean onLoad) {
    try{
      transient List<CAT_Filter_Group__c> filterGroups = [
        SELECT Advanced_Criteria__c, Name, Associated_Head__c,Description__c,
               (SELECT Name, Label__c, Field__c, Operator__c, Value__c, Active__c, Tag_Count__c From Filters__r)
        FROM CAT_Filter_Group__c
        WHERE Id = :specificId
          AND RecordType.Name = :TLM_RECORDTYPE];

      if(!filterGroups.isEmpty()) {
        transient CAT_Filter_Group__c filterGroup = filterGroups[0];
        transient List<CAT_Filter__c> filters   = filterGroup.Filters__r;

         String myFilterDescription = '';

        //Reset the Filter Input Manager and add rows as necessary
        fim.clearFilters();

        while(filters.size() > fim.foList.size()) {
          fim.addRow();
        }

        //Map out filter numbers to the filters
        Map<Integer, CAT_Filter__c> filterNumberToFilter = new Map<Integer, CAT_Filter__c>();
        for(CAT_Filter__c filter : filters) {
          filterNumberToFilter.put(Integer.valueOf(filter.Tag_Count__c), filter);
        }

        //Load the values back in
        CAT_Filter__c currentFilter;

        for(CAT_FilterInputManager.FilterOption fo : fim.foList) {
          Integer currKey = Integer.valueOf(fo.filterNumber);
          if(filterNumberToFilter.containsKey(currKey)) {
            currentFilter = filterNumberToFilter.get(currKey);
            myFilterDescription += currentFilter.Label__c + ' ' + currentFilter.Operator__c + ' '+ currentFilter.Value__c + '\n';
            fo.optionValues.Name = currentFilter.Field__c;
            ApexPages.currentPage().getParameters().put('optionNumber', fo.filterNumber);
            fim.buildOperatorList();
            if(currentFilter.Value__c.toLowerCase() == 'true') {
               fo.optionValues.setValue('Yes');
            } else if(currentFilter.Value__c.toLowerCase() == 'false') {
               fo.optionValues.setValue('No');
            } else {
                fo.optionValues.setValue(currentFilter.Value__c);
            }
            fo.optionValues.operator = currentFilter.Operator__c;
          }
        }
        myFilterDescription = myFilterDescription.substring(0, myFilterDescription.length() -1 );
        //Still needs to handle advanced criteria
        if(filterGroup.Advanced_Criteria__c != null && filterGroup.Advanced_Criteria__c.trim() != '') {
          fim.enableAdvancedFilters = true;
          fim.advancedCriteria    = filterGroup.Advanced_Criteria__c;
        }

        appliedFilter = filterGroup.Name;
        filterDescription = filterGroup.Description__c;
        lastQuery = fim.getSOQLQuery();

        if(!onLoad) {
          createAppData();
          appData.Saved_Filter__c = filterGroup.Id;
          update appData;
        }

      }

    } catch(Exception e) { }
  }

  /**
   * @description Called when clicking on a saved filter.  Applies the saved filter.
   **/
  global void applyFilter() {
    applySpecificFilter((Id)ApexPages.currentPage().getParameters().get('filterId'), false);
  }

  /**
   * @description Deletes a saved filter.
   * @return      PageReference - required by action methods.
   **/
  public PageReference deleteFilter() {
    try{
      delete [SELECT Id FROM CAT_Filter_Group__c
        WHERE Id = :ApexPages.currentPage().getParameters().get('filterId')
        AND RecordType.Name = :TLM_RECORDTYPE];
    } catch(Exception e){
      return null;
    }

    return null;
  }

  /**
   * @description Resets the applied filter message to one which is appropriate for the "Apply Filter" button.
   *        Updates the user's CAT_Application_Data__c record to null out the field which tracks last applied filter.
   **/
  public void resetAppliedFilter() {
    determineFilterTooltip();
    lastQuery   = fim.getSOQLQuery();
    appliedFilter = '*Unsaved Filter*';
    saveApplied();
  }

  /**
   * @description Clear the select filter and change the app data record to null out the "last used filter" field.
   **/
  public void clearFilters() {
    createAppData();
    appData.Saved_Filter__c = null;
    update appData;
    fim.clearFilters();
    lastQuery     = fim.getSOQLQuery();
    appliedFilter   = '*None*';
    filterDescription = 'No Filter Applied';
  }

  public void saveApplied() {
    System.SavePoint sp = Database.setSavePoint();

    try {
      if(appliedFilter == '*Unsaved Filter*') {
        delete [SELECT Id FROM CAT_Filter_Group__c
          WHERE Category__c = 'HRBPUI Last Filter'
          AND OwnerId = :UserInfo.getUserId()
          AND RecordType.Name = :TLM_RECORDTYPE];

        //Create and insert header
        CAT_Filter_Group__c lastApplied = new CAT_Filter_Group__c( Category__c      = 'HRBPUI Last Filter',
                                       OwnerId        = UserInfo.getUserId(),
                                       Advanced_Criteria__c = fim.advancedCriteria,
                                       Name         = 'My Last Unsaved Filter' );

        insert lastApplied;

        //Create and insert children
        insert getFiltersFromInputManager(lastApplied.Id);

        createAppData();
        appData.Saved_Filter__c = lastApplied.Id;
        update appData;
      }
    } catch(Exception ex) {
      Database.rollback(sp);
    }
  }

  private List<CAT_Filter__c> getFiltersFromInputManager(Id groupId) {
    //Save the filters themselves
    CAT_Filter__c tempFilter;
    transient List<CAT_Filter__c> filters = new List<CAT_Filter__c>();
    transient Map<String, String> filterNameToLabel = new Map<String, String>();
    for(SelectOption so : fim.filterFields) {
      filterNameToLabel.put(so.getValue(), so.getLabel());
    }

    for(CAT_FilterInputManager.FilterOption fo : fim.foList) {
      if (fo.optionValues.Name != null && fo.optionValues.Name.length() > 0 && fo.optionValues.Name != '--None--'
                       && fo.optionValues.getValue() != null
                       && fo.optionValues.operator   != null
                       && fo.optionValues.operator   != '--None--') {

        tempFilter = new CAT_Filter__c();
        tempFilter.Active__c     = true;
        tempFilter.Filter_Group__c = groupId;
        tempFilter.Field__c    = fo.optionValues.Name;
        tempFilter.Label__c    = filterNameToLabel.get(tempFilter.Field__c);
        tempFilter.Operator__c   = fo.optionValues.operator;
        tempFilter.Value__c    = String.valueOf(fo.optionValues.getValue());
        tempFilter.Tag_Count__c  = Integer.valueOf(fo.filterNumber);

        filters.add(tempFilter);
      }
    }

    return filters;
  }

  /**
   * @description Saves the current state of the filter component.
   **/
  global void saveFilter() {
    String tempQuery = fim.getSOQLQuery();
    if( tempQuery == null ) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Your filters did not save because there are errors in the critera.<br />' + fim.errorDisplay));
      return;
    }

    CAT_Filter_Group__c newFilter;
    System.SavePoint sp = Database.setSavePoint();

    try {
      // If Advanced Criteria has been given, validate it
      if (fim.enableAdvancedFilters && fim.advancedCriteria.length() >= 1) {
        Set<String> numbersUsed = new Set<String>();

        for (CAT_FilterInputManager.FilterOption fo : fim.foList) {
          numbersUsed.add(fo.filterNumber);
        }

        if (numbersUsed.size() >= 1 && !fim.verifyAdvancedCriteria(numbersUsed)) {
          throw new HRBPUIException('Advanced Criteria is not valid.');
        }
      }

      //Create Filter with filterName, Category of HRBPUI Local, and ownerid of the current user
      newFilter = new CAT_Filter_Group__c();
      newFilter.Name         = filterName;
      newFilter.Category__c      = 'HRBPUI Local Filter';
      newFilter.OwnerId        = UserInfo.getUserId();
      newFilter.Advanced_Criteria__c = fim.advancedCriteria;
      newFilter.RecordTypeId =null;//= TLM_GridUtil.getRecordType(newFilter,TLM_RECORDTYPE);
      newFilter.Associated_Head__c = null;

      //coupleWithHead = false;
      insert newFilter;

      appliedFilter = filterName;
      lastQuery = tempQuery;
      determineFilterTooltip();

      transient List<CAT_Filter__c> filters = getFiltersFromInputManager(newFilter.Id);

      if(filters.isEmpty()) {
        throw new HRBPUIException('You cannot save a filter that does not filter.');
      } else {
        insert filters;
        createAppData();
        appData.Saved_Filter__c = newFilter.Id;
        update appData;
        insert new CAT_Metric__c(
          Application__c = 'TLM',
          Type__c     = 'Created a Filter',
          Current_User__c = UserInfo.getUserId(),
          Details__c    = 'Filter Name: ' + appliedFilter);
      }

    } catch (Exception e) {
      fim.errorDisplay = e.getMessage();
      Database.rollback(sp);
    }
  }

  //This is not logging right.
  //Seems to be logging the first twice as often as it should
  private static void logSearchMetrics(String content,
                     String type,
                     String filterName) {

    List<CAT_Metric__c> metrics = new List<CAT_Metric__c>();

    if(String.isNotEmpty(content)) {
    metrics.add(new CAT_Metric__c(
      Application__c = 'TLM',
      Details__c    = 'Searched On: ' + content,
      Current_User__c = UserInfo.getUserId(),
      Type__c     = type == 'name' ? 'Primary Search By Name' :
                       'Primary Search By Mgt Chain'));
    }

    if(filterName != '*None*' &&
     filterName != '*Unsaved Filter*' &&
     filterName != 'My Last Unsaved Filter') {

    metrics.add(new CAT_Metric__c(
      Application__c = 'TLM',
      Details__c    = 'Filter Name: ' + filterName,
      Current_User__c = UserInfo.getUserId(),
      Type__c     = 'Used a Filter'));
    }

    insert metrics;
  }

  global class LMSData implements Comparable {
    public String sessionEndDate {get; set;}
    public String courseName     {get; set;}
    public String status         {get; set;}
    public DateTime endDateTime  {get; set;}

    public LMSData(Date endDate, String courseName, String status) {
      this.endDateTime = endDate == null ? null :
        DateTime.newInstance(endDate.year(), endDate.month(), endDate.day());
      this.sessionEndDate = endDateTime == null ? '' :
        endDateTime.format('yyyy-MM-dd');
      this.courseName = courseName;
      this.status = status;
    }

    global Integer compareTo(Object compareTo) {
      LMSData compareToLMS = (LMSData)compareTo;
      if (endDateTime == null) {
        return -1;
      } else if (compareToLMS.endDateTime == null) {
        return 1;
      } else {
        return endDateTime.getTime() > compareToLMS.endDateTime.getTime() ? -1 :
          endDateTime.getTime() < compareToLMS.endDateTime.getTime() ? 1 :
          courseName.compareTo(compareToLMS.courseName);
      }
    }
  }

  //This is used for retrieving the historical data for an employee.  Since this
  //data now will hold the label instead of API name, a separate class is used
  global class HistoricalActivityData {
    public String fieldName   {get; set;}
    public DateTime changeDate  {get; set;}
    public String oldFieldValue {get; set;}
    public String newFieldValue {get; set;}
    public String alias     {get; set;}

    public HistoricalActivityData(String fieldName, DateTime changeDate, String oldFieldValue, String newFieldValue, String alias){
      this.fieldName    = fieldName;
      this.changeDate   = changeDate;
      if (oldFieldValue == 'false' ||
        oldFieldValue == 'true'){
          this.oldFieldValue = oldFieldValue == 'true' ? 'Yes' : 'No';
      } else {
        this.oldFieldValue  = oldFieldValue;
      }
      if (newFieldValue == 'false' ||
        newFieldValue == 'true'){
          this.newFieldValue = newFieldValue == 'true' ? 'Yes' : 'No';
      } else {
        this.newFieldValue  = newFieldValue;
      }
      this.alias      = alias;
    }

  }

  //This is used for retrieving the historical performance data for an employee.  Since this
  //data lives in multiple location, a separate global class is used.
  global class PerformanceData {
    public String period    {get; set;}
    public String assessment  {get; set;}
    public String promotion   {get; set;}
    public String DE   {get; set;}
    public String level   {get; set;}

    public PerformanceData(String period, String assessment,
      String promotion, String DE, String level){
      this.period   = period;
      this.assessment = assessment == '' || assessment == null ?
        'No Assessment': assessment;
      this.promotion  = promotion  == null ? '-' : promotion;
      this.DE = DE == null ? '-' : DE;
      this.level  = level  == null ? '-' : level;
    }

    public PerformanceData(String period, String assessment,
      String promotion, String DE){
      this.period   = period;
      this.assessment = assessment == '' || assessment == null ?
        'No Assessment': assessment;
      this.promotion  = promotion  == null ? '-' : promotion;
      this.DE = DE == null ? '-' : DE;
    }
  }

  global class Chunk {
    public List<ChunkRow> rows    {get; set;}
    public Integer    start   {get; set;}
    public Integer    expected  {get; set;}
    public String     queryCode {get; set;}

    public Chunk(List<ChunkRow> chunkRows, Integer startAt,
           Integer expectedRows, String queryKey) {

    rows    = chunkRows;
    start   = startAt;
    expected  = expectedRows;
    queryCode = queryKey;
    }
  }

  global class ChunkRow {

    public TLM_Employee__c record {get; set;}
    public String      extras {get; set;}

    public ChunkRow(TLM_Employee__c focalFile, String extra) {
    record = focalFile;
    extras = extra;
    }
  }

  global class DevelopmentGoalWrapper {
    public TLM_Development_Goal__c goal {get; set;}
    public Integer linkNum {get; set;}

    public DevelopmentGoalWrapper(TLM_Development_Goal__c goal, Integer linkNum) {
      this.goal = goal;
      this.linkNum = linkNum;
    }
  }

  global class SuccessorGoalsWrapper implements Comparable {
    public TLM_Successor__c successor {get; set;}
    public List<TLM_Development_Goal__c> goals {get; set;}

    public SuccessorGoalsWrapper(TLM_Successor__c successor,
      List<TLM_Development_Goal__c> goals) {

      this.successor = successor;
      this.goals = goals;
    }

    // -1 if our object is earlier in sort list
    global Integer compareTo(Object compareTo) {
      SuccessorGoalsWrapper compareToSuc = (SuccessorGoalsWrapper)compareTo;
      if ('FER' == FIRST) {
        if ('TEST'== FIRST) {
          // if both are the same, sort by createddate
          return getFirstCreatedDate(this.successor.CreatedDate.getTime(),
            compareToSuc.successor.CreatedDate.getTime());
        } else {
          return -1;
        }
      } else if ('SEC' == SECOND) {
        if ('TEST' == FIRST) {
          return 1;
        } else if ('SEC' == SECOND) {
          return getFirstCreatedDate(this.successor.CreatedDate.getTime(),
            compareToSuc.successor.CreatedDate.getTime());
        } else {
          return -1;
        }
      } else if ('THR' == THIRD) {
        if ('TEst' == FIRST ||
          'SEC' == SECOND) {

          return 1;
        } else if ('THir' == THIRD) {
          return getFirstCreatedDate(this.successor.CreatedDate.getTime(),
            compareToSuc.successor.CreatedDate.getTime());
        } else {
          return -1;
        }
      } else {
        if ('FOURTH' == FOURTH) {
          return getFirstCreatedDate(this.successor.CreatedDate.getTime(),
            compareToSuc.successor.CreatedDate.getTime());
        } else {
          return 1;
        }
      }
    }

    private Integer getFirstCreatedDate(Long firstTime, Long secondTime) {
      return firstTime > secondTime ? 1 : -1;
    }
  }

  /** CUSTOM EXCEPTIONS **/

  private class HRBPUIException extends Exception {}

  /** REMOTE ACTIONS **/

  @RemoteAction
  global static TLM_Employee__c[] getEmployeeNames() {
    return null;//TLM_GridUtil.getEmployeeNames();
  }

  /**
   * @description Gets all columns.  Used by column picker.
   * @return      CAT_Grid_Column__c[] - list of all columns in the system
   **/
  @RemoteAction
  global static CAT_Grid_Column__c[] getAllColumns() {
    return null;//TLM_GridUtil.getAllColumns();
  }

   /**
   * @description Gets all tab column couplers for a user
   * @param       String activeTab      - the currently selected tab.
   * @return      CAT_Tab_Column_Coupler__c[] - all tab column couplers for a user
   **/
  @RemoteAction
  global static CAT_Tab_Column_Coupler__c[] getCouplers(String activeTab) {
    return null;//TLM_GridUtil.getCouplers(activeTab);
  }

  /**
   * @description Method is fired when user tabs out of a cell on the HRBP UI.  Saves the modified data,
   *        and requeries for the saved data, to get the update in formulas, and returns the results.
   * @param     String sobject  - the type of sobject that the field specified in the columnName parameter exists on.
   * @param     String dataType   - the data type of the field specified by the columnName.
   * @param     String recordId   - the id of the record to update data on.
   * @param     String headerId   - the id of the header to update data on.
   * @param     String columnName - the field api name to update data in.
   * @param     String newValue   - the new value for the field.
   * @return    CAT_Focal_File__c - The focal file which has been requeried for.
   **/
  @RemoteAction
  global static TLM_Employee__c saveToServer(String sobjectName,
    String dataType, String recordId, String headerId, String columnName,
    String newValue) {

    return null;//TLM_GridUtil.saveToServer(sobjectName, dataType, recordId, headerId,columnName, newValue);
  }

  @RemoteAction
  global static SuccessorGoalsWrapper[] getSuccessors(String employeeId) {
    return null;//TLM_GridUtil.getSuccessors(employeeId);
  }

  @RemoteAction
  global static SuccessorGoalsWrapper[] updateSuccessor(String employeeId,
    String oldSuccessor, String newSuccessor, String readiness) {

    if (employeeId == null) {
    throw new TLM_GridController.HRBPUIException('EmployeeId isn\'t set.');
    }
    return null;//TLM_GridUtil.updateSuccessor(employeeId, oldSuccessor, newSuccessor, readiness);
  }

  @RemoteAction
  global static void updateFocalHeader(String employeeId, String fieldName, String value, Boolean isBoolean) {
   // TLM_GridUtil.updateFocalHeader(employeeId, fieldName, value, isBoolean);
  }

  @RemoteAction
  global static List<CAT_Performance_Plan_LKUP__c> getMetrics(String reviewCycleId) {
    return null;//TLM_GridUtil.getMetrics(reviewCycleId);
  }


   /**
   * @description Finds and returns all historical data for this employee
   * @param       String focalId - Id so we know which historical data to bring back
   * @return      HistoricalActivityData - List of results
   **/
  @RemoteAction
  global static List<HistoricalActivityData> retrieveHistoricalData(String focalId) {
    return null;//TLM_GridUtil.retrieveHistoricalData(focalId);
  }

  @RemoteAction
  global static List<LMSData> retrieveLMSData(String employeeId) {
    return null;//TLM_GridUtil.retrieveLMSData(employeeId);
  }

   /**
   * @description  Finds and returns the historical performance data for the Employee
   * @param        String workdayId  - employee workday id to get history for
   * @return       List<PerformanceData> - performance history
   **/
  @RemoteAction
  global static List<PerformanceData> retrievePerformanceHistoricalData(String workdayId) {
    return null;//CAT_WithoutSharingUtil.retrievePerformanceHistoricalData(workdayId);
  }

  /**
   * @description Gets the default set of data for analytics.
   * @return      List<CAT_Graphable_Pair__c> - aggregated data ready for analytics to process.
   **/
  @RemoteAction
  global static List<CAT_Graphable_Pair__c> getDefaultTableData() {
    return null;//TLM_GridUtil.getDefaultTableData();
  }

  @RemoteAction
  global static List<DevelopmentGoalWrapper> getDevelopmentGoals(Id employeeId) {
    return null;//TLM_GridUtil.getDevelopmentGoals(employeeId);
  }

  @RemoteAction
  global static Map<String, TLM_Strengths__c> getStrengths(Id tlmId) {
    return null;//TLM_GridUtil.getStrengths(tlmId);
  }

  @RemoteAction
  global static void updateStrength(Id tlmId, String key,
    String field, String value, String year) {

    //TLM_GridUtil.updateStrengths(tlmId, key, field, value, year);
  }

  @RemoteAction
  global static List<DevelopmentGoalWrapper> saveDevelopmentGoals(String developmentGoals, Id employeeId) {
    return null;//TLM_GridUtil.saveDevelopmentGoals((List<DevelopmentGoalWrapper>)Json.deserialize(developmentGoals, List<DevelopmentGoalWrapper>.class), employeeId);
  }

  /**
   * @description Saves the slide up's position to the db
   * @param     Lock position - true is up, false is down
   **/
  @RemoteAction
  global static void updateSlideUpLock(Boolean lockPosition) {
    //TLM_GridUtil.updateSlideUpLock(lockPosition);
  }

  /**
   * @description Get fully up-to-date focal file data
   * @param       List<String> ffIds   - focals to retrieve
   * @return      String         - requested focals as json
   **/
  @RemoteAction
  global static String getFocalFiles(List<String> ffIds, Double currStock) {
    return null;//TLM_GridUtil.getFocalFiles(ffIds, currStock);
  }

  @RemoteAction
  global static String getSuccessorsAndRelated(List<String> ffIds,
    Double currStock) {

    return null;//TLM_GridUtil.getSuccessorsAndRelated(ffIds, currStock);
  }

  @RemoteAction
  global static Chunk loadRows(Double currStock, String  query,
                String  queryCode,
                Integer rowsLoaded,
                Integer rowsExpected,
                String  primaryFilterContent,
                String  primaryFilterType,
                String  filterName) {

    String  firstQuery  = query;
    Integer start     = rowsLoaded;
    String  secondQuery =
      'SELECT ' +
      //TLM_GridUtil.getSelectClause(TLM_GridUtil.getFieldsToSelect2()) +
      ' FROM ' + TLM_EMPLOYEE + ' WHERE ' + firstQuery.split('WHERE', 2)[1];

    if(rowsExpected == -1) {
      List<String> queryList = query.split(' WHERE ', 2);
      rowsExpected = Database.countQuery(
        'SELECT count() FROM ' + TLM_EMPLOYEE + ' WHERE ' +
        queryList[1].split(' ORDER BY', 2)[0]);
    }

    if (rowsExpected == 0) {
      return new Chunk(new List<ChunkRow>(), 0, 0, queryCode);
    }

    List<ChunkRow> chunkRows   = new List<ChunkRow>();
    Map<Id, TLM_Employee__c> idToFocal = new Map<Id, TLM_Employee__c>(
      (List<TLM_Employee__c>) Database.query(secondQuery));

    List<TLM_Employee__c> focals =
      (List<TLM_Employee__c>)Database.query(firstQuery);
    Map<Id, List<TLM_Development_Goal__c>> empGoalsMap =
      new Map<Id, List<TLM_Development_Goal__c>>();
    /*for(TLM_Development_Goal__c dg : [SELECT Past_Due__c, TLM_Employee__c
      FROM TLM_Development_Goal__c WHERE TLM_Employee__c IN :focals AND
      Past_Due__c = true]) {

      List<TLM_Development_Goal__c> goals = empGoalsMap.get(dg.TLM_Employee__c);
      if(goals == null) {
        goals = new List<TLM_Development_Goal__c>();
        empGoalsMap.put(dg.TLM_Employee__c, goals);
      }
      goals.add(dg);
    }
  */
    for(TLM_Employee__c focal : focals) {
      List<TLM_Development_Goal__c> goals = empGoalsMap.get(focal.Id);
      /*chunkRows.add(new ChunkRow(
        idToFocal.get(focal.Id),
        TLM_GridUtil.getComplexJson(
        ++rowsLoaded,
        focal,
        currStock,
        goals)));*/
    }

    if(rowsLoaded == rowsExpected) {
      /*TLM_GridUtil.updatePrimaryFilters(
        primaryFilterContent, primaryFilterType);*/

      logSearchMetrics(primaryFilterContent, primaryFilterType, filterName);
    }

    return new Chunk(chunkRows,
             start,
             rowsExpected,
             queryCode);
  }

  @RemoteAction
  global static CAT_Equity_Aggregate__c[] updateAndGetEquityData(
    String employeeId, String currencyIsoCode) {
    TLM_Employee__c emp = [SELECT CAT_Employee__c FROM TLM_Employee__c
      WHERE Id = :employeeId];
    return null;//TLM_GridUtil.updateAndGetEquityData(emp.CAT_Employee__c, currencyIsoCode);
  }

  @RemoteAction
  global static CAT_Equity_Aggregate__c[] getExistingVested(String empId) {
    TLM_Employee__c emp = [SELECT CAT_Employee__c FROM TLM_Employee__c
      WHERE Id = :empId];
    return null;//TLM_GridUtil.getExistingVested(emp.CAT_Employee__c, '');
  }

  @RemoteAction
  global static CAT_Focal_Archive__c[] getFocalArchives(String employeeId) {
    TLM_Employee__c emp = [SELECT CAT_Employee__c FROM TLM_Employee__c
      WHERE Id = :employeeId];
    return null;//TLM_GridUtil.getFocalArchives(emp.CAT_Employee__c);
  }
}