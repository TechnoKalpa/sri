/**
 * @author      Model Metrics {Phil Rymek}
 * @date        3/22/11
 * @description Contains methods useful when creating test classes.
 **/
public with sharing class CAT_TestClassUtilities4 {
    public static String CAT_ADMIN_PROFILE = 'Facebook Developer Admin - CAT Contingent Admin';

    /**
     * @description Creates a new assessment history record
     * @param       String assessment - rypple assessment
     * @param       Id empId          - id of employee this record belongs to
     * @param       Date startDate    - start date of review cycle for this entry
     * @param       Date endDate      - end date of review cycle for this entry
     * @param       Boolean recPromo  - true if employee received promotion for this cycle
     * @param       String rcName     - name of cycle
     **/
    public static CAT_Assessment_History__c createAssessmentHistory(String assessment, String empWorkdayId, Date startDate, Date endDate, String recPromo, String rcName) {
        CAT_Assessment_History__c testAh = new CAT_Assessment_History__c(Assessment_Title__c = assessment, Employee_Id__c = empWorkdayId,
                                                                         Cycle_Start__c = startDate, Cycle_End__c = endDate,
                                                                         Received_Promotion__c = recPromo, Review_Cycle_Name__c = rcName);
        return testAh;
    }

    /**
     * @description Creates a CAT_Performance_Plan_LKUP__c
     * @param       String name                  - Name of this CAT_Performance_Plan_LKUP__c
     * @param       String level                 - level of this CAT_Performance_Plan_LKUP__c
     * @param       Id planId                    - id of parent plan
     * @return      CAT_Performance_Plan_LKUP__c - returned, inserted CAT_Performance_Plan_LKUP__c
     **/
    public static CAT_Performance_Plan_LKUP__c createPerformanceLookup(String name, String level, Id planId) {
        CAT_Performance_Plan_LKUP__c testLKUP = new CAT_Performance_Plan_LKUP__c(Name = name, Level__c = level, Program__c = planId);

        insert testLKUP;
        return testLKUP;
    }

    /**
      * @description Creates new Equity By Grant Records.
      * @return      The created Records.
      **/
  public static CAT_Equity_by_Grant__c  createEquityByGrant(String employeeId, String grantId){
    CAT_Equity_by_Grant__c testEqByGrant = new CAT_Equity_by_Grant__c(CAT_Employee__c = employeeId, Grant_Id__c = grantId);

    insert testEqByGrant;
    return testEqByGrant;
  }


    /**
      * @description Creates new Equity By Grant Records.
      * @return      The created Records.
      **/
  public static CAT_Equity_by_Grant__c createEquityByGrant(String employeeId,
                                                          String grantId,
                                                          Date grantDate,
                                                          Integer qty,
                                                          Double awardPrice) {

    CAT_Equity_by_Grant__c testEqByGrant = new CAT_Equity_by_Grant__c();
    testEqByGrant.Grant_Id__c     = grantId;
    testEqByGrant.CAT_Employee__c = employeeId;
    testEqByGrant.Grant_Date__c   = grantDate;
    testEqByGrant.Vesting_Qty__c  = qty;
    testEqByGrant.Award_Price__c  = awardPrice;
    insert testEqByGrant;
    return testEqByGrant;
  }

    public static CAT_Equity_Aggregate__c createEquityAggregate(Date month,
                                                                Integer exist,
                                                                Integer discr,
                                                                Integer refresh,
                                                                Integer reduct,
                                                                Id employeeId) {
                                                                    
      CAT_Equity_Aggregate__c aggregate = new CAT_Equity_Aggregate__c();
      aggregate.Month__c                    = month;
      aggregate.Existing_Vesting__c         = exist;
      aggregate.DE_Vesting__c               = discr;
      aggregate.Refresher_Vesting__c        = refresh;
      aggregate.Vesting_Reduction_Amount__c = reduct;
      aggregate.CAT_Employee__c             = employeeId;
      
      insert aggregate;
      return aggregate;
    }

     /**
      * @description Creates a user for run as.
      * @return      The created user.
      **/
     public static User createUser() {
        Profile p = [select id from profile where name=:CAT_ADMIN_PROFILE limit 1];
        User testUser = new User(alias = 'standt', email='standarduser@salesforce.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, firstname='phil',
            timezonesidkey='America/Los_Angeles', username='standarduser@salesforce.com');
        insert testUser;
        return testUser;
     }

     /**
      * @description Creates a user with employee number.
      * @param       String incEmployeeNumber - employee number.
      * @return      The created user.
      **/
     public static User createUserWithEmployeeNumber(String incEmployeeNumber) {
        Profile p = [select id from profile where name=:CAT_ADMIN_PROFILE limit 1];
        User testUser = new User(alias = 'standt', email='standarduser@salesforce.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, firstname='phil',
            employeeNumber= incEmployeeNumber,
            timezonesidkey='America/Los_Angeles', username='standarduser@salesforce.com');
        insert testUser;
        return testUser;
     }

      /**
       * @description Creates a user with employee number and given username.
       * @param       String incEmployeeNumber - employee number.
       * @param       String incUserName - Username.
       * @return      The created user.
       **/
      public static User createUserWithEmployeeNumber(String incUserName, String incEmployeeNumber) {
        Profile p = [select id from profile where name=:CAT_ADMIN_PROFILE limit 1];
        User testUser = new User(alias = 'standt', email='standarduser@salesforce.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, firstname='phil',
            timezonesidkey='America/Los_Angeles', isActive = true, username= incUserName,  employeeNumber= incEmployeeNumber);
        insert testUser;
        return testUser;
     }

      /**
       * @description Creates a user with employee number and given username.
       * @param       String incEmployeeNumber - employee number.
       * @param       String incUserName - Username.
       * @return      The created user.
       **/
      public static User createUserWithEmployeeNumAndValidate(String incUserName, String incEmployeeNumber) {
        List<User> userCheck = [SELECT Id,
                                        alias,
                                        email,
                                        lastname,
                                        firstname,
                                        profileId,
                                        isActive,
                                        username,
                                        employeeNumber
                                        FROM User
                                        WHERE employeeNumber =: incEmployeeNumber limit 1];
        if (userCheck.size() > 0){
            return userCheck[0];
        }
        Profile p = [select id from profile where name=:CAT_ADMIN_PROFILE limit 1];
        User testUser = new User(alias = 'standt', email='standarduser@salesforce.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, firstname='phil',
            timezonesidkey='America/Los_Angeles', isActive = true, username= incUserName,  employeeNumber= incEmployeeNumber);
        insert testUser;
        return testUser;
     }

      /**
      * @description Creates a user with admin profile.
      * @return      The created user.
      **/
      public static User createSuperUser(String workdayId) {
        Profile p = 
            [select id
               from profile
              where name='System Administrator' limit 1];
        User testUser = new User(
            alias = 'standt', 
            email='standarduser@salesforce.com',
            emailencodingkey='UTF-8', 
            lastname='Testing', 
            languagelocalekey='en_US',
            localesidkey='en_US', 
            profileid = p.Id, 
            firstname='phil',
            timezonesidkey='America/Los_Angeles', 
            username='standarduser@salesforce.com',
            employeenumber = workdayId);
        insert testUser;
        return testUser;
     }

     /**
      * @description Creates a user with admin profile.
      * @return      The created user.
      **/
      public static User createSuperUser() {
        Profile p = [select id from profile where name='System Administrator' limit 1];
        User testUser = new User(alias = 'standt', email='standarduser@salesforce.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, firstname='phil',
            timezonesidkey='America/Los_Angeles', username='standarduser@salesforce.com');
        insert testUser;
        return testUser;
     }


    /**
     * @description Creates the dummy contact used by letter generation.
     * @return      The created contact.
     *
    public static Contact createDummyContact(String workdayId) {
        // Focal File requires a dummy contact where no valid Contacts can be found for the Focal File's Workday ID
        Account newAcct = TestClassUtilities.createAccount();
        Contact newContact =  new Contact(LastName = 'Unit Test', AccountId = newAcct.Id, External_Contact_Id__c = workdayId);
        insert newContact;
        insert new JSO2__File__c(JSO2__Employee_Name__c = newContact.Id,
                           Referred_By__c = 'Dummy Contact',
                           Referred_By_Email__c = 'dc@fb.com',
                           Referral_Type__c = 'Regular',
                           Referral_Bonus_Type__c = 'Non-tech entry-level'
                           );
        return newContact;
    }*/

    /**
     * @description Create Filter
     * @param       String label - Label of filter.
     * @param       String field - Filed for filter.
     * @param       String operator - operator on field.
     * @param       String String value - Value from user.
     * @param       String Id filterGroupId - Filter group.
     * @return      CAT_Filter__c - New filter.
     **/
    public static CAT_Filter__c createFilter(String label, String field, String operator, String value, Id filterGroupId) {
        CAT_Filter__c testFilter = new CAT_Filter__c(
            Label__c            = label
            , Field__c          = field
            , Operator__c       = operator
            , Value__c          = value
            , Filter_Group__c   = filterGroupId
        );

        insert testFilter;
        return testFilter;
    }

    /**
    * @description  Create Filter Group
    * @param        String advancedCriteria - combination of all criteria.
    * @param        String category - Category of filter group.
    * @return       CAT_Filter_Group__c - New flter group.
    **/
    public static CAT_Filter_Group__c createFilterGroup(String advancedCriteria, String category, Id rcId) {
        CAT_Filter_Group__c testFilter = new CAT_Filter_Group__c(Advanced_Criteria__c = advancedCriteria, Category__c = category);

        insert testFilter;
        return testFilter;
    }

    /**
    * @description  Create Filter Group
    * @param        String advancedCriteria - combination of all criteria.
    * @param        String category - Category of filter group.
    * @return       CAT_Filter_Group__c - New flter group.
    **/
    public static CAT_Filter_Group__c createFilterGroup(String advancedCriteria, String category) {
        CAT_Filter_Group__c testFilter = new CAT_Filter_Group__c(Advanced_Criteria__c = advancedCriteria, Category__c = category);

        insert testFilter;
        return testFilter;
    }

    /**
    * @description  Create details for a Review Letter Template
    * @return       CAT_Review_Letter_Template_Detail__c -  new CAT_Review_Letter_Template_Detail__c.
    **/
  /*  public static CAT_Review_Letter_Template_Detail__c createReviewCycleLetterDetail(String name, Decimal displayOrder, String language, String text, Id templateId, Id filterGroupId) {
        CAT_Review_Letter_Template_Detail__c testDetail = new CAT_Review_Letter_Template_Detail__c(
            Name                            = name
            , Display_Order__c              = displayOrder
            , Language__c                   = language
            , CAT_Review_Letter_Template__c = templateId
            , Text__c                       = text
            , CAT_Filter_Group__c           = filterGroupId
        );

        insert testDetail;
        return testDetail;
    }
*/
    /**
    * @description  Create details for a Review Letter Template.
    * @param        String name - Name of the template.
    * @param        String category - Category of CAT_Review_Letter_Template_Detail__c.
    * @param        String language - Langyage of template.
    * @param        String text - text of template details.
    * @param        String text - text of template details.
    * @return       Id templateId - Id of the letter template..
    **/
    public static CAT_Review_Letter_Template_Detail__c createReviewCycleLetterDetail(String name, Decimal displayOrder, String language, String text, Id templateId) {
        CAT_Review_Letter_Template_Detail__c testDetail = new CAT_Review_Letter_Template_Detail__c(
            Name                            = name
            , Display_Order__c              = displayOrder
            , Language__c                   = language
            , CAT_Review_Letter_Template__c = templateId
            , Text__c                       = text
        );

        insert testDetail;
        return testDetail;
    }

    /**
    * @description Create a new Review Cycle Letter Template
    * @return      The created CAT_Review_Letter_Template__c.
    **/
    public static CAT_Review_Letter_Template__c createReviewCycleLetterTemplate(String name) {
        CAT_Review_Letter_Template__c testTemp = new CAT_Review_Letter_Template__c(Name = name);

        insert testTemp;
        return testTemp;
    }


    /**
     * @description Obtains the id for a record type.
     * @param       The name of the record type to obtain the id for.
     * @return      The id of the record type obtained.
     **/
    public static Id getRecordTypeId(String name) {
        return [SELECT Id FROM RecordType WHERE DeveloperName = :name].Id;
    }

    /**
     * @description Creates a plan sobject.
     * @param       String name - the name of the plan
     * @param       String  cycleId - rypple id
     * @return      The created Plan__c.
     **/
    public static CAT_Review_Cycle__c createPlan(String name, String cycleId) {
        CAT_Review_Cycle__c testPlan = new CAT_Review_Cycle__c(
          Name = name,
          Rypple_Review_Cycle__c = cycleId,
          End_Date__c = System.today().addYears(101),
          Phase__c = 'Phase 1: No Access');
        insert testPlan;
        return testPlan;
    }

    public static CAT_Review_Cycle__c createPlan(String name,
                          String cycleId,
                          String phase) {
        CAT_Review_Cycle__c testPlan = new CAT_Review_Cycle__c(
          Name = name,
          Rypple_Review_Cycle__c = cycleId,
          Phase__c = phase,
          End_Date__c = System.today().addYears(101));
        insert testPlan;
        return testPlan;
    }


    /**
     * @description Creates a program sobject.
     * @param       String name - the name of the program.
     * @return      The created Program__c.
     **/

    public static CAT_Plan__c createProgram(String name, Double companyMultiplier, Id planId, Id recordTypeId) {
        CAT_Plan__c testProgram = new CAT_Plan__c(Name = name, Company_Multiplier__c = companyMultiplier,
                                                Default_Merit_Incr_Country__c = 'US1',
                                                CAT_Review_Cycle__c = planId, RecordTypeId = recordTypeId);
        insert testProgram;
        return testProgram;
    }


     public static boolean gluOverride = true;

     public static CAT_Employee__c createEmployee(String firstName, String lastName, Id jobToBandId) {
        CAT_Employee__c testEmployee = new CAT_Employee__c(First_Name__c = firstName, Last_Name__c = lastName, Hire_Date__c = System.today().addYears(100));

        //CAT_GenerateLetterUtil.isRunning = gluOverride;
        insert testEmployee;
        //CAT_GenerateLetterUtil.isRunning = false;
        return testEmployee;
     }


       public static CAT_Employee__c createEmployee(String firstName, String lastName, Id jobToBandId, String workdayId) {
        CAT_Employee__c testEmployee = new CAT_Employee__c(Active__c = true, First_Name__c = firstName, Last_Name__c = lastName, Hire_Date__c = System.today().addYears(100), Workday_External_Id__c = workdayId );

        //CAT_GenerateLetterUtil.isRunning = gluOverride;
        insert testEmployee;
        //CAT_GenerateLetterUtil.isRunning = false;
        return testEmployee;
     }

      public static CAT_Employee__c createEmployee(String firstName, String lastName, Id jobToBandId, String workdayId, String jobCode, String gradeProfile) {
        CAT_Employee__c testEmployee = new CAT_Employee__c(Active__c = true, First_Name__c = firstName, Last_Name__c = lastName, Hire_Date__c = System.today().addYears(100), Job_Code__c = jobCode, Grade_Profile__c = gradeProfile);

        //CAT_GenerateLetterUtil.isRunning = gluOverride;
        insert testEmployee;
        //CAT_GenerateLetterUtil.isRunning = false;
        return testEmployee;
     }

     /**
      * @description        Creates a batch of Employees
      **/
     public static List<CAT_Employee__c> createEmployees(Map<String, String> firstNameToLastName) {
        List<CAT_Employee__c> testEmps = new List<CAT_Employee__c>();
        for (String firstName : firstNameToLastName.keySet()) {
            testEmps.add(new CAT_Employee__c(Active__c = true, Hire_Date__c = System.today().addYears(100), First_Name__c = firstName, Last_Name__c = firstNameToLastName.get(firstName)));
        }

        //CAT_GenerateLetterUtil.isRunning = gluOverride;
        insert testEmps;
        //CAT_GenerateLetterUtil.isRunning = false;
        return testEmps;
     }

     /**
      * @description Creates an focal file sobject.
      * @param       Id employeeId - the id of the employee for the focal file.
      * @param       Id planId     - the id of the plan for the focal file.
      * @return      Focal_File__c - the created focal file.
      **/

     public static CAT_Focal_File__c createFocalFile(Id employeeId, Id planId) {
        CAT_Focal_File__c testFocal = new CAT_Focal_File__c(Employee__c = employeeId, CAT_Review_Cycle__c = planId);

        //CAT_GenerateLetterUtil.isRunning = gluOverride;
        insert testFocal;
        //CAT_GenerateLetterUtil.isRunning = false;

        return testFocal;
     }

      /**
       * @description Creates a batch of Focals
       **/
     public static List<CAT_Focal_File__c> createFocalFiles(Set<Id> empIds, Id planId) {
        List<CAT_Focal_File__c> testFocals = new List<CAT_Focal_File__c>();
        for (Id empId : empIds) {
            testFocals.add(new CAT_Focal_File__c(Employee__c = empId, CAT_Review_Cycle__c = planId));
        }

        //CAT_GenerateLetterUtil.isRunning = gluOverride;
        insert testFocals;
        //CAT_GenerateLetterUtil.isRunning = false;

        return testFocals;
     }

     /**
      * @description Creates an entire table of promotion program lookup data for a given program.
      * @param       Id programId - the id of the program to create this table for.
      * @return      The table of promotion data.
      **/


     public static List<CAT_Promotion_Plan_LKUP__c> createPromotionPrograms(Id programId) {
        List<CAT_Promotion_Plan_LKUP__c> promotionTable = new List<CAT_Promotion_Plan_LKUP__c>();
        String planName = [SELECT CAT_Review_Cycle__r.Name FROM CAT_Plan__c WHERE Id = :programId].CAT_Review_Cycle__r.Name;

        promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '1', Promotion_Target__c = 70, Rypple_Assessment__c = '6',
                                                         Minimum_Increase_Promotion_Percentage__c = 0, Name = planName + '-PROMO-1'));
        promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '2', Promotion_Target__c = 70, Rypple_Assessment__c = '5',
                                                         Minimum_Increase_Promotion_Percentage__c = 0, Name = planName + '-PROMO-2'));
        promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '3', Promotion_Target__c = 75, Rypple_Assessment__c = '4',
                                                         Minimum_Increase_Promotion_Percentage__c = 0, Name = planName + '-PROMO-3'));
        promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '4', Promotion_Target__c = 80, Rypple_Assessment__c = '3',
                                                         Minimum_Increase_Promotion_Percentage__c = 5, Name = planName + '-PROMO-4'));
        promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '5', Promotion_Target__c = 90, Rypple_Assessment__c = '2',
                                                         Minimum_Increase_Promotion_Percentage__c = 8, Name = planName + '-PROMO-5'));
        promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '6', Promotion_Target__c = 110, Rypple_Assessment__c = '1',
                                                         Minimum_Increase_Promotion_Percentage__c = 10, Name = planName + '-PROMO-6'));
        promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '7', Promotion_Target__c = 115, Rypple_Assessment__c = '0',
                                                         Minimum_Increase_Promotion_Percentage__c = 20, Name = planName + '-PROMO-7'));
        promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '0', Promotion_Target__c = 115, Rypple_Assessment__c = 'None',
                                                         Minimum_Increase_Promotion_Percentage__c = 20, Name = planName + '-PROMO-8'));
         promotionTable.add(new CAT_Promotion_Plan_LKUP__c(Program__c = programId, Level__c = '0', Promotion_Target__c = 115, Rypple_Assessment__c = null,
                                                         Minimum_Increase_Promotion_Percentage__c = 0, Name = planName + '-PROMO-9'));
        insert promotionTable;
        return promotionTable;
     }





     /**
      * @description Creates an entire table of Individual Performance lookup data for a given program.
      * @param       Id programId - the id of the program to create this table for.
      * @return      The table of performance data.
      **/


     public static List<CAT_Performance_Plan_LKUP__c> createPerformancePrograms(Id programId) {
        List<CAT_Performance_Plan_LKUP__c> performanceTable = new List<CAT_Performance_Plan_LKUP__c>();


        performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '1',
                                                     Individual_Percent_Target_Modifier__c = 0, Rypple_Assessment__c = '6',
                                                     Name = 'Does Not Meet Expectations', Equity_Multiplier__c = 0));
        performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '2', Rypple_Assessment__c = '5',
                                                     Individual_Percent_Target_Modifier__c = 10,
                                                     Name = 'Meets Some Expectations', Equity_Multiplier__c = 10));
        performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '3', Rypple_Assessment__c = '4',
                                                     Individual_Percent_Target_Modifier__c = 50,
                                                     Name = 'Meets Most Expectations', Equity_Multiplier__c =  20));
        performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '4', Rypple_Assessment__c = '3',
                                                     Individual_Percent_Target_Modifier__c = 60,
                                                     Name = 'Meets All Expectations', Equity_Multiplier__c = 100));
        performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '5', Rypple_Assessment__c = '2',
                                                     Individual_Percent_Target_Modifier__c = 75,
                                                     Name = 'Exceeds Expectations', Equity_Multiplier__c = 150));
        performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '6', Rypple_Assessment__c = '1',
                                                     Individual_Percent_Target_Modifier__c = 100,
                                                     Name = 'Greatly Exceeds Expectations', Equity_Multiplier__c = 200));
        performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '7', Rypple_Assessment__c = '0',
                                                     Individual_Percent_Target_Modifier__c = 300,
                                                     Name = 'Redefines Expectations', Equity_Multiplier__c = 300));
         performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '0', Rypple_Assessment__c = 'None',
                                                     Individual_Percent_Target_Modifier__c = 0,
                                                     Name = 'NONE', Equity_Multiplier__c = 80));
          performanceTable.add(new CAT_Performance_Plan_LKUP__c(Program__c = programId, Level__c = '0', Rypple_Assessment__c = null,
                                                     Individual_Percent_Target_Modifier__c = 0,
                                                     Name = 'NONE', Equity_Multiplier__c = 80));

        insert performanceTable;
        return performanceTable;
     }

      /**
      * @description Creates an entire table of Individual Performance lookup data for a given program.
      * @param       Id programId - the id of the program to create this table for.
      * @return      The table of performance data.
      **/

     public static List<CAT_Merit_Matrix_Detail__c> createRangePositionPrograms(Id programId) {
        List<CAT_Merit_Matrix_Detail__c> rangeTable = new List<CAT_Merit_Matrix_Detail__c>();


        rangeTable.add(new CAT_Merit_Matrix_Detail__c(Rypple_Assessment__c = '0'
                                                        , Program__c = programId
                                                        , Name = '01'
                                                        , Min_range_pos__c =90
                                                        , Max_range_pos__c =100
                                                        , Ind_Merit_Mult__c = 100
                                                        , Country_Code__c = 'US1'));
        rangeTable.add(new CAT_Merit_Matrix_Detail__c(Rypple_Assessment__c = '1'
                                                        , Program__c = programId
                                                        , Name = '02'
                                                        , Min_range_pos__c= 90
                                                        , Max_range_pos__c =100
                                                        , Ind_Merit_Mult__c = 95
                                                        , Country_Code__c = 'US1'));
        rangeTable.add(new CAT_Merit_Matrix_Detail__c(Rypple_Assessment__c = '0'
                                                        , Program__c = programId
                                                        , Name = '03'
                                                        , Min_range_pos__c= 80
                                                        , Max_range_pos__c =90
                                                        , Ind_Merit_Mult__c = 85
                                                        , Country_Code__c = 'US1'));
        rangeTable.add(new CAT_Merit_Matrix_Detail__c(Rypple_Assessment__c = '1'
                                                        , Program__c = programId
                                                        , Name = '04'
                                                        , Min_range_pos__c=  80
                                                        , Max_range_pos__c = 90
                                                        , Ind_Merit_Mult__c =85
                                                        , Country_Code__c = 'US1'));
        insert rangeTable;
        return rangeTable;
     }

     /**
      * @description Creates an entire table of equity program lookup data for a given program.
      * @param       Id equityId - the id of the program to create this table for.
      * @return      The table of bonus data.
      **/
     public static List<CAT_Equity_Vesting_Detail__c> createEquityPrograms(Id programId) {
        List <CAT_Equity_Vesting_Detail__c> equityTable = new List<CAT_Equity_Vesting_Detail__c>();

        equityTable.add(new CAT_Equity_Vesting_Detail__c( Vesting_Type__c ='Employee', Starting_Date__c = date.newinstance(2011, 01, 01), Ending_Date__c = date.newinstance(2011, 12, 31), Period__c = System.today().addYears(101), Vesting_Amount__c = 5, CAT_Plan__c = programId));
        equityTable.add(new CAT_Equity_Vesting_Detail__c(Vesting_Type__c ='Employee', Starting_Date__c = date.newinstance(2012, 01, 01), Ending_Date__c = date.newinstance(2012, 12, 31), Period__c = System.today().addYears(102), Vesting_Amount__c = .5, CAT_Plan__c = programId));
        equityTable.add(new CAT_Equity_Vesting_Detail__c(Vesting_Type__c ='Employee', Period__c = System.today().addYears(103), Vesting_Amount__c = .5, CAT_Plan__c = programId));
        equityTable.add(new CAT_Equity_Vesting_Detail__c(Vesting_Type__c ='Employee', Period__c = System.today().addYears(104), Vesting_Amount__c = .5, CAT_Plan__c = programId));
        equityTable.add(new CAT_Equity_Vesting_Detail__c(Vesting_Type__c ='Employee', Period__c = System.today().addYears(105), Vesting_Amount__c = .5, CAT_Plan__c = programId));
        equityTable.add(new CAT_Equity_Vesting_Detail__c(Vesting_Type__c ='Employee', Period__c = System.today().addYears(106), Vesting_Amount__c = .5, CAT_Plan__c = programId));
        insert equityTable;
        return equityTable;
     }


     public static List<CAT_Equity_Vesting_Detail__c> initVestingTable(Id pId) {
        List<CAT_Equity_Vesting_Detail__c> equityTable =
            new List<CAT_Equity_Vesting_Detail__c>();

        equityTable.add(
            new CAT_Equity_Vesting_Detail__c(Vesting_Type__c  = 'Employee', 
                                             Period__c = 
                                                System.today().addYears(-1), 
                                             Vesting_Amount__c = 5, 
                                             CAT_Plan__c = pId)); 
        equityTable.add(
            new CAT_Equity_Vesting_Detail__c(Vesting_Type__c  = 'Employee', 
                                             Period__c = 
                                                System.today().addYears(1), 
                                             Vesting_Amount__c = 5, 
                                             CAT_Plan__c = pId));
        equityTable.add(
            new CAT_Equity_Vesting_Detail__c(Vesting_Type__c  = 'Employee', 
                                             Period__c = 
                                                System.today().addYears(2), 
                                             Vesting_Amount__c = 5, 
                                             CAT_Plan__c = pId));
        equityTable.add(
            new CAT_Equity_Vesting_Detail__c(Vesting_Type__c  = 'Employee', 
                                             Period__c = 
                                                System.today().addYears(3), 
                                             Vesting_Amount__c = 5, 
                                             CAT_Plan__c = pId)); 
        equityTable.add(
            new CAT_Equity_Vesting_Detail__c(Vesting_Type__c  = 'Employee', 
                                             Period__c = 
                                                System.today().addYears(4), 
                                             Vesting_Amount__c = 5, 
                                             CAT_Plan__c = pId)); 
        equityTable.add(
            new CAT_Equity_Vesting_Detail__c(Vesting_Type__c  = 'Employee', 
                                             Period__c = 
                                                System.today().addYears(5), 
                                             Vesting_Amount__c = 5, 
                                             CAT_Plan__c = pId));                                              
        insert equityTable;
        return equityTable;
     }

     /**
      * @description Creates a job to band junction object.
      * @param       String name       - name of the job to band.
      * @param       Id bandId         - the band for this job to band.
      * @param       Id profileId      - the profile for this job to band.
      * @return      JS_Job_to_Band__c - the created job to band object.
      **/
     public static RCT_JobToBand__c createJobToBand(String name, Id bandId, Id profileId) {

        RCT_JobToBand__c testJ2B = new RCT_JobToBand__c(Name = name, RCT_JobBand__c = bandId, RCT_JobProfile__c = profileId, Active__c = true);
        insert testJ2B;
        return testJ2B;
     }

     /**
      * @description Creates a job profile sobject.
      * @param       String name       - name of the job profile.
      * @param       String jobCode    - the job code.
      * @return      JS_Job_Profile__c - the created job profile sobject.
      **/
     public static RCT_JobProfile__c createJobProfile(String name, String jobCode, String jobLevel) {
        RCT_JobProfile__c testProfile = new RCT_JobProfile__c(Name = name, Job_Code__c = jobCode, Level__c = jobLevel);
        insert testProfile;
        return testProfile;
     }

     /**
      * @description Creates a compensation band.
      * @param       String name             - name of the compensation band.
      * @param       String countryCode      - the country code.
      * @return      JS_Compensation_Band__c - the created JS_Compensation_Band__c.
      **/
     public static RCT_JobBand__c createCompensationBand(String name, String countryCode, Double futureIncentiveMix,
                                                                  Double minSal, Double midSal, Double maxSal) {
        RCT_JobBand__c testBand = new RCT_JobBand__c(Name = name, Country_Code__c = countryCode,
                                                                       Salary_Min__c = minSal, Salary_Mid__c = midSal,
                                                                       Salary_Max__c = maxSal);
        insert testBand;
        return testBand;
     }

}