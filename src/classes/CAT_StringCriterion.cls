public class CAT_StringCriterion extends CAT_AbstractCriterion {
    
    private String value;

    /**
     * @description Constructor for a filter statement on a field with data type string.
     * @param       String aName - the name of the filter statement.
     * @param       String op - the operator in the filter statement.
     * @param       String aValue - the value in the filter statement.
     */ 
    public CAT_StringCriterion(String aName, String op, String aValue) {
        name = aName;
        operator = op;
        value = aValue;
    }

    /** 
     * @description Mutator method for the value of the filter statement.
     * @param       Object aValue - the new value
     */ 
    public override void setValue(Object aValue) {
        value = String.valueOf(aValue);
    }
    
    /** 
     * @description Combines the name, operator, and value to create a string filter statement within the where clause for a SOQL query.
     *              Allows for extra operators to be used: starts with, ends with, contains, does not contain, includes, and excludes.
     * @return      String - the criterion in SOQL.
     */         
    public override String toSOQL() {
        String tempValue = String.escapeSingleQuotes(value);
        String realOperator = operator;
        
        if(operator == 'starts with' || operator == 'ends with' || operator == 'contains' || operator == 'does not contain') {
            realOperator = 'like';
        }
        if(operator == 'starts with') {
            tempValue = tempValue + '%';
        } else if(operator == 'ends with') {
            tempValue = '%' + tempValue;
        } else if(operator == 'contains') {
            tempValue = '%' + tempValue + '%';
        } else if(operator == 'does not contain') {
            tempValue = '%' + tempValue + '%';
            return '(NOT ' + name + ' ' + realOperator + ' \'' + tempValue + '\'' + ')';
        } else if(operator == 'INCLUDES' || operator == 'EXCLUDES') {
            return name + ' ' + realOperator + ' (\'' + tempValue + '\')';
        }
        
        return name + ' ' + realOperator + ' \'' + tempValue + '\'';
    }
    
    /** 
     * @description Accessor method for the value of the filter statement.
     * @return      Object - the value of the criterion.
     */         
    public override Object getValue() {
        return value;
    }
}