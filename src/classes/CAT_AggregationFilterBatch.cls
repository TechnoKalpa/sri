global with sharing class CAT_AggregationFilterBatch implements Database.Batchable<SObject> {

    private Id                                 currCycleId;
    private Id                                 prevCycleId;
    private String                                   query;
    private List<CAT_Performance_Plan_LKUP__c> assessments;
    private Datetime                             startTime;
    private Boolean                                   isQ3;
    private CAT_Review_Cycle__c                  prevCycle;

    public static final Integer BATCH_SIZE = 2;
    private static final String Q3 = 'Q3';
    private static final String DEFAULT_AGGREGATION_QUERY =
        'SELECT Id, Advanced_Criteria__c FROM CAT_Filter_Group__c';
    private static final String NON_REGRET_TERM = 'non-regrettable termination';
    private static final String REGRET_TERM = 'regrettable termination';

    public class CATAnalyticsFilterException extends Exception {}

    global CAT_AggregationFilterBatch(Id currentCycle,
                                      Id asyncJobId,
                                      Datetime focalStartTime) {

        try {
            currCycleId    = currentCycle;
            prevCycleId    = currCycleId;
            query          = DEFAULT_AGGREGATION_QUERY +
                             ' WHERE RecordType.Name = \'' +
                             CAT_EmployeeTriggerUtil.CAT_RECORDTYPE +
                             '\' AND Category__c = \'HRBPUI Analytics Filter\'';

            List<CAT_Review_Cycle__c> rcs =
                [SELECT Id,
                        Quarter__c,
                        Phase__c,
                        End_Date__c
                   FROM CAT_Review_Cycle__c
               ORDER BY End_Date__c desc
                  LIMIT 2];

            isQ3 = false;

            if(rcs.isEmpty()) {
                throw new CATAnalyticsFilterException('No Review Cycles Found');
            } else if(rcs.size() == 1 || !rcs.get(0).Phase__c.contains('0')) {
                isQ3 = rcs[0].Quarter__c == Q3;
                prevCycle = rcs.get(0);
            } else if(rcs.get(0).Phase__c.contains('0')) {
                isQ3 = rcs[0].Quarter__c == Q3;
                prevCycleId = rcs.get(1).Id;
                prevCycle = rcs.get(1);
            }

            assessments =
                [SELECT Id,
                        Name,
                        Title__c,
                        Alternate_Title__c,
                        Reporting_Order__c
                   FROM CAT_Performance_Plan_LKUP__c
                  WHERE Program__r.Cat_Review_Cycle__c = :prevCycleId
               ORDER BY Reporting_Order__c asc];

            startTime   = focalStartTime;
        } catch(Exception ex) {
            CAT_ErrorHandler.handleDMLError(
                ex,
                null,
                'Aggregation Filter Batch - constructor',
                'Seed data.  [currCycle]=' + currentCycle);
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    /**
     * @description Executes one batch (200 records).
     *              General process is: iterate over focals, for each focal
     *              calculate name value pairs, bubble these pairs up.
     * @param       Database.BatchableContext bc - the context of the batch.
     * @return      List<SObject> scope - the set of records in this batch.
     */
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        try {
            List<CAT_Filter_Group__c> filterGroups =
                new List<CAT_Filter_Group__c>();

            for(SObject currObj : scope) {
                filterGroups.add( (CAT_Filter_Group__c) currObj );
            }

            //Map filtergroup id to list of filters
            Map<Id, List<CAT_Filter__c>> filterMap =
                new Map<Id, List<CAT_Filter__c>>();
            for(CAT_Filter__c filter : [SELECT Id,
                                               Filter_Group__c,
                                               Tag_Count__c,
                                               Field__c,
                                               Operator__c,
                                               Value__c
                                          FROM CAT_Filter__c
                                         WHERE Filter_Group__c in :filterGroups]
                                         ) {
                if(filterMap.containsKey(filter.Filter_Group__c)) {
                    filterMap.get(filter.Filter_Group__c).add(filter);
                } else {
                    filterMap.put(filter.Filter_Group__c,
                                  new List<CAT_Filter__c>{filter});
                }
            }

            //Create a FIM
            CAT_FilterInputManager fim =
                new CAT_FilterInputManager('CAT_Focal_File__c',
                                            new List<String>{'Id',
                                            'ae__c',
                                            'Promotion_Eligible__c',
                                            'Assessment_Title__c',
                                            'job_level__c',
                                            'Promotion_Flag__c',
                                            'prop_ds_approval__c',
                                            'prop_de_approval__c',
                                            'prop_de_tier__c',
                                            'Date_of_Termination__c',
                                            'CAT_Review_Cycle__r.End_Date__c',
                                            'Employee__r.Rypple_Chain_Exec__c'},
                                            25,
                                            25,
                                            ' AND (CAT_Review_Cycle__c = ' +
                                            ':currCycleId OR '             +
                                            'CAT_Review_Cycle__c = '       +
                                            ':prevCycleId)',
                                            false);

            //Create a list to hold name value pairs
            List<CAT_Graphable_Pair__c> pairsToInsert =
                new List<CAT_Graphable_Pair__c>();
            Set<String> inAssessments =
                new Set<String>
                    (System.Label.CAT_Exclude_From_Denominator.split(','));

            Set<String> inPromoAssess =
                new Set<String>
                    (System.Label.CAT_Exclude_From_Denom_Promo.split(','));

            //Iterate over each filter group
            for(CAT_Filter_Group__c currGroup : filterGroups) {
                //get the list for the fg from the map
                List<CAT_Filter__c> currentFilters =
                    filterMap.get(currGroup.Id);

                //instantiate the FIM with the data
                //from the filter group & filters
                /*CAT_AggregationUtil.instantiateFIM(fim,
                                                   currGroup,
                                                   currentFilters);*/

                //Iterate over results and create name value pairs
                List<CAT_Graphable_Pair__c> queryPairs =
                    new List<CAT_Graphable_Pair__c>();
                String currQuery = fim.getSOQLQuery();
                Double assessmentDenom = 0, promotionDenom = 0, otherDenom = 0;

                if(currQuery != null && currQuery.contains(' WHERE ')) {
                    //Build assessment count map
                    Map<String, Double> titleToValue =
                        new Map<String, Double>();
                    for(CAT_Performance_Plan_LKUP__c assessment : assessments) {
                        titleToValue.put(assessment.Title__c, 0);
                    }

                    Double promoCount = 0, kpCount = 0,
                           dsCount = 0, deCountTier1 = 0, deCountTier2 = 0;
                    for(CAT_Focal_File__c focal : Database.query( currQuery )) {
                        if(focal.Job_Level__c == null ||
                           focal.Job_level__c.trim() == '') {
                            continue;
                        }

                      Date prevCycleEnd = prevCycle != null ?
                          prevCycle.End_Date__c :
                          focal.CAT_Review_Cycle__r.End_Date__c;
                        if(!inAssessments.contains(focal.Assessment_Title__c) &&
                           focal.Assessment_Title__c != REGRET_TERM &&
                           !(focal.Assessment_Title__c == NON_REGRET_TERM &&
                           focal.Date_of_Termination__c > prevCycleEnd) &&
                           !focal.Employee__r.Rypple_Chain_Exec__c &&
                           focal.Assessment_Title__c != null &&
                           focal.Assessment_Title__c.trim() != '') {

                           assessmentDenom++;
                        }

                      if(!inPromoAssess.contains(focal.Assessment_Title__c) &&
                         /*focal.Promotion_Eligible__c == Label.CAT_Yes &&*/
                         !focal.Employee__r.Rypple_Chain_Exec__c) {

                         promotionDenom++;
                      }

                        if(focal.ae__c == System.Label.CAT_Yes) {
                            otherDenom++;
                        }

                        //Calculate assessments
                        /*for(String key :
                            CAT_AggregationUtil.calculateAssessmentsByFilter(
                            focal, assessments)) {

                            Double mapValue = titleToValue.get(key);
                            if((key != NON_REGRET_TERM && key != REGRET_TERM) ||
                              focal.Date_of_Termination__c <= prevCycleEnd) {

                               titleToValue.put(key, ++mapValue);
                           }
                        } comment*/

                        //Calculate promotions
                        if(/*focal.Promotion_Flag__c == System.Label.CAT_Yes &&*/
                         !focal.Employee__r.Rypple_Chain_Exec__c) {
                            promoCount++;
                        }

                        if(focal.prop_ds_approval__c == System.Label.CAT_Yes &&
                          !focal.Employee__r.Rypple_Chain_Exec__c) {
                            dsCount++;
                        }

                        if(focal.prop_de_approval__c == System.Label.CAT_Yes &&
                          !focal.Employee__r.Rypple_Chain_Exec__c) {
                            if(focal.prop_de_tier__c == 'Tier 1') {
                              deCountTier1++;
                            } else {
                              deCountTier2++;
                            }
                        }

                    }

                    for(CAT_Performance_Plan_LKUP__c assessment : assessments) {
                        pairsToInsert.add(
                            new CAT_Graphable_Pair__c(
                            Name = assessment.Alternate_Title__c,
                            Value__c = titleToValue.get(assessment.Title__c),
                            Value_Denominator__c = assessmentDenom,
                            CAT_Filter_Group__c = currGroup.Id,
                            Order__c = assessment.Reporting_Order__c));
                    }

                    pairsToInsert.add(new CAT_Graphable_Pair__c(
                        Name = System.Label.CAT_Analytics_Promotion,
                        Value__c = promoCount,
                        Value_Denominator__c = promotionDenom,
                        CAT_Filter_Group__c = currGroup.Id,
                        Order__c =
                        Integer.valueOf(
                        System.Label.CAT_Analytics_Promotion_Order
                        )));

                    if(!isQ3) {
                        pairsToInsert.add(
                        new CAT_Graphable_Pair__c(
                        Name = System.Label.CAT_Analytics_DS,
                        Value__c = dsCount,
                        Value_Denominator__c = otherDenom,
                        CAT_Filter_Group__c = currGroup.Id,
                        Order__c = Integer.valueOf(
                        System.Label.CAT_Analytics_DS_Order)));

                        pairsToInsert.add(
                        new CAT_Graphable_Pair__c(
                        Name = System.Label.CAT_Analytics_DE_T1,
                        Value__c = deCountTier1,
                        Value_Denominator__c = otherDenom,
                        CAT_Filter_Group__c = currGroup.Id,
                        Order__c = Integer.valueOf(
                        System.Label.CAT_Analytics_DE_Order)));

                        pairsToInsert.add(
                        new CAT_Graphable_Pair__c(
                        Name = System.Label.CAT_Analytics_DE_T2,
                        Value__c = deCountTier2,
                        Value_Denominator__c = otherDenom,
                        CAT_Filter_Group__c = currGroup.Id,
                        Order__c = Integer.valueOf(
                        System.Label.CAT_Analytics_DE_Order)));
                    }
                }
            }

            //insert name value pair list
            insert pairsToInsert;
        } catch (Exception ex) {
            CAT_ErrorHandler.handleDMLError(
                ex,
                null,
                'Aggregation Filter Batch - execute()',
                'Seed data.  [currCycle]=' + currCycleId);
        }
    }

    global void finish(Database.BatchableContext bc) {
        try {
            updateAnalyticsTimestamp(currCycleId, startTime);

            if(!Test.isRunningTest()) {
                //CAT_AggregationUtil.aggregateByDeletion(currCycleId, null);
            }
        } catch(Exception ex) {
            CAT_ErrorHandler.handleDMLError(
                ex,
                null,
                'Aggregation Filter Batch - finish()',
                'Seed data.  [currCycle]=' + currCycleId);
        }
    }

    private static void updateAnalyticsTimestamp(Id cycleId,
                                                 Datetime timestamp) {
        List<CAT_Review_Cycle__c> currentCycle =
            [SELECT Last_Analytics_Run__c
               FROM CAT_Review_Cycle__c
              WHERE Id = :cycleId];

        if(!currentCycle.isEmpty()) {
            currentCycle[0].Last_Analytics_Run__c = timestamp;
            currentCycle[0].Last_Analytics_Run_End__c = System.now();
            update currentCycle;
        }
    }
}