public class TLM_ComplexCriterion extends CAT_AbstractCriterion {

  private String value;

  /**
   * @description Constructor for a filter statement on a field with Complex data type.
   * @param       String aName - the name of the filter statement.
   * @param       String op - the operator in the filter statement.
   * @param       String aValue - the value in the filter statement.
   */
  public TLM_ComplexCriterion(String aName, String op, String aValue) {
    name = aName;
    operator = op;
    value = aValue;
  }

  /**
   * @description Mutator method for the value of the filter statement.
   * @param       Object aValue - the new value
   */
  public override void setValue(Object aValue) {
    value = String.valueOf(aValue);
  }

  /**
   * @description Combines the name, operator, and value to create a string filter statement within the where clause for a SOQL query.
   *              Allows for extra operators to be used: starts with, ends with, contains, does not contain, includes, and excludes.
   * @return      String - the criterion in SOQL.
   */
  public override String toSOQL() {
    String tempValue = String.escapeSingleQuotes(value);
    String realOperator = operator;
    String inOrNot = 'in';

    if(operator == 'starts with' || operator == 'ends with' || operator == 'contains' || operator == 'does not contain') {
      realOperator = 'like';
    }

    if(operator == 'starts with') {
      tempValue = tempValue + '%';
    } else if(operator == 'ends with') {
      tempValue = '%' + tempValue;
    } else if(operator == 'contains') {
      tempValue = '%' + tempValue + '%';
    } else if(operator == 'does not contain') {
      tempValue = '%' + tempValue + '%';
      inOrNot = 'not in';
    } else if (operator == '<>') {
      inOrNot = 'not in';
      realOperator = '=';
    }

    // todo: make dynamic
    return 'Id' + ' ' + inOrNot +
      '(select Employee__c from TLM_LMS__c where Course_Name__c ' + realOperator + ' \'' + tempValue + '\')';
  }

  /**
   * @description Accessor method for the value of the filter statement.
   * @return      Object - the value of the criterion.
   */
  public override Object getValue() {
    return value;
  }
}