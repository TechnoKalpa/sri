public with sharing class CAT_SingleRyppleRecord {
  public static CAT_Focal_File__c incomingFocal {get; set;}
  public static transient string errorLog {get; set;}

  /**
   * @description Constructor for CAT_SingleFocalArchive.
   * @param       ApexPages.StandardController sc - CAT_Focal_File__c record.
   */
  public CAT_SingleRyppleRecord(ApexPages.StandardController sc){
  }
    
  /**
   * @description Gets employee to update from rypple and updates on focal.
   * @return      PageReference - View page for focal.
   */
  public static PageReference findEmployeeToUpdate(){
    transient string jsonValue;
    List<CAT_Review_Cycle__c> reviewCycle =
      CAT_HrbpUIUtil.getCurrentReviewCycle(false);
    errorLog = '';
    if(reviewCycle[0].Rypple_Review_Cycle__c != 'NEW') {
      //Setup and Connect to Rypple
      Http http = new Http();
      HttpRequest req = new HttpRequest();
      CAT_Endpoint_Data__c rsu = CAT_Endpoint_Data__c.getValues(
        'Single Rypple Endpoint');
      if (rsu == null)
        rsu = new CAT_Endpoint_Data__c(name = 'Single Rypple Endpoint',
        Username__c = 'Bob', password__c ='Password', URL__c = 'facebook.com',
        Encrypted__c = 'BL25');

          
      String url = rsu.URL__c + reviewCycle[0].Rypple_Review_Cycle__c +
        rsu.URL_2__c + '&workday_ids=' + incomingFocal.Workday_Id__c;
      String encrypted    = rsu.Encrypted__c;

      String authorizationHeader = 'Basic ' + encrypted;
      req.setHeader('Authorization', authorizationHeader);
      req.setTimeout(20000);
      req.setMethod('GET');
      req.setEndpoint(url);

      //Upon return, attempt to parse the JSON values to insert into the system.
      if (Test.isRunningTest()) {
        jsonValue = '{"objects":[{"id":32984,"period_id":40,"reviewer":' +
          '{"id":"234389","display_name":"Erick Tseng","email":' +
          '"ericktseng@rypp.ly","workday_id":"99"},"reviewee":{"id":"235328",' +
          '"display_name":"Sid Murlidhar","email":"sid@rypp.ly",' +
          '"workday_id":"101429"},"requester":{"id":"234389","display_name":' +
          '"Erick Tseng","email":"ericktseng@rypp.ly","workday_id":"99"},' +
          '"ratings":{"Assessment":6,"Promotion":0}}]}';
      }
      else {
        HTTPResponse resp = http.send(req);
        jsonValue = resp.getBody().replace('\n', '');
        //Parse out the excess values that return so its a valid JSON string
        jsonValue = jsonValue.substring(12, jsonValue.length() -2);
      }
    }
    try {
      JSONObject j = jsonValue != null ? new JSONObject( jsonValue ) : null;
      updateRyppleDataOnFocal(j);
    } catch (JSONObject.JSONException e) {
      errorLog = 'We are sorry, this employee is too new to evaluate and ' +
        'does not have a Rypple Loop.';
    }
    PageReference ffPage = new ApexPages.StandardController(incomingFocal)
      .view();
    ffPage.setRedirect(true);
    return ffPage;
  }
    
  /**
   * @description update focal from Rypple.
   * @return      JSONObject jsonValue - JSONObject to update.
   */
  public static void updateRyppleDataOnFocal(JSONObject jsonValue){
    try{
      String assessment = jsonValue == null ? 'None' : String.valueOf(
        jsonValue.getValue('ratings').obj.getValue('Assesment').num);
      CAT_Integration_Set__c updateSet = new CAT_Integration_Set__c(
        Id = incomingFocal.CAT_Integration_Set__c);
      updateSet.ACT_Performance_Assessment__c = assessment;
      updateSet.ACT_Promotion_Flag__c = (jsonValue != null &&
        jsonValue.getValue('ratings').obj.getValue('Promotion').num == 0) ?
        'Yes' : 'No';
      update updateSet;
    } catch(Exception e) {
      errorLog = 'We are sorry, this employee is too new to evaluate ' +
        'and does not have a Rypple Loop.';
    }
  }
}