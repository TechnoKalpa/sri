public class CAT_FilteredSearchStatement extends CAT_AbstractSearchStatement {

    private String advancedCriteria;                //Dictates how CAT_AbstractCriterion are concatenated to create the where clause.
    private List<String> fields;                    //Fields for selection.
    private String queryObject;                     //The object to query on.
    private static final String DELIMITER = ', ';   //The delimiter in the SELECT portion of a SOQL query.
    private String queryTail;                       //An end to the query.  Used to add limits or an ordering.
    private List<String> numbersUsed;               //Used to number the criterion.  Allows the user to not start with filter #1.
    
    /**
     * @description Instantiates an object which represents a SOQL query.
     * @param       List<String> fieldsForSelection - the fields which will be in the select clause.
     * @param       List<CAT_AbstractCriterion> acList - the CAT_AbstractCriterion which will make up the where clause.
     * @param       String objectForSelection - the object that we will be selecting the fieldsForSelection from.
     * @param       String booleanConcatenators - the designation of how to concatenate the CAT_AbstractCriterion.
     * @param       String queryEnd - used if a limit or sort is needed to be added to the query.
     */
    public CAT_FilteredSearchStatement(List<String> fieldsForSelection, List<CAT_AbstractCriterion> acList, String objectForSelection, String booleanConcatenators, String queryEnd, List<String> numbers) {
        criteria = acList;
        fields = fieldsForSelection;
        queryObject = String.escapeSingleQuotes(objectForSelection);
        advancedCriteria = booleanConcatenators;
        queryTail = queryEnd;
        numbersUsed = numbers;
    }
    
    /**
     * @description Turns this statement into a SOQL query.
     * @return      String - the soql query.
     */
    public override String toSOQL() {
        //Build the select clause
        String query = 'SELECT '; 
        for(String field : fields) {
            query += String.escapeSingleQuotes(field) + DELIMITER;
        }
        query = query.substring(0, query.length() - DELIMITER.length());
        
        //Add on the from clause.  If there are no criterion return the SOQL query with the query tail added on.
        query += ' FROM ' + queryObject;
        if(criteria.size() == 0) {
            return query  + (queryTail != null && queryTail.trim() != '' ? ' ' + queryTail : '');   
        } 
        
        //Create a map of the order of the criterion to the the criterion.
        query += ' WHERE ';
        Map<String, CAT_AbstractCriterion> orderToAC = new Map<String, CAT_AbstractCriterion>();
        Integer counter = 0;
        for(CAT_AbstractCriterion criterion : criteria) {
            orderToAc.put(' ' + numbersUsed[counter] + ' ', criterion);
            counter++;
        }
        
        //Using the map created above, and the advanced criteria, build out there where clause.
        advancedCriteria = '(' + String.escapeSingleQuotes(advancedCriteria) + ')';
        advancedCriteria = advancedCriteria.replace('(', ' ( ');
        advancedCriteria = advancedCriteria.replace(')', ' ) ');
        List<String> acSplit = advancedCriteria.split(' ', 0);
        String whereClause = '', paddedToken;
        Set<String> keySet = orderToAc.keySet();
        for(String token : acSplit) {
            if(token == '') continue;
            paddedToken = ' ' + token + ' ';
            if(keySet.contains(paddedToken)) {
                whereClause += orderToAc.get(paddedToken).toSOQL();
            } else {
                whereClause += token;
            }
            whereClause += ' ';
        }
        whereClause = whereClause.substring(0, whereClause.length()-1);
        
        /** ADDED SPECIFICALLY FOR CAT TOOL **/
        if(queryTail != null && queryTail.contains('WHERE')) {
            queryTail = queryTail.replace('WHERE', 'AND');
        } else if(queryTail != null) {
            queryTail = String.escapeSingleQuotes(queryTail);
        }
        
        //Concatenate all parts of the soql query and return it.
        return query + whereClause + (queryTail != null && queryTail.trim() != '' ? ' ' + queryTail : '');
    }

}