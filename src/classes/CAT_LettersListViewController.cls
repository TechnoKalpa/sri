global with sharing class CAT_LettersListViewController {
  public boolean downloadAll{get;set;}
  public List<CAT_Review_Cycle_Letter__c>    compFiles            {get; set;}
  public List<CAT_Review_Cycle_Letter__c>    deFiles              {get; set;}
  public Boolean                             areErrors            {get; set;}
  public Boolean                             hasDELetters         {get; set;}
  public String                              letterType           {get; set;}
  public String                              managersList         {get; set;}
  public String                              selectedManager;
  public String                              selectedManagerName;
  public String                              allManagersAvailable;
  public String                              currUserNum          {get; set;}
  public Boolean                             isHRBP               {get; set;}
  public Boolean                             isComp               {get; set;}
  public Id                                  currCycleId          {get; set;}
  public String                              fileName             {get; set;}

  private static final String COMP_LETTER = 'COMP';
  private static final String DE_LETTER   = 'DE';
  private Set<String> exclude;
  private static final Id recTypeId =
    CAT_TestClassUtilities4.getRecordTypeId('Letter');
  private static final Id deRecTypeId = 
    CAT_TestClassUtilities4.getRecordTypeId('DE_Letter');

  public CAT_LettersListViewController () {
    downloadAll         = false;
    areErrors           = false;
    isHRBP              = false;
    compFiles           = new List<CAT_Review_Cycle_Letter__c>();
    deFiles             = new List<CAT_Review_Cycle_Letter__c>();
    selectedManager     = '';
    selectedManagerName = '';
    Id currUserId       = UserInfo.getUserId();
    fileName =  userInfo.getName()+'.'+String.valueOf(Date.today());
    exclude = new Set<String>();
    List <User> currUser = CAT_WithoutSharingUtil.queryUser(
      'SELECT Id, EmployeeNumber FROM User WHERE Id= \'' +
        currUserId  +'\' LIMIT 1');

    if (currUser.size() > 0 && currUser[0].EmployeeNumber != null){
      currUserNum = currUser[0].EmployeeNumber;

      List <CAT_Employee__c> matchingCat =
        CAT_WithoutSharingUtil.queryEmployee(
          'SELECT Is_HRBP__c, Current_Cycle__c FROM CAT_Employee__c ' +
          'WHERE Workday_External_Id__c = \'' + currUserNum + '\' LIMIT 1');
      
      List <GroupMember> compAdminGroup = new List <GroupMember> (
        [SELECT Id,
          UserOrGroupId,
          Group.Name
          FROM GroupMember
          WHERE UserOrGroupId =: currUserId
          AND ((Group.Name = 'Comp Admin')
          OR Group.Name = 'Comp Admin Limited')]);
      system.debug('&&&&&&&'+matchingCat);
      currCycleId = matchingCat.size() > 0 ? matchingCat[0].Current_Cycle__c :
        null;
      isHrbp = (matchingCat.size() > 0 && matchingCat[0].is_Hrbp__c) ||
        compAdminGroup.size() > 0;
      isComp = compAdminGroup.size() > 0;
      if(isComp && compAdminGroup[0].Group.Name == 'Comp Admin Limited'){
        String exludeList = System.label.CAT_ExclLettersList;
        Set<String> others = new Set<String>(exludeList.split(','));
        String compMgr = exludeList.subString(0, exludeList.indexOf(','));
        if(compMgr != currUserNum){
          exclude.add(compMgr);
        }
        if(!others.contains(currUserNum)){
          exclude.addAll(others);
        }
      }

      if(isHrbp) {
      
        managersList = managers();
        system.debug('@@@@@@@@@@@mgrs'+managersList);
      }
    }

    if (!isHRBP) {
      compFiles = getLetters('', currCycleId);
      deFiles = getDELetters('', currCycleId);
      hasDELetters = deFiles.size() > 0;
    }
  }

  public PageReference createShares() {
    //CAT_DynamicLetterShareUtil.createShareRecords(UserInfo.getUserId());
    return null;
  }
  public boolean getCancelBtnShow(){
    List<CAT_Focal_File__c> employees = new List<CAT_Focal_File__c>([SELECT Employee__r.Full_Name__c FROM CAT_Focal_File__c WHERE  Employee__r.IS_HRBP__c=true]);
    return employees.size() > 0;
  }
  public String getAttachmentIds(){
     List<sObject> atts = new List<sObject>();
     atts = allAttachments('', '',currCycleId );
     String letter ='';
     String attIds ='';
     String comma ='';
     if(downloadAll){
         for(CAT_Focal_File__c  obj : [SELECT id, Employee__r.Full_Name__c,
                      CAT_Review_Cycle_Letter__r.Letter__c,(select id from attachments)
                      FROM CAT_Focal_File__c 
                      WHERE  CAT_Integration_Set__r.Rypple_Chain_by_Name__c
                      Like:('%'+selectedManagerName+'%') 
                       AND CAT_Review_Cycle__c =:currCycleId]){
                        for(Attachment att : obj.attachments){
                             attIds +=comma+att.Id;comma=',';
                         }
         }
                       
                       
         /*for(CAT_Focal_File__c  obj : [SELECT id, Employee__r.Full_Name__c,(select id from attachments) FROM CAT_Focal_File__c WHERE  CAT_Integration_Set__r.Rypple_Chain_by_Name__c like:('%'+selectedManagerName+'%') AND CAT_Review_Cycle__c =:currCycleId]){
             for(Attachment att : obj.attachments){
                 attIds +=comma+att.Id;comma=',';
             }
         }*/
     }else{
         if(atts.size() > 0){
             if(letterType == '' || letterType == 'COMP'){
                letter = String.valueOf(atts[0].get('Letter__c'));
              } else if (letterType == 'DE') {
                letter = String.valueOf(atts[0].get('Discretionary_Equity_Letter__c'));
              } else {
                letter = String.valueOf(atts[0].get('Letter__c'));
              }
              
              for(Attachment att : [SELECT Name, Body,Id FROM Attachment WHERE Id =:letter]){
                  attIds +=comma+att.Id;comma=',';
              }
         }
     }
     
     return attIds;
  }      
  // Change by me
  private sObject[] allAttachments(String letterType, String hrbpManager,
    String currCycleId){
    if(letterType == '' || letterType == COMP_LETTER){
      if (hrbpManager != '') {
        /*return CAT_WithoutSharingUtil.noShareCompRetrieval(hrbpManager,
          currCycleId, recTypeId);*/
      }
      return getLetters1('', 'a0F1500000EbkI7');
    } else if (letterType == DE_LETTER){
      if (hrbpManager != ''){
        /*return CAT_WithoutSharingUtil.noShareCompDERetrieval(hrbpManager,
          currCycleId, deRecTypeId);*/
      }
      return getDELetters1(hrbpManager, currCycleId);
    } else {
      return getLetters1(hrbpManager, currCycleId);
    }
  }
  
  @RemoteAction
  global static sObject[] getAttachments(String letterType, String hrbpManager,
    String currCycleId){
    if(letterType == '' || letterType == COMP_LETTER){
      if (hrbpManager != '') {
        /*return CAT_WithoutSharingUtil.noShareCompRetrieval(hrbpManager,
          currCycleId, recTypeId);*/
      }
      return getLetters('', 'a0F1500000EbkI7');
    } else if (letterType == DE_LETTER){
      if (hrbpManager != ''){
        /*return CAT_WithoutSharingUtil.noShareCompDERetrieval(hrbpManager,
          currCycleId, deRecTypeId);*/
      }
      return getDELetters(hrbpManager, currCycleId);
    } else {
      return getLetters(hrbpManager, currCycleId);
    }
  }

  @RemoteAction
  global static void updateManagerDownloadDate(String hrbpManager,
    String letterType, String currCycleId) {
    if(letterType == DE_LETTER){
      updateDeManagerDownloadDate(hrbpManager, currCycleId);
    } else {
      updateCompManagerDownloadDate(hrbpManager, currCycleId);
    }
  }

  private static List<CAT_Review_Cycle_Letter__c> getLetters(String hrbpManager,
    Id currCycleId) {
   /* List <CAT_Review_Cycle_Letter__c> onlyDirectLetters =
      CAT_WithoutSharingUtil.noShareCompRetrieval(hrbpManager == '' ?
        UserInfo.getUserId() : hrbpManager, currCycleId, recTypeId);
  */
    return [SELECT Id,
      Employee_Name__c,
      Name,
      Letter__c,
      LastModifiedDate
      FROM CAT_Review_Cycle_Letter__c
      WHERE Letter__c <> null
      AND CAT_Review_Cycle__c ='a0F1500000EbkI7'
      /*AND Id in: onlyDirectLetters
      AND RecordTypeId = :recTypeId*/
      ORDER BY Employee_Name__c asc]; 
      /*:currCycleId*/
  }
  private List<CAT_Review_Cycle_Letter__c> getLetters1(String hrbpManager,
    Id currCycleId) {
   /* List <CAT_Review_Cycle_Letter__c> onlyDirectLetters =
      CAT_WithoutSharingUtil.noShareCompRetrieval(hrbpManager == '' ?
        UserInfo.getUserId() : hrbpManager, currCycleId, recTypeId);
  */
    return [SELECT Id,
      Employee_Name__c,
      Name,
      Letter__c,
      LastModifiedDate
      FROM CAT_Review_Cycle_Letter__c
      WHERE Letter__c <> null
      AND CAT_Review_Cycle__c ='a0F1500000EbkI7'
      /*AND Id in: onlyDirectLetters
      AND RecordTypeId = :recTypeId*/
      ORDER BY Employee_Name__c asc]; 
      /*:currCycleId*/
  }
  private static List<CAT_Review_Cycle_Letter__c> getDELetters(
    String hrbpManager, Id currCycleId) {

    List <CAT_Review_Cycle_Letter__c> onlyDirectDELetters ;/*=
      CAT_WithoutSharingUtil.noShareCompDERetrieval(hrbpManager == '' ?
        UserInfo.getUserId() : hrbpManager, currCycleId, deRecTypeId);
     */
    return [SELECT Id,
      Employee_Name__c,
      Name,
      Discretionary_Equity_Letter__c,
      LastModifiedDate
      FROM CAT_Review_Cycle_Letter__c
      WHERE Discretionary_Equity_Letter__c <> null
      AND CAT_Review_Cycle__c = 'a0F1500000EbkI7'
      /*:currCycleId
      AND Id in: onlyDirectDELetters
      AND RecordTypeId = :deRecTypeId*/
      ORDER BY Employee_Name__c asc];
  }
  // Change by me
  private List<CAT_Review_Cycle_Letter__c> getDELetters1(
    String hrbpManager, Id currCycleId) {

    List <CAT_Review_Cycle_Letter__c> onlyDirectDELetters ;/*=
      CAT_WithoutSharingUtil.noShareCompDERetrieval(hrbpManager == '' ?
        UserInfo.getUserId() : hrbpManager, currCycleId, deRecTypeId);
     */
    return [SELECT Id,
      Employee_Name__c,
      Name,
      Discretionary_Equity_Letter__c,
      LastModifiedDate
      FROM CAT_Review_Cycle_Letter__c
      WHERE Discretionary_Equity_Letter__c <> null
      AND CAT_Review_Cycle__c = 'a0F1500000EbkI7'
      /*:currCycleId
      AND Id in: onlyDirectDELetters
      AND RecordTypeId = :deRecTypeId*/
      ORDER BY Employee_Name__c asc];
  }

  private static void updateCompManagerDownloadDate(String hrbpManager,
    String currCycleId){

    transient List<CAT_Review_Cycle_Letter__c> letters =
      getLetters(hrbpManager, currCycleId);
    transient List<CAT_Metric__c> downloadAllMetrics =
      new List<CAT_Metric__c>();
    for(CAT_Review_Cycle_Letter__c letter : letters) {
        downloadAllMetrics.add(new CAT_Metric__c(
          Type__c= 'Downloaded All Comp Letters',
          Details__c = 'Downloaded Comp letter for ' + letter.Employee_Name__c,
          Current_User__c = UserInfo.getUserId(),
          Application__c = 'CAT'));
    }

    try {
      insert downloadAllMetrics;
      if(letters != null){
        //CAT_WithoutSharingUtil.updateManagerDownloadDate(letters);
      }

    } catch (Exception ex) {
      /*CAT_ErrorHandler.handleDMLError(ex, null, 'manager letter page',
        'Errored while recording letter download all.');*/
    }
  }

  private static void updateDeManagerDownloadDate(String hrbpManager,
    String currCycleId){

    transient List<CAT_Review_Cycle_Letter__c> letters =
      getDELetters(hrbpManager, currCycleId);
    transient List<CAT_Metric__c> downloadAllMetrics =
      new List<CAT_Metric__c>();
    for(CAT_Review_Cycle_Letter__c letter : letters) {
        downloadAllMetrics.add(new CAT_Metric__c(
          Type__c= 'Downloaded All DE Letters',
          Details__c = 'Downloaded DE letter for ' + letter.Employee_Name__c,
          Current_User__c = UserInfo.getUserId(),
          Application__c = 'CAT'));
    }

    try {
      insert downloadAllMetrics;
      if(letters != null){
        //CAT_WithoutSharingUtil.updateDEManagerDownloadDate(letters);
      }
    } catch (Exception ex) {
      /*CAT_ErrorHandler.handleDMLError(ex, null, 'manager letter page',
        'Errored while recording DE letter download all.');*/
    }
  }

  public void trackProxyLogger(String incSelectedManager){
    try {
      CAT_Metric__c letterMetric = new CAT_Metric__c(
        Type__c= 'Proxied in as User',
        Details__c = UserInfo.getName() + ' viewed letters as ' +
          incSelectedManager,
        Current_User__c = UserInfo.getUserId(),
        Application__c = 'CAT');
      insert letterMetric;
    } catch (Exception ex) {
      /*CAT_ErrorHandler.handleDMLError(ex, null, 'manager letter page',
        'Errored while trying to audit someone proxying in.');*/
    }
  }

  public PageReference trackLetterDownloads() {
    String selectedLetter = ApexPages.currentPage().getParameters().get('lid');
    letterType = ApexPages.currentPage().getParameters().get('ltype');

    try {
      String managerDownloadUpdateName = '';
      String letter = '';
      CAT_Review_Cycle_Letter__c managerDownloadUpdate =
        [SELECT Id, Name, Letter__c
         FROM CAT_Review_Cycle_Letter__c
         WHERE Id = :selectedLetter LIMIT 1];
      managerDownloadUpdateName = managerDownloadUpdate.Name;
      letter = managerDownloadUpdate.Letter__c;
      List<CAT_Review_Cycle_Letter__c> letters =
        new List<CAT_Review_Cycle_Letter__c>();
      letters.add(managerDownloadUpdate);

      if(letterType == null || letterType == '' || letterType == COMP_LETTER){
        //CAT_WithoutSharingUtil.updateManagerDownloadDate(letters);
      } else if (letterType == DE_LETTER) {
        //CAT_WithoutSharingUtil.updateDEManagerDownloadDate(letters);
      }

      CAT_Metric__c letterMetric = new CAT_Metric__c(
        Type__c= 'Downloaded a letter',
        Details__c = managerDownloadUpdateName + ' - ' + selectedLetter,
        Current_User__c = UserInfo.getUserId(),
        Application__c = 'CAT');
      insert letterMetric;

      return new PageReference('/servlet/servlet.FileDownload?file=' + letter);
    } catch (Exception ex) {
      areErrors = true;
      /*CAT_ErrorHandler.handleDMLError(ex, null, 'manager letter page',
        'The error occured while trying to download the letter with id \'' +
          selectedLetter + '\'');*/
      return null;
    }
  }
  /*public void trackLetterDownloadsZip(){
    try {
      List<CAT_Metric__c> metrics  = new List<CAT_Metric__c>();
      for(CAT_Review_Cycle_Letter__c obj : compFiles){
        metrics.add(new CAT_Metric__c(
        Type__c= 'Downloaded a letter',
        Details__c = obj.name + ' - ' + obj.id,
        Current_User__c = UserInfo.getUserId(),
        Application__c = 'CAT'));
       }
      if(metrics.size() > 0) insert metrics;
    } catch (Exception ex) {
      areErrors = true;
    }
  }*/
  public PageReference processLetterListChange(){
    return null;
  }

  public String managers() {
    List<User> validManagersProxy = new List<User>();
    validManagersProxy  = CAT_WithoutSharingUtil.noShareManagerRetrieval(currUserNum, currCycleId, isComp, exclude);
    transient String proxyManagers = '[';
    for (User singleManager: validManagersProxy){
      proxyManagers += '{\"value\": \"'+ singleManager.Id+'\", \"label\": \"'+
        singleManager.Name + '-' + singleManager.EmployeeNumber +'\"} ,';
        system.debug('@@@@@@@@@@@id'+singleManager.EmployeeNumber);
    }

    return String.escapeSingleQuotes(proxyManagers.substring(0,
      proxyManagers.length()-1) + ']');
  }

  public void managerAccessView(){
    compFiles   =   CAT_WithoutSharingUtil.noShareCompRetrieval(
      selectedManager, currCycleId, recTypeId);
    deFiles     =   CAT_WithoutSharingUtil.noShareCompDERetrieval(
      selectedManager, currCycleId, deRecTypeId);
    trackProxyLogger(selectedManager);
    system.debug('selectedManager = '+selectedManager);
    system.debug('currCycleId = '+currCycleId);
    system.debug('recTypeId = '+recTypeId);
    system.debug('deFiles = '+deFiles);
    if(deFiles != null) {
      hasDELetters =deFiles.size() > 0;
    }
  }

  public void setSelectedManager(String selectedManager){
    this.selectedManager = selectedManager;
  }

  public String getSelectedManager(){
    return selectedManager;
  }


  public void setSelectedManagerName(String selectedManagerName){
    if (selectedManagerName.contains('-')){
      this.selectedManagerName = selectedManagerName.split('-')[0];
    } else {
      this.selectedManagerName = selectedManagerName;
    }
  }

  public String getSelectedManagerName(){
    return selectedManagerName;
  }

  public void setAllManagersAvailable(String allManagersAvailable){
    this.allManagersAvailable = allManagersAvailable;
  }
  public static List<CAT_Employee__c> retrieveViewableEmp(Id currCycleId) {
    return [SELECT Id, Workday_External_Id__c, Name
            FROM CAT_Employee__c
            WHERE Latest_Focal__c != null
            AND Latest_Focal__r.CAT_Review_Cycle__c =
              :currCycleId];
  }
  public String getAllManagersAvailable(){
    return allManagersAvailable;
  }
}