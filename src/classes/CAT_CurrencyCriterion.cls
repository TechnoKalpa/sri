public class CAT_CurrencyCriterion extends CAT_AbstractCriterion {
    
    private String value;
    private String currencyIsoCode;

    /**
     * @description Constructor for a filter statement on a field with data type currency.
     * @param       String aName - the name of the filter statement.
     * @param       String op - the operator in the filter statement.
     * @param       String aValue - the value in the filter statement.
     */
    public CAT_CurrencyCriterion(String aName, String op, String isoCode, String aValue) {
        name = aName;
        operator = op;
        currencyIsoCode = isoCode;
        value = aValue;
    }

    /** 
     * @description Mutator method for the value of the filter statement.
     * @param       Object aValue - the new value
     */     
    public override void setValue(Object aValue) {
        value = String.valueOf(aValue);
    }
    
    /** 
     * @description Accessor method for the value of the filter statement.
     * @return      Object - the value of the criterion.
     */         
    public override Object getValue() {
        return value;
    }       
    
    /**
     * @description Sets the CurrencyIsoCode for the value.  Does not effect the SOQL query.
     * @param       String isocode - the string representation of the CurrencyIsoCode.
     */
    public void setCurrencyIsoCode(String isocode) {
        currencyIsoCode = isocode;
    }
    
    /**
     * @description Accessor for the CurrencyIsoCode.
     * @return      String - the CurrencyIsoCode.
     */
    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    /** 
     * @description Combines the name, operator, and value to create a currency filter statement within the where clause for a SOQL query.
     * @return      String - the criterion in SOQL.
     */ 
    public override String toSOQL() {
        return name + ' ' + operator + ' ' + value;
    }       
}