public abstract class CAT_AbstractCriterion {
    
    public String name {get; set;} 
    public String operator {get; set;}
    
    /** 
     * @description Mutator method for the value of the criterion.
     * @param       Object aValue - the new value
     */
    public abstract void setValue(Object aValue);
    
    /** 
     * @description Combines the name, operator, and value to create a filter statement within the where clause for a SOQL query.
     * @return      String - the criterion in SOQL.
     */ 
    public abstract String toSOQL();
    
    /** 
     * @description Accessor method for the value of the criterion.
     * @return      Object - the value of the criterion.
     */ 
    public abstract Object getValue();
    
}