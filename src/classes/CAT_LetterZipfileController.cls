public with sharing class CAT_LetterZipfileController {
    public Integer randomInt{get;set;}
    
    public CAT_LetterZipfileController(){
        randomInt = getRandomNumber(10000);
    }
    
    @RemoteAction
    public static List<AttachmentWrapper> getAttachments(String attachmentIdCSV){
        List<String> attachmentIds = new List<String>();
        attachmentIds = attachmentIdCSV.split(',');
        return wrapAttachments([SELECT Id,Name,Body FROM Attachment WHERE Id IN:attachmentIds]);
    }
    @RemoteAction
    public static String trackLetterDownloads(String attachmentIdCSV){
          List<String> attachmentIds = new List<String>();
          for(string aId : attachmentIdCSV.split(',')){
              attachmentIds.add(aId.substring(0, 15));
          }
          List<CAT_Metric__c> metrics  = new List<CAT_Metric__c>();
          for(CAT_Review_Cycle_Letter__c obj : [SELECT Id, Name, Letter__c FROM CAT_Review_Cycle_Letter__c WHERE Letter__c IN:attachmentIds]){
            metrics.add(new CAT_Metric__c(
            Type__c= 'Downloaded a letter',
            Details__c = obj.name + ' - ' + obj.id,
            Current_User__c = UserInfo.getUserId(),
            Application__c = 'CAT'));
           }
          if(metrics.size() > 0) insert metrics;
          return '';
    }
    
    private static List<AttachmentWrapper> wrapAttachments(List<Attachment> attachments){
        List<AttachmentWrapper> wrappers = new List<AttachmentWrapper>();
        for(Attachment att : attachments){
            wrappers.add(new AttachmentWrapper(att));
        }
        
        return wrappers;
    }
    
    public class AttachmentWrapper{
        public Attachment AttachmentObj;
        public String base64Body;
        
        public AttachmentWrapper(Attachment AttachmentObj){
            this.AttachmentObj  = AttachmentObj;
            this.base64Body = EncodingUtil.base64Encode(AttachmentObj.Body);
            this.AttachmentObj.Body = NULL;
        }
    }
    
    /*
    *Random number generator to change the js function name if multiple components us
    ***/
    private Integer getRandomNumber(Integer size){
        Double d = Math.random() * size;
        return d.intValue();
    }
}