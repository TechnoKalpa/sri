public with sharing class CAT_ErrorHandler {

    /**
     * @description Handles an error by performing a rollback and sending an email to cat administrators.
     * @param Exception ex        - error that occured
     * @param System.SavePoint sp - point to rollback to
     * @param String pageName     - name of page error occured on
     * @param String bodyDetail   - body of email to be sent
     **/    
    public static void handleDMLError(Exception ex, System.SavePoint sp, String pageName, String bodyDetail) {
        if(sp != null) {
            Database.rollback(sp);  
        }
        
        String adminLabel = System.Label.CAT_Admins;
                    
        if(adminLabel != null && adminLabel.trim() != '') {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(adminLabel.contains(',')) {
                mail.setToAddresses(adminLabel.split(','));
            } else {
                mail.setToAddresses(new List<String>{adminLabel});
            }
            
            User currentUser = [SELECT Id, Name, Email FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            mail.setReplyTo(currentUser.Email);
            mail.setSenderDisplayName('Salesforce CAT Error Handler');
            mail.setSubject(currentUser.Name + ' has encountered an error in the ' + pageName);
            mail.setBCCSender(false);
            mail.setUseSignature(false);
            mail.setHTMLBody('CAT Admins,<br/><br/>' + currentUser.Name + ' has encountered an error in the ' + pageName + ' at ' + System.now() + '.  ' + bodyDetail + '  More details below.<br /><br />Error Message: ' + ex.getMessage() + '<br/><br/>Stack Trace: ' + ex.getStackTraceString());
            
            if(!Test.isRunningTest()) {
                Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
            }
        }       
    }
    
}