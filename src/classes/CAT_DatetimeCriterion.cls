public class CAT_DatetimeCriterion extends CAT_AbstractCriterion {

    private Datetime value;

    /**
     * @description Constructor for a filter statement on a field with data type datetime.
     * @param       String aName - the name of the filter statement.
     * @param       String op - the operator in the filter statement.
     * @param       Datetime aValue - the value in the filter statement.
     */
    public CAT_DatetimeCriterion(String aName, String op, Datetime aValue) {
        name = aName;
        operator = op;
        value = aValue;
    }
    
    /** 
     * @description Mutator method for the value of the filter statement.
     * @param       Object aValue - the new value
     */     
    public override void setValue(Object aValue) {
        value = Datetime.valueOf(aValue);
    }

    /** 
     * @description Accessor method for the value of the filter statement.
     * @return      Object - the value of the criterion.
     */     
    public override Object getValue() {
        return value;
    }
    
    /** 
     * @description Combines the name, operator, and value to create a datetime filter statement within the where clause for a SOQL query.
     * @return      String - the criterion in SOQL.
     */     
    public override String toSOQL() {
        List<String> dateSplit = String.valueOf(value).split(' ', 0);
        return name + ' ' + operator + ' ' + dateSplit[0] + 'T' + dateSplit[1] + 'Z';
    }       
}