public without sharing class CAT_OverrideUtil {

  public static Boolean isRunning = false;
  
  public static String normalizeString(String value) {
    if(value != null) {
      if(value.contains('%')) {
        value = value.substring(0, value.indexOf('%'));
      }

      if(value.contains('.')) {
        if(value.indexOf('.') == 0) {
          value = '0' + value;
        }

        Integer digitsAfterDecimal = value.length() - (value.indexOf('.')+1);
        if(digitsAfterDecimal > 2) {
          value = value.substring(0, value.indexOf('.')+3);
        } else if(digitsAfterDecimal == 1) {
          value += '0';
        } else if(digitsAfterDecimal == 0) {
          value += '00';
        }
      } else {
        value += '.00';
      }
    }
    return value;
  }
  }