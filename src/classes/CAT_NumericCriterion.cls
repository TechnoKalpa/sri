public class CAT_NumericCriterion extends CAT_AbstractCriterion {
    
    private String value;

    /**
     * @description Constructor for a filter statement on a field with data type number.
     * @param       String aName - the name of the filter statement.
     * @param       String op - the operator in the filter statement.
     * @param       String aValue - the value in the filter statement.
     */
    public CAT_NumericCriterion(String aName, String op, String aValue) {
        name = aName;
        operator = op;
        value = aValue;
    }
    
    /** 
     * @description Mutator method for the value of the filter statement.
     * @param       Object aValue - the new value
     */     
    public override void setValue(Object aValue) {
        value = String.valueOf(aValue);
    }

    /** 
     * @description Accessor method for the value of the filter statement.
     * @return      Object - the value of the criterion.
     */     
    public override Object getValue() {
        return value;
    }

    /** 
     * @description Combines the name, operator, and value to create a number filter statement within the where clause for a SOQL query.
     * @return      String - the criterion in SOQL.
     */     
    public override String toSOQL() {
        return name + ' ' + operator + ' ' + value;
    }   

}