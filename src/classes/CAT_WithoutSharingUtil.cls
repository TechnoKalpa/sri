public without sharing class CAT_WithoutSharingUtil {

    /**
     * @description Determines the proper equity dates to display
     *              in the employee popup highchart.
     * @param       Id currCycleId - id of current review cycle
     * @return      Set<Date> - set of dates to display
     **/
    private static Set<Date> determineEquityDates(Id currCycleId) {
        Set<Date> equityDates = new Set<Date>();
        for(CAT_Equity_Vesting_Detail__c evd :
          [SELECT period__c
         FROM cat_equity_vesting_detail__c
              WHERE cat_plan__r.cat_review_cycle__c = :currCycleId]) {
            equityDates.add(evd.period__c);
        }

        return equityDates;
    }

    /**
     * @description Used by graphing to smooth out inconsistent data
     * @param       String tier - input tier name
     * @return      String - correct tier name
     **/
    private static String getUsableTier(String tier) {
        if(tier == '253%' || tier == 'Tier 1') {
            return '253%';
        } else {
            return '';
        }
    }

    /**
     * @description Gets the sum of the existing vested shares up until the
     * start of the current year.
     * @param       String empId - the id of the employee to aggregate data on.
     * @param       String tier  - tier category
     * @return      Double       - amount of existing vested
     **/
    public static CAT_Equity_Aggregate__c[] getExistingVested(String empId,
                                  String tier) {

        String usableTier = getUsableTier(tier);
        CAT_Review_Cycle__c currCycle =
          null;//CAT_HrbpUIUtil.getCurrentReviewCycle(true)[0];

        List<CAT_Equity_Aggregate__c> aggs =
            [SELECT Existing_Vested_Graphable__c,
                Month__c,
                Vested_Reduction_Amount__c,
                Vested_Reduction_Graphable__c
               FROM CAT_Equity_Aggregate__c
              WHERE CAT_Employee__c = :empId AND
                    Month__c = :currCycle.Vested_As_Of__c AND
                    Aggregate_Type__c = :usableTier
              LIMIT 1];

        return aggs;
    }

    /**
     * @description  Updates currency code on equity data and
     *                 returns altered data.
     * @param        String employeeId         - Id of employee to get data for.
     * @param        String newCurrencyCode    - New currencyisocode
     * @param        String tier               - tier category
     * @return       CAT_Equity_Aggregate__c[] - List of results
     **/
    public static CAT_Equity_Aggregate__c[] updateAndGetEquityData(
               String employeeId, String currencyIsoCode, String tier) {

        String usableTier = getUsableTier(tier);

        Id currCycleId = null;//CAT_AggregationUtil.getCurrentReviewCycle();

        Set<Date> equityDates = determineEquityDates(currCycleId);

        if(equityDates.isEmpty()) {
            return new List<CAT_Equity_Aggregate__c>();
        }

        List<CAT_Equity_Aggregate__c> aggregates =
          [SELECT Id
               FROM CAT_Equity_Aggregate__c
              WHERE CAT_Employee__c = :employeeId AND Month__c in :equityDates];

        for(CAT_Equity_Aggregate__c agg : aggregates) {
            //agg.CurrencyIsoCode = currencyIsoCode;
        }

        update aggregates;

        return [SELECT DE_Graphable__c,
                         Existing_Graphable__c,
                         Refresher_Graphable__c,
                         Month__c,
                         DE_Graphable_Vesting__c,
                         Refresher_Graphable_Vesting__c,
                         Existing_Graphable_Vesting__c,
                         Existing_Vesting_USD__c,
                         Vesting_Reduction_Amount__c,
                         Unvested_Reduction_Amount__c,
                         Unvested_Reduction_Graphable__c,
                         Vested_Reduction_Graphable__c,
                         Vesting_Reduction_Graphable__c
                    FROM CAT_Equity_Aggregate__c
                   WHERE CAT_Employee__c = :employeeId AND
                         Month__c in :equityDates      AND
                         Aggregate_Type__c = :usableTier
                ORDER BY Month__c asc];
    }

    public static List<User> queryUser(String query) {
        return Database.query(query);
    }
    public static List<CAT_Employee__c> queryEmployee(String query) {
        return Database.query(query);
    }

    public static List<User> noShareManagerRetrieval(String currEmp,
      Id currCycleId, Boolean isCompAdmin, Set<String> exclude){
        if (isCompAdmin){
          transient List <User> compAdminSuperList = new List<User>();
          transient Set <String> compAdminWorkdayId = new Set<String>();
          system.debug(currEmp+'curremp');
          system.debug(exclude+'exclude');
          system.debug(currCycleId+'currCycleId');
          for (CAT_Employee__c singleName :
            [SELECT Latest_Focal__r.CAT_Integration_Set__r.Loop_Owner__r.Workday_External_Id__c,
                    Direct_Manager__r.Workday_External_id__c
               FROM CAT_Employee__c
              WHERE Latest_Focal__c != null
                AND Workday_External_Id__c != null
                AND Workday_External_Id__c NOT IN :exclude
                AND Latest_Focal__r.CAT_Review_Cycle__c = :currCycleId]){

            compAdminWorkdayId.add(singleName.Latest_Focal__r
              .CAT_Integration_Set__r.Loop_Owner__r.Workday_External_Id__c);

            compAdminWorkdayId.add(singleName
              .Direct_manager__r.Workday_External_Id__c);
          }

            for (User singleUser: [SELECT Id, Name, EmployeeNumber FROM User
              WHERE EmployeeNumber in: compAdminWorkdayId]){
                compAdminWorkdayId = null;
                compAdminSuperList.add(singleUser);
            }
            return compAdminSuperList;
        }
        currEmp                             = '\'%' +currEmp +'%\'';
       // String managerQuery                 = 'SELECT Id, Workday_External_Id__c, Name FROM CAT_Employee__c Where HRBP_Partner_Chain__c like ' + currEmp;
        List<CAT_Employee__c> allManagers =
         CAT_LettersListViewController.retrieveViewableEmp(currCycleId);


        List<String> workdayIdsForManagers  = new List<String>();
        List <User> finalManagerNames       = new List<User>();

        for (CAT_Employee__c c: allManagers){
            workdayIdsForManagers.add(c.Workday_External_Id__c);
        }

        List<User> userManagerList  = new List<User>([SELECT Id,
                                                             EmployeeNumber,
                                                             Name
                                                             FROM User
                                                             WHERE EmployeeNumber in: workdayIdsForManagers]);

        Set<Id> managerSet = new Set<Id>();
        for (CAT_Review_Cycle_Letter__Share rcl: [SELECT Id, UserOrGroupId
              FROM CAT_Review_Cycle_Letter__Share
              WHERE UserOrGroupId in :userManagerList
              AND Parent.CAT_Review_Cycle__c = :currCycleId
              AND RowCause = 'Manager_Share__c']){
            managerSet.add(rcl.UserOrGroupId);
        }

        for (User cs: userManagerList){
            if (managerSet.contains(cs.Id)){
                finalManagerNames.add(cs);
            }
        }

        return finalManagerNames;
    }

    public static List<CAT_Review_Cycle_Letter__c> noShareCompRetrieval(
      String theManagersId, Id currCycleId, Id recTypeId){
      System.debug('********theManagersId='+theManagersId+'currCycleId='+currCycleId+'recTypeId='+recTypeId);
      List <Id> letterIds = new List<Id>();
      for (CAT_Letter_Share__c l: [SELECT Id, Parent_Id__c
        FROM CAT_Letter_Share__c
        WHERE Row_Cause__c = 'Manager_Share__c'
        AND Parent_Id__r.CAT_Review_Cycle__c = :currCycleId
        AND User_Id__c = :theManagersId
        AND Parent_Id__r.RecordTypeId = :recTypeId]){

        letterIds.add(l.Parent_Id__c);
      }

      return [SELECT Employee_Name__c, Id, Name, Letter__c, LastModifiedDate
                FROM CAT_Review_Cycle_Letter__c
                WHERE Letter__c <> null AND Id in :letterIds
                ORDER BY Employee_Name__c asc];
    }

    public static List<CAT_Review_Cycle_Letter__c> noShareCompDERetrieval(
      String theManagersId, Id currCycleId, Id recTypeId){

      List <Id> letterIds = new List<Id>();
      for (CAT_Letter_Share__c l: [SELECT Id, Parent_Id__c
            FROM CAT_Letter_Share__c
            WHERE (Row_Cause__c = 'DE_Letter_Owner__c' OR
              Row_Cause__c = 'Manager_Share__c')
            AND Parent_Id__r.CAT_Review_Cycle__c = :currCycleId
            AND User_Id__c = :theManagersId
            AND Parent_Id__r.RecordTypeId = :recTypeId]){
        letterIds.add(l.Parent_Id__c);
      }

      return [SELECT Employee_Name__c, Id, Name, Discretionary_Equity_Letter__c,
                LastModifiedDate
                FROM CAT_Review_Cycle_Letter__c
                WHERE Discretionary_Equity_Letter__c <> null AND
                Id in: letterIds ORDER BY Employee_Name__c asc];
    }

  public static void updateDEManagerDownloadDate(
    List<CAT_Review_Cycle_Letter__c> letters){
    for (CAT_Review_Cycle_Letter__c letter : letters) {
      letter.Manager_Downloaded_Date__c = Date.today();
    }
    if (letters != null && !letters.isEmpty()) {
      update letters;
    }
  }

  public static void updateManagerDownloadDate(
    List<CAT_Review_Cycle_Letter__c> letters){
    for (CAT_Review_Cycle_Letter__c letter : letters) {
      letter.Manager_Downloaded_Date__c = Date.today();
    }
    if (letters != null && !letters.isEmpty()) {
      update letters;
    }
  }

    public static CAT_Equity_Aggregate__c[] getExistingVestedforTLM(String empId,
    String tier) {

    String usableTier = getUsableTier(tier);
    CAT_Review_Cycle__c currCycle =
    CAT_HrbpUIUtil.getCurrentReviewCycle(true)[0];

    List<CAT_Equity_Aggregate__c> aggs = [SELECT Existing_Vested_Graphable__c,
      Month__c,
      Vested_Reduction_Amount__c,
      Vested_Reduction_Graphable__c
      FROM CAT_Equity_Aggregate__c
      WHERE CAT_Employee__c = :empId AND
      Month__c = :currCycle.Vested_As_Of__c AND
      Aggregate_Type__c = :usableTier AND
      CAT_Employee__r.Rypple_Chain_Exec__c = false
      LIMIT 1];

      return aggs;
    }

  public static CAT_Equity_Aggregate__c[] updateAndGetEquityDataforTLM(
    String employeeId, String currencyIsoCode, String tier) {

    String usableTier = getUsableTier(tier);
    Id currCycleId ;//= CAT_AggregationUtil.getCurrentReviewCycle();
    Set<Date> equityDates = determineEquityDates(currCycleId);

    if(equityDates.isEmpty()) {
      return new List<CAT_Equity_Aggregate__c>();
    }

    return [SELECT DE_Graphable__c, Existing_Graphable__c,
      Refresher_Graphable__c, Month__c, DE_Graphable_Vesting__c,
      Refresher_Graphable_Vesting__c, Existing_Graphable_Vesting__c,
      Existing_Vesting_USD__c, Vesting_Reduction_Amount__c,
      Unvested_Reduction_Amount__c, Unvested_Reduction_Graphable__c,
      Vested_Reduction_Graphable__c, Vesting_Reduction_Graphable__c
      FROM CAT_Equity_Aggregate__c
      WHERE CAT_Employee__c = :employeeId AND
      Month__c in :equityDates      AND
      Aggregate_Type__c = :usableTier AND
      CAT_Employee__r.Rypple_Chain_Exec__c = false
      ORDER BY Month__c asc];
  }

  /**
  * @description  Finds and returns historical performance data for the Employee
  * @param        String empId   - employee id to get history for
  * @return       List<PerformanceData> - performance history
  **/
  public static List<TLM_GridController.PerformanceData>
    retrievePerformanceHistoricalData(String empId) {

    transient List<TLM_GridController.PerformanceData> performanceList
     = new List<TLM_GridController.PerformanceData>();

    if(empId == null) {
      return performanceList;
    }
    for(CAT_Assessment_History__c hist : [SELECT Review_Cycle_Name__c,
      Assessment_Title__c,
      Received_Promotion__c,
      DE__c,
      CAT_Focal_File__r.ACT_Job_Level__c
      FROM CAT_Assessment_History__c
      WHERE CAT_Employee__c in (
        select CAT_Employee__c
        from TLM_Employee__c
        where Id = :empId
        and Rypple_Chain_Exec__c = false)
      ORDER BY Cycle_End__c desc]) {

      performanceList.add(new TLM_GridController.PerformanceData(
        hist.Review_Cycle_Name__c,
        hist.Assessment_Title__c ,
        hist.Received_Promotion__c,
        hist.DE__c,
        hist.CAT_Focal_File__r.ACT_Job_Level__c));
    }

    return performanceList;
  }

  public static Boolean isHRBPUser(Id userId) {
    User currentUser = [SELECT Id, EmployeeNumber FROM User
      WHERE Id = :userId limit 1];
    List<CAT_Employee__c> userEmp = [Select Id, Is_HRBP__c FROM CAT_Employee__c
      WHERE Workday_External_Id__c = :currentUser.EmployeeNumber];

    if(userEmp != null && !userEmp.isEmpty()) {
      return userEmp[0].Is_HRBP__c;
    } else {
      return false;
    }
  }

  public static Boolean isPeepsUser(Id userId) {
    List<GroupMember> peepsGroups = [SELECT Id, GroupId, Group.Name,
                                      UserOrGroupId
                                      FROM GroupMember
                                      WHERE Group.Name like '%Peeps' and
                                      UserOrGroupId = :userId];
    return (!peepsGroups.isEmpty());
  }

  public static void saveToServer(String dataType, String osId,
    String columnName, String newValue, String oldValue) {

    Boolean newSameAsOld;/* = CAT_OverrideUtil.normalizeString(newValue) ==
      CAT_OverrideUtil.normalizeString(oldValue);*/
    CAT_Override_Set__c overrideSet = new CAT_Override_Set__c(Id = osId);
    if(newValue == null || newValue.trim() == '' || newSameAsOld) {
      overrideSet.put(columnName, null);
    } else if(dataType == 'Text') {
      overrideSet.put(columnName, newValue);
    } else if(dataType == 'Number' || dataType == 'Percent') {
      overrideSet.put(columnName, Double.valueOf(newValue));
    } else if(dataType == 'Bool') {
      overrideSet.put(columnName, newValue.toUpperCase() == 'TRUE');
    }

    update overrideSet;
  }

  public static void setCustomStyles(List<CAT_Tab_Column_Coupler__c> couplers) {
    Map<Id, Id> ownerIds = new Map<Id, Id>();
    Set<Id> readOnlyUserId = new Set<Id>();
    List<String> editableCols ;//= System.Label.CAT_GridEditableColumns.split(',');

    for(CAT_Tab_Column_Coupler__c tab : [SELECT Grid_Tab__c, Grid_Tab__r.OwnerId
                                FROM CAT_Tab_Column_Coupler__c
                                WHERE Id IN :couplers AND
                                Grid_Column__r.Name NOT IN :editableCols]) {
      ownerIds.put(tab.Grid_Tab__c, tab.Grid_Tab__r.OwnerId);
    }

    for(PermissionSetAssignment psa : [SELECT AssigneeId, Id
      FROM PermissionSetAssignment
      WHERE PermissionSet.Name = 'CAT_HR_Non_HR_Read_Only'
      AND AssigneeId IN :ownerIds.values()]) {

      readOnlyUserId.add(psa.AssigneeId);
    }

    for(CAT_Tab_Column_Coupler__c coupler : couplers) {
      Id ownerId = ownerIds.get(coupler.Grid_Tab__c);
      if(readOnlyUserId.contains(ownerId)) {
        coupler.CSS_Class__c = 'cell-title-readonly-number';
      }
    }
  }
}