/**
 * @author      Phil Rymek
 * @date        4/4/11
 * @description Controller for HRPBUI.
 **/
global with sharing class CAT_HrbpController {

    private static final Integer CHUNK_SIZE      = Integer.valueOf(System.Label.CAT_Chunk_Size); //Number of results per chunk
    private static final Integer INIT_CHUNK_SIZE = Integer.valueOf(System.Label.CAT_Initial_Chunk_Size); //Initial chunk size

    public CAT_FilterInputManager fim       {get; set;} //Controls the FilterInput component
    public Boolean scrollUpLocked           {get; set;}
    public CAT_Review_Cycle__c currCycle    {get; set;} //The current review cycle
    public CAT_Review_Cycle__c prevCycle    {get; set;}
    public CAT_Review_Cycle__c alwaysOn     {get; set;} //The always on cycle
    public String filterDescription         {get; set;} //Filter description on mouse over of the filter
    public transient String exportString    {get; set;} //Used by export functionality, holds table downloads as .xlsx
    public Boolean isCompAdmin              {get; set;} //True if user is in comp admin group
    public Boolean isHrbp                   {get; set;} //True if user is an HRBP
    public Boolean isTierUser               {get; set;} //True if user can see the Tier functionality in the grid
    public String filterName                {get; set;} //Used by "Save Filter" functionality.  Holds the users desired name for the filter.
    public String appliedFilter             {get; set;} //Currently applied filter
    public Boolean locked                   {get; set;} //True when cycle is in phase 1 or 5
    public String focalPrefix               {get; set;} //First three digits of focal file ids.  Used by typeahead in reporting tab
    public String primaryFilterParam        {get; set;}         //Contains data typed into primary filter
    public String primaryFilterParam2       {get; set;}         //Contains value of radio button associated with primary filter
    public Date startingEquityDate          {get; set;} //Contains the date shown for starting of the employee popup
    public String pageLoadTime              {get; set;}
    public String  lastQuery                {get; set;}

    private Boolean createAppData;                      //True when the user has never been in HRBPUI before and they need an app data record created.
    private CAT_Application_Data__c appData;            //The app data record for the current user
    private String splitValue;                          //Value of tag user has clicked on

    public static final String OVERRIDE_SET = 'CAT_Override_Set__c';
    public static final String FOCAL_FILE   = 'CAT_Focal_File__c';

    /**
     * @description Constructor for CAT_HrbpController.  Instantiates the filter input manager.
     **/
    global CAT_HrbpController() {
        primaryFilterParam2 = 'name';
        isTierUser          = false;

        focalPrefix  = CAT_Focal_File__c.SObjectType.getDescribe().getKeyPrefix();
        pageLoadTime = '' + Datetime.now().addMinutes(-2);

        //Get the current review cycle
        List<CAT_Review_Cycle__c> reviewCycle =
            CAT_HrbpUIUtil.getCurrentReviewCycle(true);

        if(!reviewCycle.isEmpty()) {
            currCycle = reviewCycle[0];
            if(currCycle.Phase__c.contains('0')) {
                List<CAT_Review_Cycle__c> lastCycles =
                    CAT_HrbpUIUtil.getCurrentReviewCycle(false);

                if(!lastCycles.isEmpty()) {
                    prevCycle = lastCycles.get(0);
                }
            }
        }

        //Load CAT_Application_Data__c record
        loadData();

        //Set the applied filter to none, then check app data record to see if there there is a last applied filter,
        //If there was, apply it.
        appliedFilter = '*None*';
        filterName = '';

        fim = new CAT_FilterInputManager(FOCAL_FILE, CAT_HrbpUIUtil.getFieldsToSelect(), Integer.valueOf(System.Label.CAT_FIM_Minimum),
                                         Integer.valueOf(System.Label.CAT_FIM_Maximum), getDefaultWhere(), true);

        //Load in saved filtering information
        if(appData.Primary_Filter_Option__c != null) {
            primaryFilterParam2 = appData.Primary_Filter_Option__c;
        }

        if(appData.Primary_Filter__c != null && appData.Primary_Filter__c.trim() != '') {
            primaryFilterParam = appData.Primary_Filter__c;
        }

        if(appData.Saved_Filter__c != null) {
            applySpecificFilter(appData.Saved_Filter__c, true);
        }

        //Determine if user is comp admin
        List <GroupMember> compAdminGroup = new List<GroupMember>(
          [SELECT Id, UserOrGroupId
             FROM GroupMember
            WHERE Group.Name IN ('Comp Admin','Comp Admin Limited')
              AND UserOrGroupId = :UserInfo.getUserId()]);

        isCompAdmin = compAdminGroup.size() > 0;


        isHrbp = CAT_WithoutSharingUtil.isHRBPUser(UserInfo.getUserId());

        //Determines if the user seeing the grid should be able to see the Tier field
        List <CAT_Focal_File__c> isTierWorker = [SELECT Id FROM CAT_Focal_File__c WHERE prop_eq_tier__c = 'Yes' limit 1];
        if  (isTierWorker.size() > 0){
            isTierUser = true;
        }

        //PR 7/19 Locked definition changed so half-implemented feature doesnt get pushed.
        locked = currCycle.Phase__c.contains('0') ||
                 currCycle.Phase__c.contains('1') ||
                 currCycle.Phase__c.contains('5');

        //When its still not locked, do an additional test to see if the ui needs to be locked.
        if(!locked) {
            locked = [SELECT count()
                        FROM PermissionSetAssignment
                       WHERE AssigneeId = :UserInfo.getUserId() AND
                             (PermissionSet.Name = 'CAT_Non_HR_Read_Only' OR
                              PermissionSet.Name = 'CAT_Non_HR_Read_Only_Platform' OR
                              PermissionSet.Name = 'CAT_Sales_Group_Read_Only')] > 0;
        }

        lastQuery = fim.getSOQLQuery();
        system.debug('##### ' + lastQuery);
    }

    /**
     * @description getter for chunk size
     **/
    public Integer getChunkSize() {
        return CHUNK_SIZE;
    }

    /**
     * @description getter for initial chunk size
     **/
    public Integer getInitialChunkSize() {
        return INIT_CHUNK_SIZE;
    }

    /**
     * @description Loads the CAT_Application_Data__c record for the current user.  If it doesnt exist, createAppData is flagged.
     **/
    private void loadData() {
        transient List<CAT_Application_Data__c> appDataList = [SELECT Id, Saved_Filter__c, Primary_Filter__c, Primary_Filter_Option__c, Slide_Up_Locked__c
                                                                 FROM CAT_Application_Data__c
                                                                WHERE OwnerId = :UserInfo.getUserId()
                                                                AND RecordType.Name = :CAT_EmployeeTriggerUtil.CAT_RECORDTYPE];

        if(appDataList.isEmpty()) {
            appData = new CAT_Application_Data__c(OwnerId = UserInfo.getUserId());
            appData.RecordTypeId = CAT_HrbpUIUtil.getRecordType(appData,
              CAT_EmployeeTriggerUtil.CAT_RECORDTYPE);
            createAppData = true;
            scrollUpLocked = currCycle.Phase__c != 'Phase 1: No Access' &&
              currCycle.Phase__c != 'Phase 5: Locked and archived';
        } else {
            createAppData = false;
            appData = appDataList[0];
            scrollUpLocked = (currCycle.Phase__c != 'Phase 1: No Access' &&
              currCycle.Phase__c != 'Phase 5: Locked and archived') ?
              appData.Slide_Up_Locked__c : false;
        }

        if(appData.Saved_Filter__c == null) {
            transient List<CAT_Filter_Group__c> defaultFilters = [SELECT Id FROM CAT_Filter_Group__c WHERE Category__c = 'HRBPUI Default Filter' AND RecordType.Name = :CAT_EmployeeTriggerUtil.CAT_RECORDTYPE];

            if(defaultFilters.isEmpty()) {
                appData.Saved_Filter__c = null;
                //loadBlank = true;
            } else {
                appData.Saved_Filter__c = defaultFilters[0].Id;
            }
        }
    }

    /**
     * @description When flagged to createAppData, this method will insert a new app data record for the user.
     **/
    public void createAppData() {
        if(createAppData) {
            insert appData;
            createAppData = false;
        }
    }

    /**
     * @description Gets the basic where clause, to be modified by the FIM.
     * @return      The basic where clause.
     **/
    private String getDefaultWhere() {
        if(prevCycle == null) {
            return 'WHERE CAT_Review_Cycle__c = \'' +
                   currCycle.Id + '\'' +
                   ' ORDER BY Employee__r.Full_Name__c ASC';
        }

        return 'WHERE (CAT_Review_Cycle__c = \'' +
               currCycle.Id + '\'' +
               ' OR CAT_Review_Cycle__c = \'' +
               prevCycle.Id + '\') ' +
               'ORDER BY Employee__r.Full_Name__c ASC';
    }

    /**
     * @description Gets the users tabs.
     * @return      List<CAT_Grid_Tab__c> - the users tabs.
     **/
    public List<CAT_Grid_Tab__c> getTabs() {
        return [SELECT Name FROM CAT_Grid_Tab__c
          WHERE Active__c = true AND
          Default__c = false AND
          OwnerId = :UserInfo.getUserId() AND
          RecordType.Name = :CAT_EmployeeTriggerUtil.CAT_RECORDTYPE
          Order by Tab_Order__c asc];
    }

    /**
     * @description Gets global filters defined for the org.
     * @return      List<CAT_Filter_Group__c> - all defined global filters.
     **/
    global List<CAT_Filter_Group__c> getGlobalFilters(){
           return [Select CAT_Filter_Group__c.Name, CAT_Filter_Group__c.Category__c,CAT_Filter_Group__c.Description__c
                     From CAT_Filter_Group__c
                     WHERE (Category__c = 'HRBPUI Global Filter'
                       OR Category__c = 'HRBPUI Default Filter')
                     AND RecordType.Name = :CAT_EmployeeTriggerUtil.CAT_RECORDTYPE order by Name asc];
    }

    /**
     * @description Gets local filters defined for the user.
     * @return      List<CAT_Filter_Group__c> - local filters defined for the user.
     **/
    global List<CAT_Filter_Group__c> getLocalFilters(){
        return [SELECT Name, Description__c, Category__c FROM CAT_Filter_Group__c
                                                         WHERE (Category__c = 'HRBPUI Local Filter'
                                                         OR Category__c = 'HRBPUI Last Filter')
                                                         AND OwnerId = :UserInfo.getUserId()
                                                         AND RecordType.Name = :CAT_EmployeeTriggerUtil.CAT_RECORDTYPE
                                                         order by Category__c asc, Name asc];
    }

    /**
     * @description Builds out a tooltip for the current filter.
     **/
    private void determineFilterTooltip() {
        filterDescription = '';

        transient Map<String, String> filterNameToLabel = new Map<String, String>();
        for(SelectOption so : fim.filterFields) {
            filterNameToLabel.put(so.getValue(), so.getLabel());
        }

        //Load the values back in
        CAT_Filter__c currentFilter;
        for(CAT_FilterInputManager.FilterOption fo : fim.foList) {
            if (fo.optionValues.Name != null && fo.optionValues.Name.length() > 0 && fo.optionValues.Name != '--None--'
                                             && fo.optionValues.getValue() != null
                                             && fo.optionValues.operator   != null
                                             && fo.optionValues.operator   != '--None--') {

                filterDescription += filterNameToLabel.get(fo.optionValues.Name) + ' ' + fo.optionValues.operator + ' ' +
                                     String.valueOf(fo.optionValues.getValue()) + '\n';
            }
        }

        if(filterDescription != '') {
            filterDescription = filterDescription.substring(0, filterDescription.length() - 1);
        }
    }

    /**
     * @description Applies a specific filter.  Loads data into FIM.  FIM filter is then applied by a rerender of dataTableBuilder.
     * @param       Id specificId  - id of filter to apply
     * @param       Boolean onLoad - when the filter is applied at a time other than onLoad, update the app data record to save the last used filter.
     **/
    private void applySpecificFilter(Id specificId, Boolean onLoad) {
        try{
            transient List<CAT_Filter_Group__c> filterGroups = [SELECT Advanced_Criteria__c, Name, Associated_Head__c,Description__c,
                                                                        (SELECT Name, Label__c, Field__c, Operator__c, Value__c, Active__c, Tag_Count__c From Filters__r)
                                                                  FROM CAT_Filter_Group__c
                                                                 WHERE Id = :specificId AND RecordType.Name = :CAT_EmployeeTriggerUtil.CAT_RECORDTYPE];

            if(!filterGroups.isEmpty()) {
                transient CAT_Filter_Group__c filterGroup = filterGroups[0];
                transient List<CAT_Filter__c> filters     = filterGroup.Filters__r;

                 String myFilterDescription = '';

                //Reset the Filter Input Manager and add rows as necessary
                fim.clearFilters();

                while(filters.size() > fim.foList.size()) {
                    fim.addRow();
                }

                //Map out filter numbers to the filters
                Map<Integer, CAT_Filter__c> filterNumberToFilter = new Map<Integer, CAT_Filter__c>();
                for(CAT_Filter__c filter : filters) {
                    filterNumberToFilter.put(Integer.valueOf(filter.Tag_Count__c), filter);
                }

                //Load the values back in
                CAT_Filter__c currentFilter;

                for(CAT_FilterInputManager.FilterOption fo : fim.foList) {
                    Integer currKey = Integer.valueOf(fo.filterNumber);
                    if(filterNumberToFilter.containsKey(currKey)) {
                        currentFilter = filterNumberToFilter.get(currKey);
                        myFilterDescription += currentFilter.Label__c + ' ' + currentFilter.Operator__c + ' '+ currentFilter.Value__c + '\n';
                        fo.optionValues.Name = currentFilter.Field__c;
                        ApexPages.currentPage().getParameters().put('optionNumber', fo.filterNumber);
                        fim.buildOperatorList();
                        if(currentFilter.Value__c.toLowerCase() == 'true') {
                             fo.optionValues.setValue('Yes');
                        } else if(currentFilter.Value__c.toLowerCase() == 'false') {
                             fo.optionValues.setValue('No');
                        } else {
                              fo.optionValues.setValue(currentFilter.Value__c);
                        }
                        fo.optionValues.operator = currentFilter.Operator__c;
                    }
                }
                myFilterDescription = myFilterDescription.substring(0, myFilterDescription.length() -1 );
                //Still needs to handle advanced criteria
                if(filterGroup.Advanced_Criteria__c != null && filterGroup.Advanced_Criteria__c.trim() != '') {
                    fim.enableAdvancedFilters = true;
                    fim.advancedCriteria      = filterGroup.Advanced_Criteria__c;
                }

                appliedFilter = filterGroup.Name;
                filterDescription = filterGroup.Description__c;
                lastQuery = fim.getSOQLQuery();

                if(!onLoad) {
                    createAppData();
                    appData.Saved_Filter__c = filterGroup.Id;
                    update appData;
                }
            }
        } catch(Exception e) { }
    }

    /**
     * @description Called when clicking on a saved filter.  Applies the saved filter.
     **/
    global void applyFilter() {
        applySpecificFilter((Id)ApexPages.currentPage().getParameters().get('filterId'), false);
    }

    /**
     * @description Deletes a saved filter.
     * @return      PageReference - required by action methods.
     **/
    public PageReference deleteFilter() {
        try{
            delete [SELECT Id FROM CAT_Filter_Group__c
              WHERE Id = :ApexPages.currentPage().getParameters().get('filterId')
              AND RecordType.Name = :CAT_EmployeeTriggerUtil.CAT_RECORDTYPE];
        } catch(Exception e){
            return null;
        }

        return null;
    }

    /**
     * @description Resets the applied filter message to one which is appropriate for the "Apply Filter" button.
     *              Updates the user's CAT_Application_Data__c record to null out the field which tracks last applied filter.
     **/
    public void resetAppliedFilter() {
        determineFilterTooltip();
        lastQuery     = fim.getSOQLQuery();
        appliedFilter = '*Unsaved Filter*';
        saveApplied();
    }

    /**
     * @description Clear the select filter and change the app data record to null out the "last used filter" field.
     **/
    public void clearFilters() {
        createAppData();
        appData.Saved_Filter__c = null;
        update appData;
        fim.clearFilters();
        lastQuery         = fim.getSOQLQuery();
        appliedFilter     = '*None*';
        filterDescription = 'No Filter Applied';
    }

    public void saveApplied() {
        System.SavePoint sp = Database.setSavePoint();

        try {
            if(appliedFilter == '*Unsaved Filter*') {
                delete [SELECT Id FROM CAT_Filter_Group__c
                  WHERE Category__c = 'HRBPUI Last Filter'
                  AND OwnerId = :UserInfo.getUserId()
                  AND RecordType.Name = :CAT_EmployeeTriggerUtil.CAT_RECORDTYPE];

                //Create and insert header
                CAT_Filter_Group__c lastApplied = new CAT_Filter_Group__c(
                  Category__c = 'HRBPUI Last Filter',
                  OwnerId = UserInfo.getUserId(),
                  Advanced_Criteria__c = fim.advancedCriteria,
                  Name = 'My Last Unsaved Filter',
                  RecordTypeId = CAT_HrbpUIUtil.getRecordType(
                    new CAT_Filter_Group__c(),
                    CAT_EmployeeTriggerUtil.CAT_RECORDTYPE) );

                insert lastApplied;

                //Create and insert children
                insert getFiltersFromInputManager(lastApplied.Id);

                createAppData();
                appData.Saved_Filter__c = lastApplied.Id;
                update appData;
            }
        } catch(Exception ex) {
            Database.rollback(sp);
        }
    }

    private List<CAT_Filter__c> getFiltersFromInputManager(Id groupId) {
        //Save the filters themselves
        CAT_Filter__c tempFilter;
        transient List<CAT_Filter__c> filters = new List<CAT_Filter__c>();
        transient Map<String, String> filterNameToLabel = new Map<String, String>();
        for(SelectOption so : fim.filterFields) {
            filterNameToLabel.put(so.getValue(), so.getLabel());
        }

        for(CAT_FilterInputManager.FilterOption fo : fim.foList) {
            if (fo.optionValues.Name != null && fo.optionValues.Name.length() > 0 && fo.optionValues.Name != '--None--'
                                             && fo.optionValues.getValue() != null
                                             && fo.optionValues.operator   != null
                                             && fo.optionValues.operator   != '--None--') {

                tempFilter = new CAT_Filter__c();
                tempFilter.Active__c       = true;
                tempFilter.Filter_Group__c = groupId;
                tempFilter.Field__c        = fo.optionValues.Name;
                tempFilter.Label__c        = filterNameToLabel.get(tempFilter.Field__c);
                tempFilter.Operator__c     = fo.optionValues.operator;
                tempFilter.Value__c        = String.valueOf(fo.optionValues.getValue());
                tempFilter.Tag_Count__c    = Integer.valueOf(fo.filterNumber);

                filters.add(tempFilter);
            }
        }

        return filters;
    }

    /**
     * @description Saves the current state of the filter component.
     **/
    global void saveFilter() {
        String tempQuery = fim.getSOQLQuery();
        if( tempQuery == null ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Your filters did not save because there are errors in the critera.<br />' + fim.errorDisplay));
            return;
        }

        CAT_Filter_Group__c newFilter;
        System.SavePoint sp = Database.setSavePoint();

        try {
            // If Advanced Criteria has been given, validate it
            if (fim.enableAdvancedFilters && fim.advancedCriteria.length() >= 1) {
                Set<String> numbersUsed = new Set<String>();

                for (CAT_FilterInputManager.FilterOption fo : fim.foList) {
                    numbersUsed.add(fo.filterNumber);
                }

                if (numbersUsed.size() >= 1 && !fim.verifyAdvancedCriteria(numbersUsed)) {
                    throw new HRBPUIException('Advanced Criteria is not valid.');
                }
            }

            //Create Filter with filterName, Category of HRBPUI Local, and ownerid of the current user
            newFilter = new CAT_Filter_Group__c();
            newFilter.Name                 = filterName;
            newFilter.Category__c          = 'HRBPUI Local Filter';
            newFilter.OwnerId              = UserInfo.getUserId();
            newFilter.Advanced_Criteria__c = fim.advancedCriteria;
            newFilter.Associated_Head__c   = null;
            newFilter.RecordTypeId         =
              CAT_HrbpUIUtil.getRecordType(
            newFilter, CAT_EmployeeTriggerUtil.CAT_RECORDTYPE);

            //coupleWithHead = false;
            insert newFilter;

            appliedFilter = filterName;
            lastQuery = tempQuery;
            determineFilterTooltip();

            transient List<CAT_Filter__c> filters =
                getFiltersFromInputManager(newFilter.Id);

            if(filters.isEmpty()) {
                throw new HRBPUIException('You cannot save a filter that does not filter.');
            } else {
                insert filters;
                createAppData();
                appData.Saved_Filter__c = newFilter.Id;
                update appData;
                insert new CAT_Metric__c(
                  Type__c         = 'Created a Filter',
                  Current_User__c = UserInfo.getUserId(),
                  Details__c      = 'Filter Name: ' + appliedFilter,
                  Application__c = 'CAT');
            }

        } catch (Exception e) {
            fim.errorDisplay = e.getMessage();
            Database.rollback(sp);
        }
    }

    //This is not logging right.
    //Seems to be logging the first twice as often as it should
    private static void logSearchMetrics(String content,
                                         String type,
                                         String filterName) {

      List<CAT_Metric__c> metrics = new List<CAT_Metric__c>();

      if(String.isNotEmpty(content)) {
        metrics.add(new CAT_Metric__c(
          Details__c      = 'Searched On: ' + content,
          Current_User__c = UserInfo.getUserId(),
          Type__c         = type == 'name' ? 'Primary Search By Name' :
                                             'Primary Search By Mgt Chain',
          Application__c = 'CAT'));
      }

      if(filterName != '*None*' &&
         filterName != '*Unsaved Filter*' &&
         filterName != 'My Last Unsaved Filter') {

        metrics.add(new CAT_Metric__c(
          Details__c      = 'Filter Name: ' + filterName,
          Current_User__c = UserInfo.getUserId(),
          Type__c         = 'Used a Filter',
          Application__c = 'CAT'));
      }

      insert metrics;
    }

    /** INNER CLASSES **/

    global class FilterData {
        public Double value   {get; set;}
        public String name    {get; set;}
        public String rowName {get; set;}
        public Double denom   {get; set;}

        public FilterData(Double val, String fdName, String fgName, Double fgDenom) {
            value   = val;
            name    = fdName;
            rowName = fgName;
            denom   = fgDenom;
        }
    }

    //This is used for retrieving the historical data for an employee.  Since this
    //data now will hold the label instead of API name, a separate class is used
    global class HistoricalActivityData {
        public String fieldName     {get; set;}
        public DateTime changeDate  {get; set;}
        public String oldFieldValue {get; set;}
        public String newFieldValue {get; set;}
        public String alias         {get; set;}

        public HistoricalActivityData(String fieldName, DateTime changeDate, String oldFieldValue, String newFieldValue, String alias){
            this.fieldName      = fieldName;
            this.changeDate     = changeDate;
            if (oldFieldValue == 'false' ||
                oldFieldValue == 'true'){
                    this.oldFieldValue = oldFieldValue == 'true' ? 'Yes' : 'No';
            } else {
                this.oldFieldValue  = oldFieldValue;
            }
            if (newFieldValue == 'false' ||
                newFieldValue == 'true'){
                    this.newFieldValue = newFieldValue == 'true' ? 'Yes' : 'No';
            } else {
                this.newFieldValue  = newFieldValue;
            }
            this.alias          = alias;
        }

    }

    //This is used for retrieving the historical performance data for an employee.  Since this
    //data lives in multiple location, a separate global class is used.
    global class PerformanceData {
        public String period        {get; set;}
        public String assessment    {get; set;}
        public String promotion     {get; set;}
        public String DE     {get; set;}

        public PerformanceData(String period, String assessment,
          String promotion, String DE){
            this.period     = period;
            this.assessment = assessment == '' || assessment == null ?
              'No Assessment': assessment;
            this.promotion  = promotion  == null ? '-' : promotion;
            this.DE = DE == null ? '-' : DE;
        }
    }

    global class Chunk {
      public List<ChunkRow> rows      {get; set;}
      public Integer        start     {get; set;}
      public Integer        expected  {get; set;}
      public String         queryCode {get; set;}

      public Chunk(List<ChunkRow> chunkRows, Integer startAt,
                   Integer expectedRows, String queryKey) {

        rows      = chunkRows;
        start     = startAt;
        expected  = expectedRows;
        queryCode = queryKey;
      }
    }

    global class ChunkRow {

      public CAT_Focal_File__c record {get; set;}
      public String            extras {get; set;}

      public ChunkRow(CAT_Focal_File__c focalFile, String extra) {
        record = focalFile;
        extras = extra;
      }
    }

    /** CUSTOM EXCEPTIONS **/

    private class HRBPUIException extends Exception {}

    /** REMOTE ACTIONS **/

    @RemoteAction
    global static CAT_Employee__c[] getEmployeeNames() {
        return CAT_HrbpUIUtil.getEmployeeNames();
    }

    /**
     * @description Gets all columns.  Used by column picker.
     * @return      CAT_Grid_Column__c[] - list of all columns in the system
     **/
    @RemoteAction
    global static CAT_Grid_Column__c[] getAllColumns(String tierSetting) {
        return CAT_HrbpUIUtil.getAllColumns(tierSetting);
    }

     /**
     * @description Gets all tab column couplers for a user
     * @param       String activeTab            - the currently selected tab.
     * @return      CAT_Tab_Column_Coupler__c[] - all tab column couplers for a user
     **/
    @RemoteAction
    global static CAT_Tab_Column_Coupler__c[] getCouplers(String activeTab, String isTierUser) {
        return CAT_HrbpUIUtil.getCouplers(activeTab, isTierUser);
    }

    /**
     * @description Calls out to Rypple for a single employee's worth of data, returns results
     * @param       String ryppleId   - Id of focal file to update with new data
     * @return      CAT_Focal_File__c - new data from rypple for the focal
     **/
    @RemoteAction
    global static CAT_Focal_File__c updateRyppleRecord(String ryppleId){
        return CAT_HrbpUIUtil.updateRyppleRecord(ryppleId);
    }

    /**
     * @description Calls out to Workday for a single employee's worth of data
     * @param       String focalId   - Id of focal file to update with new data
     * @return      CAT_Focal_File__c - new data from rypple for the focal
     **/
    @RemoteAction
    global static CAT_Focal_File__c updateWorkdayRecord(String focalId){
      return CAT_HrbpUIUtil.updateWorkdayRecord(focalId);
    }

    /**
     * @description Method is fired when user tabs out of a cell on the HRBP UI.  Saves the modified data,
     *              and requeries for the saved data, to get the update in formulas, and returns the results.
     * @param       String sobject    - the type of sobject that the field specified in the columnName parameter exists on.
     * @param       String dataType   - the data type of the field specified by the columnName.
     * @param       String recordId   - the id of the record to update data on.
     * @param       String columnName - the field api name to update data in.
     * @param       String newValue   - the new value for the field.
     * @return      CAT_Focal_File__c - The focal file which has been requeried for.
     **/
    @RemoteAction
    global static CAT_Focal_File__c saveToServer(String sobjectName, String dataType, String ffId, String osId, Double propMeritIncr, String columnName,
                                                 String newValue, String oldValue, Boolean firstRun, String overrideState, Double currOverrideAmt, Double dsChange, String approvalComment) {
        return CAT_HrbpUIUtil.saveToServer(sobjectName, dataType, ffId, osId, propMeritIncr, columnName, newValue, oldValue, firstRun, overrideState, currOverrideAmt, dsChange, approvalComment);
    }

    /**
     * @description  Updates currency code on equity data and returns altered data.
     * @param        String employeeId         - Id of employee to get data for.
     * @param        String newCurrencyCode    - New currencyisocode
     * @param        String tier               - tier category
     * @return       CAT_Equity_Aggregate__c[] - List of results
     **/
    @RemoteAction
    global static CAT_Equity_Aggregate__c[] updateAndGetEquityData(String employeeId, String currencyIsoCode, String tier) {
        return CAT_HrbpUIUtil.updateAndGetEquityData(employeeId, currencyIsoCode, tier);
    }

   /**
     * @description  Finds and returns all historical data for this employee
     * @param        String focalId, focalHeader, integrationHeader        - Id's so we know which historical data to bring back
     * @return       String[] - List of results
     **/
    @RemoteAction
    global static List<HistoricalActivityData> retrieveHistoricalData(String focalId, String intHeader, String osRefer) {
        return CAT_HrbpUIUtil.retrieveHistoricalData(focalId, intHeader, osRefer);
    }

     /**
     * @description  Finds and returns the historical performance data for the Employee
     * @param        String workdayId  - employee workday id to get history for
     * @param        String assessment - last cycle assessment
     * @param        String promotion  - last cycle promotion
     * @return       List<PerformanceData> - performance history
     **/
    @RemoteAction
    global static List<PerformanceData> retrievePerformanceHistoricalData(String workdayId, String assessment, String promotion) {
        return CAT_HrbpUIUtil.retrievePerformanceHistoricalData(workdayId, assessment, promotion);
    }

    /**
     * @description Gets the sum of the existing vested shares up until the start of the current year.
     * @param       String empId - the id of the employee to aggregate data on.
     * @param       String tier  - tier category
     * @return      Double       - amount of existing vested
     **/
    @RemoteAction
    global static CAT_Equity_Aggregate__c[] getExistingVested(String empId, String tier) {
        return CAT_HrbpUIUtil.getExistingVested(empId, tier);
    }

    /**
     * @description Gets the default set of data for analytics.
     * @param       String rcId - most recent review cycle id.
     * @return      List<CAT_Graphable_Pair__c> - aggregated data ready for analytics to process.
     **/
    @RemoteAction
    global static List<CAT_Graphable_Pair__c> getDefaultTableData(String rcId) {
        return CAT_HrbpUIUtil.getDefaultTableData(rcId);
    }

    /**
     * @description Aggregates and returns a row of focal-node data for analytics
     * @param       String focalId     - id of focal to aggregate on
     * @param       String currCycleId - id of current review cycle
     * @return      List<FilterData>   - aggregated data ready for analytics to process.
     **/
    @RemoteAction
    global static List<FilterData> getFocalFileNameValuePairs(String focalId, String currCycleId) {
        return CAT_HrbpUIUtil.getFocalFileNameValuePairs(focalId, currCycleId);
    }

    /**
     * @description Aggregates and returns a row of filter-node data for analytics
     * @param       String filterId    - id of filter to generate analytics for
     * @param       String currCycleId - id of current review cycle
     * @param       String additionalClause - This is the extra filter, that you can stack on top of the filters.
     * @return      List<FilterData>   - aggregated data ready for analytics to process.
     **/
    @RemoteAction
    global static List<FilterData> getFilterNameValuePairs(String filterId, String currCycleId, String additionalClause) {
        return CAT_HrbpUIUtil.getFilterNameValuePairs(filterId, currCycleId, additionalClause);
    }

    /**
     * @description Saves the slide up's position to the db
     * @param       Lock position - true is up, false is down
     **/
    @RemoteAction
    global static void updateSlideUpLock(Boolean lockPosition) {
        CAT_HrbpUIUtil.updateSlideUpLock(lockPosition);
    }

    /**
     * @description Get fully up-to-date focal file data
     * @param       List<String> ffIds   - focals to retrieve
     * @param       Double currStock     - current price per share
     * @return      String               - requested focals as json
     **/
    @RemoteAction
    global static String getFocalFiles(List<String> ffIds, Double currStock) {
        return CAT_HrbpUIUtil.getFocalFiles(ffIds, currStock);
    }

    @RemoteAction
    global static Chunk loadRows(String  query,
                                 String  queryCode,
                                 Integer rowsLoaded,
                                 Integer rowsExpected,
                                 Double  currStock,
                                 String  primaryFilterContent,
                                 String  primaryFilterType,
                                 String  filterName) {
      system.debug('#### ' + rowsExpected);
      String  firstQuery  = query;
      Integer start       = rowsLoaded;
      String  secondQuery =
        'SELECT ' +
        CAT_HrbpUIUtil.getSelectClause(CAT_HrbpUIUtil.getFieldsToSelect2()) +
        ' FROM CAT_Focal_File__c WHERE ' + firstQuery.split('WHERE')[1];
      system.debug('#### firstQuery ' + firstQuery);  
      system.debug('#### secondQuery ' + secondQuery);  
      if(rowsExpected == -1) {
        system.debug('#### query ' + query);  
        List<String> queryList = query.split(' WHERE ');
        system.debug('#### queryList ' + queryList);
        rowsExpected = Database.countQuery(
          'SELECT count() FROM CAT_Focal_File__c WHERE ' +
          queryList[1].split(' ORDER BY')[0]);
      }
      system.debug('#### rowsExpected ' + rowsExpected);   
      if (rowsExpected == 0) {
        return new Chunk(new List<ChunkRow>(), 0, 0, queryCode);
      }

      List<ChunkRow> chunkRows   = new List<ChunkRow>();
      Map<Id, CAT_Focal_File__c> idToFocal =
        new Map<Id, CAT_Focal_File__c>(
        (List<CAT_Focal_File__c>) Database.query(secondQuery));

      Double valueIncreasing         = 0,
             valueIncreasePercent    = 0,
             projectedValue          = 0,
             projectedMarketPosition = 0,
             projectedTTC            = 0,
             originalFinalValue      = 0,
             totalEQ                 = 0,
             usdGrantValue           = 0,
             usdDeValue              = 0,
             usdTotalValue           = 0,
             prevGrantUSD            = 0,
             prevDEUSD               = 0,
             prevTotalEQRSU          = 0,
             prevTotalEQUSD          = 0,
             vestedRSU               = 0;

      for(CAT_Focal_File__c focal : Database.query(firstQuery)) {
        chunkRows.add(new ChunkRow(
          idToFocal.get(focal.Id),
          CAT_HrbpUIUtil.getComplexJson(
            valueIncreasing,
            valueIncreasePercent,
            projectedValue,
            projectedMarketPosition,
            projectedTTC,
            originalFinalValue,
            totalEQ,
            usdGrantValue,
            usdDeValue,
            usdTotalValue,
            ++rowsLoaded,
            currStock,
            prevGrantUSD,
            prevDEUSD,
            prevTotalEQRSU,
            prevTotalEQUSD,
            vestedRSU,
            focal)));
      }

      if(rowsLoaded == rowsExpected) {
        CAT_HrbpUIUtil.updatePrimaryFilters(
          primaryFilterContent, primaryFilterType);
        
        logSearchMetrics(primaryFilterContent, primaryFilterType, filterName);
      }
      system.debug('#### ' + chunkRows);
      system.debug('#### ' + start);
      system.debug('#### ' + rowsExpected);
      system.debug('#### ' + queryCode);
      return new Chunk(chunkRows,
                       start,
                       rowsExpected,
                       queryCode);
    }

    @RemoteAction
    global static Boolean getAllowEdit() {
      return CAT_HrbpUIUtil.getAllowEdit();
    }
}