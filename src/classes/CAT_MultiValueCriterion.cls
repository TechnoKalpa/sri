public class CAT_MultiValueCriterion extends CAT_AbstractCriterion {

    private List<CAT_AbstractCriterion> values;

    /**
     * @description  Constructor for a filter statement on a field with data type picklist or multivalue picklist.
     * @param       String aName - the name of the filter statement.
     * @param       String op - the operator in the filter statement.
     * @param       List<CAT_AbstractCriterion> acList - the criterion for for each value.  
     */
    public CAT_MultiValueCriterion(String aName, String op, List<CAT_AbstractCriterion> acList) {
        name = aName;
        operator = op;
        values = acList;
    }
    
    /*
     * @description  Adds another value to this multi value filter statement.
     * @param        CAT_AbstractCriterion criterion - the new value to add to this multi value filter statement.
     */
    public void addValue(CAT_AbstractCriterion criterion) {
        values.add(criterion);
    }
    
    /*
     * @description  Accessor method for all values in this multi value filter statement.
     * @return       List<CAT_AbstractCriterion> - the values in this multi value criterion.
     */
    public List<CAT_AbstractCriterion> getValues() {
        return values;
    }
    
    /*
     * @description Combines all the individual CAT_AbstractCriterion that make up this CAT_MultiValueCriterion to create multiple 
     *              filter statements that work in conjunction to operate on picklists and multivalue picklists.  
     *              NOTE: This is not the ideal definiton of this method.  It currently creates creates a MultiValue picklist
     *              query with the format "(field__c includes ('value1') *operator* field__c includes ('value2'))" when it should just
     *              do "field__c includes ('value1,value2')"
     * @return      String - multiple filter statements concatenated by the operator of this multi value criterion.
     */
    public override String toSOQL() {
        String soql= '';
        String concatenator = ' ' + operator + ' ';
        
        for(CAT_AbstractCriterion value : values) {
                soql += value.toSOQL() + concatenator;
        }
        return values.size() > 1 ? '( ' + soql.substring(0, soql.length() - concatenator.length()) + ' )' : soql.substring(0, soql.length() - concatenator.length());
    }           

    /** 
     * @description In the case of the CAT_MultiValueCriterion, these two methods have no purpose,
     *              and only exist because they must.
     */
    public override void setValue(Object aValue) {}
    public override Object getValue() { return this; }
}