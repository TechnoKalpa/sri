/**
 * 
 * @date        10/25/2011
 * @description Contains various methods used to calculate graphable data.  Largely used by CAT_AggregationBatch.cls
 **/
public without sharing class CAT_AggregationUtil {

    private static final String NON_REGRET_TERM = 'non-regrettable termination';
    private static final String REGRET_TERM = 'regrettable termination';

    /**
     * @description Runs the batch job which calulates name value pairs for graphing, for filters.
     **/
    public static void aggregateByFilter(Id jobId, Datetime startTime) {
        Id currentCycleId = getCurrentReviewCycle();
        aggregateByFilter(currentCycleId, jobId, startTime);
    }

    /**
     * @description Runs the batch job which calulates name value pairs for graphing, for filters.
     * @param       Id currentCycleId - cycle to run analytics for.
     **/
    public static void aggregateByFilter(Id currentCycleId, Id jobId, Datetime startTime) {
      Database.executeBatch( new CAT_AggregationFilterBatch(
        currentCycleId, jobId, startTime ),
        CAT_AggregationFilterBatch.BATCH_SIZE );
    }

    /**
     * @description Runs the batch job which calulates name value pairs for graphing.
     **/
    public static void aggregateByFocal() {
        Id currentCycleId = getCurrentReviewCycle();
        aggregateByFocal(currentCycleId);
    }

    /**
     * @description Runs the batch job which calulates name value pairs for graphing.
     * @param       Id currentCycleId - cycle to run analytics for.
     **/
    public static void aggregateByFocal(Id currentCycleId) {
        if([SELECT count() FROM AsyncApexJob WHERE Status = 'Processing' OR Status = 'Queued'] < 5) {
            //Database.executeBatch( new CAT_AggregationBatch( currentCycleId ), CAT_AggregationBatch.BATCH_SIZE );
        }
    }

    public static void aggregateByDeletion(Id jobId) {
        Id currentCycleId = getCurrentReviewCycle();
        aggregateByDeletion( currentCycleId, jobId );
    }

    public static void aggregateByDeletion(Id currentCycleId, Id jobId) {
      String currentCycleShort = ('' + currentCycleId).substring(0, 15);
      String query = 'SELECT Id FROM CAT_Graphable_Pair__c WHERE ';
      //Database.executeBatch( new CAT_AggregationDeletionBatch(query, currentCycleId, jobId ) );
    }

    /**
     * @description Calculates name value pairs for a single focal.
     * @param       CAT_Focal_File__c focal     - focal to calculate pairs for
     * @return      List<CAT_Graphable_Pair__c> - graphable pairs for the current focal
     **/
    public static List<CAT_Graphable_Pair__c> calculateNameValuePairs(
        CAT_Focal_File__c focal, Date prevCycleEnd,
        List<CAT_Performance_Plan_LKUP__c> assessments, Double assessmentDenom,
        Double promotionDenom, Double otherDenom) {

        List<CAT_Graphable_Pair__c> pairs = new List<CAT_Graphable_Pair__c>();
        pairs.addAll(calculateAssessments(focal, prevCycleEnd, assessments,
            assessmentDenom));
        pairs.addAll(calculatePromotion(focal, promotionDenom));

        Boolean isQ3 =
            assessments[0].Program__r.Cat_Review_Cycle__r.Quarter__c != 'Q3';

        if(isQ3) {
            pairs.addAll(calculateDiscretionarySalary(focal, otherDenom));
            pairs.addAll(calculateDiscretionaryEquity(focal, otherDenom));
        }

        return pairs;
    }

    /**
     * @description Calculates assessments for a given focal
     * @param       CAT_Focal_File__c focal                        - Calculate assessments for this focal
     * @param       List<CAT_Performance_Plan_LKUP__c> assessments - All possible assessments for current focal
     * @return      List<CAT_Graphable_Pair__c>                    - New name value pairs
     **/
    public static List<CAT_Graphable_Pair__c> calculateAssessments
      (CAT_Focal_File__c focal, Date prevCycleEnd,
      List<CAT_Performance_Plan_LKUP__c> assessments, Decimal denom) {

        List<CAT_Graphable_Pair__c> pairs = new List<CAT_Graphable_Pair__c>();
        CAT_Graphable_Pair__c pair;

        for(CAT_Performance_Plan_LKUP__c assessment : assessments) {
            pair = new CAT_Graphable_Pair__c();
            pair.Name = assessment.Alternate_Title__c;
            pair.CAT_Focal_File__c = focal.Id;
            pair.Value__c = 0;
            pair.Value_Denominator__c = denom;
            pair.Order__c = assessment.Reporting_Order__c;

            if( focal.Assessment_Title__c == assessment.Title__c && focal.Job_Level__c != null && focal.Job_level__c.trim() != ''  ) {
                if((assessment.Title__c != NON_REGRET_TERM &&
                    assessment.Title__c != REGRET_TERM) ||
                    focal.Date_of_Termination__c <= prevCycleEnd) {

                  pair.Value__c++;
                }
            }

            pairs.add(pair);
        }

        return pairs;
    }

    /**
     * @description Calculates assessments for the filter job
     **/
    public static Set<String> calculateAssessmentsByFilter(CAT_Focal_File__c focal, List<CAT_Performance_Plan_LKUP__c> assessments) {
        Set<String> assessmentsHad = new Set<String>();

        for(CAT_Performance_Plan_LKUP__c assessment : assessments) {
            if( focal.Assessment_Title__c == assessment.Title__c ) {
                assessmentsHad.add(assessment.Title__c);
            }
        }

        return assessmentsHad;
    }

    /**
     * @description Calculates promotion for a given focal
     * @param       CAT_Focal_File__c focal     - Focal to calculate promotion for
     * @param       Decimal denom               - denominator for percentages
     * @return      List<CAT_Graphable_Pair__c> - New name value pairs
     **/
    public static List<CAT_Graphable_Pair__c> calculatePromotion(CAT_Focal_File__c focal, Decimal denom) {
        List<CAT_Graphable_Pair__c> pairs = new List<CAT_Graphable_Pair__c>();

        CAT_Graphable_Pair__c pair = new CAT_Graphable_Pair__c();

        pair.Name                 = System.Label.CAT_Analytics_Promotion;
        pair.CAT_Focal_File__c    = focal.Id;
        pair.Value__c             = 0;

        if(denom == -1 &&/* focal.Promotion_Eligible__c != null &&
          focal.Promotion_Eligible__c == System.Label.CAT_Yes &&*/
          !focal.Employee__r.Rypple_Chain_Exec__c) {
            pair.Value_Denominator__c = 1;
        } else if(denom != -1) {
            pair.Value_Denominator__c = denom;
        } else {
            pair.Value_Denominator__c = 0;
        }

        pair.Order__c             = Integer.valueOf(System.Label.CAT_Analytics_Promotion_Order);

        if(/*focal.Promotion_Flag__c == System.Label.CAT_Yes &&*/ focal.Job_Level__c != null && focal.Job_level__c.trim() != '') {
            pair.Value__c++;
        }

        pairs.add(pair);
        return pairs;
    }

    /**
     * @description Calculates DS for a given focal
     * @param       CAT_Focal_File__c focal     - Focal to calculate DS for
     * @param       Decimal denom               - denominator for percentages
     * @return      List<CAT_Graphable_Pair__c> - New name value pairs
     **/
    public static List<CAT_Graphable_Pair__c> calculateDiscretionarySalary(CAT_Focal_File__c focal, Decimal denom) {
        List<CAT_Graphable_Pair__c> pairs = new List<CAT_Graphable_Pair__c>();

        CAT_Graphable_Pair__c pair = new CAT_Graphable_Pair__c();

        pair.Name                 = System.Label.CAT_Analytics_DS;
        pair.CAT_Focal_File__c    = focal.Id;
        pair.Value__c             = 0;

        if(denom == -1 && focal.ae__c == System.Label.CAT_Yes &&
          !focal.Employee__r.Rypple_Chain_Exec__c) {
            pair.Value_Denominator__c = 1;
        } else if(denom != -1) {
            pair.Value_Denominator__c = denom;
        } else {
            pair.Value_Denominator__c = 0;
        }

        pair.Order__c             = Integer.valueOf(System.Label.CAT_Analytics_DS_Order);

        if(focal.prop_ds_approval__c == System.Label.CAT_Yes && focal.Job_Level__c != null && focal.Job_level__c.trim() != '') {
            pair.Value__c++;
        }

        pairs.add(pair);
        return pairs;
    }

    /**
     * @description Calculates DE for a given focal
     * @param       CAT_Focal_File__c focal     - Focal to calculate DS for
     * @param       Decimal denom               - denominator for percentages
     * @return      List<CAT_Graphable_Pair__c> - New name value pairs
     **/
    public static List<CAT_Graphable_Pair__c> calculateDiscretionaryEquity(CAT_Focal_File__c focal, Decimal denom) {
        List<CAT_Graphable_Pair__c> pairs = new List<CAT_Graphable_Pair__c>();

        CAT_Graphable_Pair__c pair = new CAT_Graphable_Pair__c();

        if(focal.prop_de_tier__c == 'Tier 1'){
          pair.Name = System.Label.CAT_Analytics_DE_T1;
        } else {
          pair.Name = System.Label.CAT_Analytics_DE_T2;
        }
        pair.CAT_Focal_File__c    = focal.Id;
        pair.Value__c             = 0;

        if(denom == -1 && focal.ae__c != null &&
          focal.ae__c == System.Label.CAT_Yes &&
          !focal.Employee__r.Rypple_Chain_Exec__c) {
            pair.Value_Denominator__c = 1;
        } else if(denom != -1) {
            pair.Value_Denominator__c = denom;
        } else {
            pair.Value_Denominator__c = 0;
        }

        pair.Order__c             = Integer.valueOf(System.Label.CAT_Analytics_DE_Order);

        if(focal.prop_de_approval__c == System.Label.CAT_Yes &&
          focal.Job_Level__c != null && focal.Job_level__c.trim() != '') {
            pair.Value__c++;
        }

        pairs.add(pair);
        return pairs;
    }

    /**
     * @description Gets the id of the current review cycle.
     * @return      Id - current review cycle id.
     **/
    public static Id getCurrentReviewCycle() {

        List<CAT_Review_Cycle__c> newestCycles = CAT_HrbpUIUtil.getCurrentReviewCycle(false);

        if( !newestCycles.isEmpty() ) {
            return newestCycles[0].Id;
        }

        return null;
    }

    /**
     * @description Instantiates a filter input manager with filter data
     * @param       CAT_FilterInputManager fim      - FIM to instantiate
     * @param       CAT_Filter_Group__c filterGroup - Filter group data to instantiate with
     * @param       List<CAT_Filter__c> filters     - Filters to instiatiate with
     **/
    public static void instantiateFIM(CAT_FilterInputManager fim, CAT_Filter_Group__c filterGroup, List<CAT_Filter__c> filters) {
        //Reset the Filter Input Manager and add rows as necessary
        fim.clearFilters();

        while(filters.size() > fim.foList.size()) {
            fim.addRow();
        }

        //Map out filter numbers to the filters
        Map<Integer, CAT_Filter__c> filterNumberToFilter = new Map<Integer, CAT_Filter__c>();
        for(CAT_Filter__c filter : filters) {
            filterNumberToFilter.put(Integer.valueOf(filter.Tag_Count__c), filter);
        }

        //Load the values back in
        CAT_Filter__c currentFilter;

        for(CAT_FilterInputManager.FilterOption fo : fim.foList) {
            Integer currKey = Integer.valueOf(fo.filterNumber);

            if(filterNumberToFilter.containsKey(currKey)) {
                currentFilter = filterNumberToFilter.get(currKey);
                fo.optionValues.Name = currentFilter.Field__c;

                fim.optionNumber = fo.filterNumber;
                fim.buildOperatorList();

                if(currentFilter.Value__c.toLowerCase() == 'true') {
                     fo.optionValues.setValue(System.Label.CAT_Yes);
                } else if(currentFilter.Value__c.toLowerCase() == 'false') {
                     fo.optionValues.setValue(System.Label.CAT_No);
                } else {
                      fo.optionValues.setValue(currentFilter.Value__c);
                }

                fo.optionValues.operator = currentFilter.Operator__c;
            }
        }

        //Still needs to handle advanced criteria
        if(filterGroup.Advanced_Criteria__c != null && filterGroup.Advanced_Criteria__c.trim() != '') {
            fim.enableAdvancedFilters = true;
            fim.advancedCriteria      = filterGroup.Advanced_Criteria__c;
        }
    }



}