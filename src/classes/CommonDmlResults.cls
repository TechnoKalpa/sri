public class CommonDmlResults {

    public static integer resultLimit    = 2000;
    public static boolean includeSuccess = false;

    public LIST<CommonDmlResults.Result> results {get; private set;}
      { results = new LIST<CommonDmlResults.Result>(); }

    public LIST<string> messages = new LIST<string>();  
   
    public CommonDmlResults() {}

    public CommonDmlResults(LIST<object> lst) { this(lst,null);}
  
    public CommonDmlResults(LIST<object> lst,  LIST<sobject> records) {
      Integer cnt = 0;
      for (Object o : lst) {
        try {
          Result  r;
          if (o instanceof Database.Saveresult ) {
            r = new Result((Database.Saveresult) o, records[cnt]);
          } else if (o instanceof Database.Upsertresult ) {
            r = new Result((Database.Upsertresult) o, records[cnt]);
          } else if (o instanceof Database.Deleteresult) {
            r = new Result((Database.Deleteresult) o, records[cnt]);
          } else if (o instanceof Database.Undeleteresult) {
            r = new Result((Database.Undeleteresult) o, records[cnt]);
          } else {
            throw new InvalidResultException('Invalid DML Result.');
          }
          if (includeSuccess || (!r.success) ) {
            if (results.size() < resultLimit) {
              this.add(r);
            }
          }
        } catch(exception ex) {}    
          cnt++;    
      }    
    }

    public void add(LIST<object> lst) { 
      add(new CommonDmlResults(lst,null)); 
    }
    
    public void add(LIST<object> lst,  LIST<sobject> records) {
      add(new CommonDmlResults(lst,records));
    }

    public void add(CommonDmlResults.Result r) {
      if (results.size() < resultLimit) {
        results.add(r);
      }
    }    

    public void add(CommonDmlResults dmlr) {
      if (results.size() < resultLimit) {
        if (results.size()+dmlr.results.size() < resultLimit) {
           results.addAll(dmlr.results);
        }else {
          for (Result r : dmlr.results) {
            if (results.size() < resultLimit)
              this.add(r);
            else
              break;
          }
        }
        messages.addAll(dmlr.messages);     
      }        
    }

    public void add(LIST<CommonDmlResults.Result> lst) {
      if (results.size() < resultLimit) {
        results.addAll(lst);
      }
    }

    public class Result {
      public sobject record       {get;set;}
      public Id id                {get;set;}
      public string errors        {get;set;}
      public boolean success      {get;set;}
      public string statusCode    {get;set;}

      public Result(Database.Saveresult r) { 
        this(r,null); 
      }
     
      public Result(Database.Saveresult r, sobject sObj) {
        if (r.getId() != null) {
          id = r.getId();
        } else if (sObj != null && sObj.id != null) {
          id = sObj.Id;
        }
        errors = string.valueOf(r);
        statusCode = (!r.getErrors().isEmpty()) ? string.valueOf(
                           r.getErrors()[0].getStatusCode()) : null;
        success = r.isSuccess();
        record=(sObj != null) ? sObj : null;
      }
      
      public Result(database.Deleteresult r) { this(r,null); }
      
      public Result(database.Deleteresult r,sobject sObj) {
        if (r.getId() != null) {
          id = r.getId();
        } else if (sObj != null && sObj.id != null) {
          id = sObj.Id;
        }
        errors = string.valueOf(r);
        statusCode = (!r.getErrors().isEmpty()) ? string.valueOf(
                        r.getErrors()[0].getStatusCode()) : null;
        success = r.isSuccess();
        record = (sObj!=null) ? sObj : null;
      }

      public Result(database.Upsertresult r) { this(r,null); }
      
      public Result(database.Upsertresult r,sobject sObj) {
        if (r.getId() != null)
          id = r.getId();
        else if (sObj != null && sObj.id != null)
          id = sObj.Id;
         
        errors=string.valueOf(r);
        statusCode = (!r.getErrors().isEmpty()) ? string.valueOf(
                          r.getErrors()[0].getStatusCode()) : null;
        success = r.isSuccess();
        record=(sObj != null) ? sObj : null;
      }

      public Result(Database.Undeleteresult r) { this(r,null); }

      public Result(Database.Undeleteresult r,sobject sObj) {

        if (r.getId() != null) {
          id = r.getId();
        } else if (sObj != null && sObj.id != null) {
          id = sObj.Id;
        }
        errors = string.valueOf(r);
        statusCode = (!r.getErrors().isEmpty()) ? string.valueOf(
                      r.getErrors()[0].getStatusCode()) : null;
        success = r.isSuccess();
        record = (sObj != null) ? sObj : null;
      }
    }
    
    public string resultsToString() {
      string rtn;
      rtn = 'Total DML results: '+ String.valueOf(results.size())+'\n';
    
      if (results.size() > 0 ) {
        for(CommonDmlResults.Result r : results) {
          if(r.record != null)
          rtn += 'Record: '+ String.valueOf(r.record)+'\n';    
          rtn += 'Error: '+ String.valueOf(r.errors)+'\n\n';
        }
      }
        return rtn;    
    }
 
    public string resultsToHtml() {
      string rtn;
      rtn = 'Total DML results: '+String.valueOf(results.size())+'<br/>';
      rtn += '<table border="1px"><tr style="background:gray;"><th>id</th>' +
                      '<th>success</th><th>error(s)</th><th>record</th>';
      for (CommonDmlResults.Result r : results) {
        rtn += String.format('{0}{1}{2}{3}',
              new string[]{String.valueOf(r.id),String.valueOf(r.success),
                                       r.errors,String.valueOf(r.record)}); 
      }

      rtn += '';
      return rtn;    
    } 
       

    public void batchOnFinish(Id jobId) { 
      batchOnFinish(jobId,true,null); 
    }

    public void batchOnFinish(Id jobId, boolean OnlyNotifyOnError) {  
      batchOnFinish(jobId,OnlyNotifyOnError,null); 
    }
    
    public void batchOnFinish(Id jobId, boolean OnlyNotifyOnError, 
                                               String emailOverride) {
      boolean html = true;
      AsyncApexJob a = [Select ApexClass.Name, Id, Status, 
                    NumberOfErrors, JobItemsProcessed,
                    TotalJobItems, CreatedBy.Email
                    From AsyncApexJob 
                    where Id =:jobId];

      if (a.NumberOfErrors > 0 || results.size() > 0 
                                || (!OnlyNotifyOnError)) {
         
        Messaging.SingleEmailMessage mail = 
                             new Messaging.SingleEmailMessage();
        string email = (emailOverride!=null) ? emailOverride : 
                                             a.CreatedBy.Email;
        String[] toAddresses = new String[] { email };
        mail.setToAddresses(toAddresses);
        mail.setSubject(a.ApexClass.Name+': ' + a.Status);
         
        string s = '';
        string crlf = (html) ? '<br/>' : '\n';
        if(messages.size()==0)messages.add('Error and success mesages');  
        if (messages != null && messages.size() > 0) {
          s += crlf+'Messages:'+crlf+crlf;
           
          for (string msg : messages) {
            try {
              s += msg.replace('\n',crlf)+crlf;
            }catch(exception ex){}
          }
          s += crlf + crlf +'The batch Apex job processed '+a.TotalJobItems 
                 +' batches with '+a.NumberOfErrors+' failures.'+crlf+crlf;
          if (html) {  
            s += resultsToHtml();
            mail.setHtmlBody(s);
          } else {
            s += resultsToString();
            mail.setPlainTextBody(s);
          }
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       }
     }
   }
   
   public class InvalidResultException extends Exception {}

 }