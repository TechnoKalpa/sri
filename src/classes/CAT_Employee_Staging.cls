public class CAT_Employee_Staging {
public static void EmployeeStagingRecords() {

    CAT_Employee_Staging__c   EmpStaging = new CAT_Employee_Staging__c();
          
    
    List<CAT_Employee_Staging__c> lstTesting = new List<CAT_Employee_Staging__c>();
	CAT_Employee_Staging__c objTest;
	for(Integer i=1;i<=100;i++)
	{
		objTest=new CAT_Employee_Staging__c(First_Name__c='empstaging'+i,
                                    		Last_Name__c='emplast'+i,
                                    		Hire_Date__c = System.today().addYears(100),
                                    		Grade_Profile__c='testprofile'+i,
                                    		frzn_i_grade_profile__c='testfrznprofile'+i,
                                    		Active__c =true);
		lstTesting.add(objTest);
	}

        insert lstTesting;
	}
}