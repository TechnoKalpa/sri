public with sharing class ListViewController
{
    public String Test{get;set;}
    
    public list<ObjectWrapper> getObjectResults()
    {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.Sobjecttype objectType;
        list<ObjectWrapper> objectResults = new list<ObjectWrapper>();
        
        list<String> keyList = new list<String>();
        keyList.addAll(gd.keySet());
        keyList.sort();
        
        for(String str : keyList)
        {
            if(str.endsWithIgnoreCase('__c'))
            {
                objectType = gd.get(str);
                objectResults.add(new ObjectWrapper(objectType.getDescribe()));
            }
        }
        
        return objectResults;
    }
    
    public class ObjectWrapper
    {
        public String Lable{get; set;}
        public String KeyPrefix{get; set;}
        public String ObjectName{get; set;}
        
        public ObjectWrapper(Schema.Describesobjectresult objectResult)
        {
            Lable = objectResult.getLabel();
            KeyPrefix = objectResult.getKeyPrefix();
            ObjectName = objectResult.getName();
        }
    }
}