public with sharing class CAT_EmployeeTriggerUtil {

  public static Boolean isRunning = false;
  public static final String OUT_OF_CYCLE = 'Phase 0: Out of Cycle';
  public static final String DEFAULT_RYPPLE_CYCLE = 'NEW';
  public static final String Q1 = 'Q1';
  public static String CAT_RECORDTYPE = 'CAT Tool';

  /*public static void generateFocalFiles(
    List<CAT_Employee__c> employeeTriggerList) {

    isRunning = true;

    Boolean isOutOfCycle = false;
    Id outOfCycleId;
    List <CAT_Focal_File__c> newFocalsToGenerate =
      new List<CAT_Focal_File__c>();
    List <CAT_Focal_File__c> focalsToUpsert = new List<CAT_Focal_File__c>();
    List <CAT_Focal_File__c> focalsToUpdate = new List<CAT_Focal_File__c>();
    String outOfCycleRyppleCycleId;
    //Is there a Phase ready to generate focals?
    List <CAT_Review_Cycle__c> phaseReadyForFocals = [SELECT Id,
      Current_Performance_Too_New_Date__c,
      End_Date__c,
      Phase__c,
      Quarter__c,
      Rypple_Review_Cycle__c,
      Previous_Review_Cycle__c,
      Default_Stock_Value__c
      FROM CAT_Review_Cycle__c
      WHERE Phase__c != :CAT_DataRefreshBatch.PHASE5
      Order by End_Date__c Desc LIMIT 1];

    Map<Id, CAT_Employee__c> jobDataMap  =
      new Map<Id, CAT_Employee__c> ([SELECT Id,
      Job_To_Band__r.RCT_JobBand__r.Bonus__c,
      Job_To_Band__r.RCT_JobBand__r.Salary_Mid__c,
      Job_To_Band__c,
      Job_To_Band__r.RCT_JobProfile__r.Job_Code__c,
      Job_To_Band__r.RCT_JobProfile__r.Level__c,
      Job_To_Band__r.RCT_JobBand__r.frzn_equity_mid__c,
      Job_To_Band__r.RCT_JobBand__r.Frzn_Equity_mid_amount__c,
      Job_To_Band__r.RCT_JobBand__r.Eq_mult__c,
      Frzn_Job_To_Band__c,
      Frzn_Job_To_Band__r.RCT_JobBand__c,
      Frzn_Job_To_Band__r.RCT_JobBand__r.frzn_equity_mid__c,
      Frzn_Job_To_Band__r.RCT_JobBand__r.Frzn_Equity_mid_amount__c,
      (SELECT prop_de_approval__c,
              prop_ds_approval__c,
              prop_de_grant__c,
              prop_de_tier__c,
              Promotion_Flag__c,
              prop_grant__c,
              Final_Increase_Percent__c,
              Total_Bonus_Payout_Amount__c,
              Total_Salary_Increase__c,
              TTC_Increase_Percent__c
       FROM CAT_Focal_Archives__r
       WHERE CAT_Review_Cycle__c != null
       ORDER BY CAT_Review_Cycle__r.End_Date__c Desc
       LIMIT 2)
      FROM CAT_Employee__c
      WHERE Id in :employeeTriggerList]);

    if (phaseReadyForFocals!= null && phaseReadyForFocals.size() > 0) {

      //If were out of phase, reset the review cycle search to pick up on
      //archived cycles
      if (phaseReadyForFocals[0].Phase__c == OUT_OF_CYCLE) {
        outOfCycleId = phaseReadyForFocals[0].Id;
        outOfCycleRyppleCycleId =
          phaseReadyForFocals[0].Rypple_Review_Cycle__c;
        phaseReadyForFocals = CAT_HrbpUIUtil.getCurrentReviewCycle(false);
        isOutOfCycle = true;
      }

      List <CAT_Focal_File__c> focalsThatAlreadyExistOnPhase = [SELECT Id,
         Rypple_External_Id__c,
         Employee__c,
         Override_Set__r.prop_ds_approval__c,
         Override_Set__r.prop_de_approval__c,
         ACT_IC_Mgr_Switch__c,
         prop_de_grant__c,
         prop_grant__c,
         Total_Bonus_Payout_Amount__c,
         Total_Salary_Increase__c,
         TTC_Increase_Percent__c,
         CAT_Review_Cycle__c,
         prev_DS_approval__c,
         p_de__c,
         Actual_Increase_Percent__c,
         prev_q1_de_grant__c,
         prev_q1_refresher__c
        FROM CAT_Focal_File__c
          WHERE Employee__c IN :employeeTriggerList
        AND (CAT_Review_Cycle__c = :phaseReadyForFocals[0].Id
        OR CAT_Review_Cycle__c =
          :phaseReadyForFocals[0].Previous_Review_Cycle__c)
        order by CreatedDate desc];

      //See if any focals exists and if they do, map via Employee Id so we can
      //easily match against incoming Employees

      Map <Id, CAT_Focal_File__c> focalsThatExistMap =
        new Map<Id, CAT_Focal_File__c>();

      for (CAT_Focal_File__c foc: focalsThatAlreadyExistOnPhase) {
          if (foc.CAT_Review_Cycle__c == phaseReadyForFocals[0].Id) {
            focalsThatExistMap.put(foc.Employee__c, foc);
          }
      }

      for (CAT_Employee__c emp:employeeTriggerList ) {

        if ((emp.Hire_Date__c <=  phaseReadyForFocals[0].End_Date__c  &&
          !isOutOfCycle)||
          (isOutOfCycle && focalsThatExistMap.get(emp.Id) == null )) {

          CAT_Focal_File__c upsertFocal;
          if (focalsThatExistMap.get(emp.Id) == null) {
            upsertFocal                         = new CAT_Focal_File__c();
            upsertFocal.Employee__c             = emp.Id;
            upsertFocal.CAT_Review_Cycle__c     =  isOutOfCycle ? outOfCycleId :
              phaseReadyForFocals[0].Id;
          } else {
              upsertFocal                 = focalsThatExistMap.get(emp.Id);
              upsertFocal.Workday_Id__c   = emp.Workday_External_Id__c;
          }

          if (jobDataMap.containsKey(emp.Id)) {
            CAT_Review_Cycle__c cycle = phaseReadyForFocals[0];
            Boolean isQ1 = cycle.Quarter__c == Q1;
            CAT_Employee__c data = jobDataMap.get(emp.Id);

            setIfNotNull(upsertFocal, 'Prev_DS_Approval__c',
              (String)getFromXFocalArchive(data, 'Prop_DS_Approval__c',
                isQ1 ? 2 : 1));

            setIfNotNull(upsertFocal, 'P_DE__c',
              (String)getFromXFocalArchive(data, 'Prop_DE_Approval__c',
                isQ1 ? 2 : 1));

            setIfNotNull(upsertFocal, 'Previous_Promo_flag_Actual__c',
              (String)getFromLastFocalArchive(data, 'Promotion_Flag__c'));

           setIfNotNull(upsertFocal, 'Prev_DE_Tier__c',
              (String)getFromXFocalArchive(data, 'prop_de_tier__c',
                isQ1 ? 2 : 1));

            setIfNotNull(upsertFocal, 'Prev_Q1_DE_Grant__c',
              (Decimal)getFromXFocalArchive(data, 'Prop_DE_Grant__c',
                isQ1 ? 2 : 1));

            setIfNotNull(upsertFocal, 'Prev_Q1_Refresher__c', (Decimal)
              getFromXFocalArchive(data, 'Prop_Grant__c', isQ1 ? 2 : 1));

            setIfNotNull(upsertFocal, 'Prev_Total_Bonus_Payout__c', (Decimal)
              getFromLastFocalArchive(data, 'Total_Bonus_Payout_Amount__c'));

            setIfNotNull(upsertFocal,  'Prev_Total_Salary_Increase__c',
              (Decimal) getFromLastFocalArchive(data,
                'Final_Increase_Percent__c'));

            setIfNotNull(upsertFocal, 'Prev_Salary_Increase__c', (Decimal)
              getFromLastFocalArchive(data, 'Total_Salary_Increase__c'));

            setIfNotNull(upsertFocal, 'Prev_TTC_Increase__c', (Decimal)
              getFromLastFocalArchive(data, 'TTC_Increase_Percent__c'));
          }
          upsertFocal.frzn_i_grade_profile__c = emp.frzn_i_grade_profile__c;
          upsertFocal.equ_type__c = emp.Worker_Type__c;
          upsertFocal.last_i_promoted_date__c = emp.last_i_promoted_date__c;
          upsertFocal.frzn_i_full_proration_factor__c =
            emp.frzn_i_full_proration_factor__c;
          upsertFocal.prev_DE_approval__c = emp.prev_DE_approval__c;
          upsertFocal.frzn_i_job_code__c = emp.frzn_job_code__c;
          upsertFocal.curr_i_level__c = emp.curr_i_level__c;
          upsertFocal.curr_i_bonus_tgt__c = emp.curr_i_bonus_tgt__c;
          upsertFocal.ACT_Job_Profile__c = emp.Job_Profile__c;
          upsertFocal.Business_Title__c = emp.Title__c;
          upsertFocal.ACT_Location__c = emp.Location__c;
          upsertFocal.ACT_Department__c = emp.Department__c;
          upsertFocal.CurrencyIsoCode = emp.CurrencyIsoCode;
          upsertFocal.ACT_Salary__c = emp.Salary__c;
          upsertFocal.ACT_Bonus_Target_Percent__c = emp.Bonus_Target_Percent__c;
          upsertFocal.ACT_Job_Level__c = emp.Job_Level__c;
          upsertFocal.ACT_Job_Code__c = emp.Job_Code__c;
          upsertFocal.ACT_Direct_Manager__c = emp.Direct_Manager__c;
          upsertFocal.ACT_Abs_Job_Level__c = emp.Absolute_Level__c;
          upsertFocal.Workday_Id__c = emp.Workday_External_Id__c;
          upsertFocal.ACT_Country_Code__c = emp.Country_Code__c;
          upsertFocal.Terminated_Regrettable__c = emp.Terminated_Regrettable__c;
          upsertFocal.Date_of_Termination__c = null;
          upsertFocal.Active_Employee__c = emp.Active__c;
          upsertFocal.Comp_Grade__c = emp.Grade_Profile__c;
          upsertFocal.Prev_Promotion_Level__c =
            emp.Prev_Promotion_Level__c == '1' ? 'Yes' : 'No';
          upsertFocal.Current_Exempt_Status__c = emp.Current_Exempt_Status__c;
          upsertFocal.Prev_Performance_Assessment__c =
            emp.Prev_Performance_Assessment__c;
          upsertFocal.stock_value__c =
            phaseReadyForFocals[0].Default_Stock_Value__c;
          upsertFocal.FTC_FTE_Conversion__c = emp.FTC_FTE_Conversion__c;
          upsertFocal.FTE_Status__c = emp.FTE_Status__c;
          String ryppleCycleID;
          if(outOfCycleRyppleCycleId == null) {
            ryppleCycleID =
              phaseReadyForFocals[0].Rypple_Review_Cycle__c;
          } else {
            ryppleCycleID = outOfCycleRyppleCycleId;
          }

          String ryppleId =
            upsertFocal.Rypple_External_Id__c == null ? '' :
            upsertFocal.Rypple_External_Id__c;

          //Check if the existing Rypple Ext Id contains 'NEW'
          Boolean isUpdate = ryppleId.contains(DEFAULT_RYPPLE_CYCLE);

          upsertFocal.Rypple_External_Id__c =
            emp.Workday_External_Id__c + '-' + ryppleCycleID;

          //Set the appropriate variables if an employee is inactive
          if (!emp.Active__c) {
              upsertFocal.Date_of_Termination__c = emp.Date_of_Termination__c;
          }

          //Current job data lookup/pass over to Focals.

          if (jobDataMap.get(emp.Id).Job_To_Band__c != null &&
               jobDataMap.get(emp.Id).Job_To_Band__r
               .RCT_JobBand__c != null) {

            Decimal fteStatus =
              (emp.FTE_Status__c != null) ? (emp.FTE_Status__c / 100) : 1.0;

            Decimal frznEqtAmount = jobDataMap.get(emp.Id)
              .Job_To_Band__r.RCT_JobBand__r.Frzn_Equity_mid_amount__c;

            upsertFocal.Current_Salary_Midpoint__c = jobDataMap.get(emp.Id)
              .Job_To_Band__r.RCT_JobBand__r.Salary_Mid__c * fteStatus;
            upsertFocal.curr_a_equity_mid_amt__c =
              (frznEqtAmount == null) ? null : frznEqtAmount * fteStatus;

            upsertFocal.curr_a_equity_mid__c = jobDataMap.get(emp.Id)
              .Job_To_Band__r.RCT_JobBand__r.frzn_equity_mid__c;

            upsertFocal.Eq_a_mult__c =
              jobDataMap.get(emp.Id).Job_To_Band__r.RCT_JobBand__r.Eq_mult__c;
          } else {
            upsertFocal.Current_Salary_Midpoint__c = 0;
            upsertFocal.curr_a_equity_mid_amt__c   = 0;
            upsertFocal.curr_a_equity_mid__c       = 0;
            upsertFocal.Eq_a_mult__c               = 0;
          }

          if(jobDataMap.get(emp.Id).Frzn_Job_To_Band__c != null) {
            upsertFocal.frzn_a_equity_mid__c = jobDataMap.get(emp.Id)
              .Frzn_Job_To_Band__r.RCT_JobBand__r.frzn_equity_mid__c;
          } else {
            upsertFocal.frzn_a_equity_mid__c = 0;
          }

          if(isUpdate) {
            focalsToUpdate.add(upsertFocal);
          } else {
            focalsToUpsert.add(upsertFocal);
          }
        }
      }

      upsert focalsToUpsert Rypple_External_Id__c;

      if(!focalsToUpdate.isEmpty()) {
        update focalsToUpdate;
      }
    }

    isRunning = false;
  }

  private static void setIfNotNull(SObject focal, String field, Object value) {
    if (value != null) {
      focal.put(field, value);
    }
  }

  private static Object getFromLastFocalArchive(CAT_Employee__c emp,
    String fieldName) {

    return getFromXFocalArchive(emp, fieldName, 1);
  }

  private static Object getFromXFocalArchive(CAT_Employee__c emp,
    String fieldName, Integer focalArchiveNum) {

    Integer index = focalArchiveNum - 1;
    if (index >= emp.CAT_Focal_Archives__r.size()) {
      return null;
    } else {
      return emp.CAT_Focal_Archives__r.get(index).get(fieldName);
    }
  }

  /**
  * @description Find a job to band record from the employees Job Code
  * @description Sprint 27 Story 3517 - Find frozen and current position via
  * grade profile.  Allow for Overrides.
  * @param List<Employee__c> employeeTriggerList - List of employee incoming
  * from trigger
  **/
 /* public static void findJobToBandForEmployee(
    List<CAT_Employee__c> employeeTriggerList) {

    Set<String> currentJobCodes     = new Set<String>();
    Set<String> currentJobLocation  = new Set<String>();

    for (CAT_Employee__c e: employeeTriggerList) {
        if (e.Override_Job_Code__c != null) {
          currentJobCodes.add(e.Override_Job_Code__c);
        } else {
          currentJobCodes.add(e.Job_Code__c);
        }
        currentJobCodes.add(e.frzn_job_code__c);
        currentJobLocation.add(e.Country_Code__c);
        currentJobLocation.add(e.frzn_a_country_code__c);
    }

    List <QueueSobject> compAdmins  = [SELECT QueueId
      FROM QueueSobject
      WHERE Queue.Name = 'Comp Admin'
      AND SObjectType = 'CAT_Employee__c'
      Limit 1];


    List<RCT_JobToBand__c> currentJob = [SELECT Id,
      RCT_JobProfile__r.Job_Code__c,
      RCT_JobBand__r.Country_Code__c
      FROM RCT_JobToBand__c
      WHERE RCT_JobProfile__r.Job_Code__c in: currentJobCodes
      AND RCT_JobBand__r.Country_Code__c in: currentJobLocation
      AND Active__c = true];


    Map <String, RCT_JobToBand__c> currentJobMap =
      new Map <String, RCT_JobToBand__c>();

    for (RCT_JobToBand__c js: currentJob){
        currentJobMap.put(js.RCT_JobBand__r.Country_Code__c +
          js.RCT_JobProfile__r.Job_Code__c, js);
    }

    for (CAT_Employee__c emp: employeeTriggerList) {
      String j2b  = emp.Country_Code__c + emp.Job_Code__c;
      String fj2b = emp.frzn_a_country_code__c + emp.frzn_job_code__c;

      if (currentJobMap.containsKey(j2b)){
          emp.Job_To_Band__c = currentJobMap.get(j2b).Id;
      } else {
          emp.Job_To_Band__c = null;
      }
      if (currentJobMap.containsKey(fj2b)){
          emp.Frzn_Job_To_Band__c = currentJobMap.get(fj2b).Id;
      } else {
          emp.Frzn_Job_To_Band__c = null;
      }
      if (compAdmins.size() > 0) {
          emp.OwnerId = compAdmins[0].QueueId;
      }
    }
  }*/

  /**
  * @description Checks if the employees are new to cat.
  * @param       List<Employee__c> triggerNewList - List of employee incoming
  * from trigger
  * @param       Map<Id, CAT_Employee__c> triggerOldMap - Record info before
  * operation
  **/
  /*public static void filterEmployees(List<CAT_Employee__c> triggerNewList,
    Map<Id, CAT_Employee__c> triggerOldMap) {

    List<CAT_Employee__c> newToCAT = new List<CAT_Employee__c>();
    List<CAT_Employee__c> workdayIdUpdates = new List<CAT_Employee__c>();
    for(CAT_Employee__c emp : triggerNewList) {
      if(emp.Reset_CAT_UI__c && !triggerOldMap.get(emp.Id).Reset_CAT_UI__c) {
          newToCAT.add(emp);
      }

      if(emp.Workday_External_Id__c !=
        triggerOldMap.get(emp.Id).Workday_External_Id__c) {
          workdayIdUpdates.add(emp);
      }
    }

    if(!newToCAT.isEmpty()) {
        setupNewCATUsers(newToCAT);
    }
  }

  /**
  * @description Sets the tabs and column couplers for new cat employee.
  * @param       List<Employee__c> triggerNewList -
  * List of new cat employees incoming from trigger
  **/
 /* private static void setupNewCATUsers(List<CAT_Employee__c> employees) {
    System.SavePoint sp = Database.setSavePoint();
    try {
      //Get the users that correspond to the new employees
      List<User> newCATUsers = employeeToUser(employees);

      //Delete all tabs and cascade delete tab column couplers
      delete [SELECT Id FROM CAT_Grid_Tab__c WHERE Default__c = false
        AND OwnerId in :newCATUsers AND RecordType.Name = :CAT_RECORDTYPE];

      //Get defaults
      List<CAT_Grid_Tab__c> defaultTabs = [SELECT Id,
        Name,
        Tab_Order__c
        FROM CAT_Grid_Tab__c
        WHERE Active__c = true AND
        Default__c = true AND RecordType.Name = :CAT_RECORDTYPE];

      List<CAT_Tab_Column_Coupler__c> defaultTCC = [SELECT Width__c,
        Grid_Column__c,
        Order__c,
        Grid_Tab__c
        FROM CAT_Tab_Column_Coupler__c
        WHERE Grid_Tab__c in :defaultTabs AND
        Grid_Column__r.Active__c = true];

        //Clone, alert, insert tabs
        Map<Id, CAT_Grid_Tab__c> userTabs = new Map<Id, CAT_Grid_Tab__c>();
        for(User currUser : newCATUsers) {
          for(CAT_Grid_Tab__c currTab : defaultTabs) {
            CAT_Grid_Tab__c tab = currTab.clone(false, true);
            tab.OwnerId         = currUser.Id;
            tab.Default__c      = false;
            tab.RecordTypeId = CAT_HrbpUIUtil.getRecordType(tab,CAT_RECORDTYPE);
            userTabs.put(currTab.Id, tab);
          }
        }
        insert userTabs.values();

        //Clone, alert, insert TCC
        List<CAT_Tab_Column_Coupler__c> clonedCouplers =
          defaultTCC.deepClone(false);
        for(CAT_Tab_Column_Coupler__c clone : clonedCouplers) {
          clone.Grid_Tab__c = userTabs.get(clone.Grid_Tab__c).Id;
        }
        insert clonedCouplers;

        //Turn the flag back off
        for(CAT_Employee__c emp : employees) {
          emp.Reset_CAT_UI__c = false;
        }
      } catch (Exception ex) {
        Database.rollback(sp);
        employees[0].addError('Reset CAT UI will only work when the employee ' +
          'has a valid associated User.');
      }
    }

    private static List<User> employeeToUser(List<CAT_Employee__c> employees) {
      Set<String> workdayIds = new Set<String>();
      for(CAT_Employee__c emp : employees) {
          workdayIds.add(emp.Workday_External_Id__c);
      }

      return [SELECT Id,
        Facebook_Id__c,
        EmployeeNumber
        FROM User
        WHERE EmployeeNumber in :workdayIds];
    }*/
}